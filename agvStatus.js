export default [{
    "agvEntity": {
        "agvId": "SY01",
        "agvType": {
            "id": "7adf8c1f-c205-4724-ae94-87977e00f9ed",
            "typeCode": "1",
            "typeName": "类型1",
            "areaName": "区域1",
            "agvModel": "1吨AGV叉车",
            "agvNavigationType": 1,
            "isAllDireections": true,
            "highBattryThreshold": 100,
            "chargingThreshold": 70,
            "lowBatteryThreshold": 30,
            "createTime": "2022-01-04T17:40:50.670379",
            "updateTime": "2022-01-04T17:40:50.670379"
        },
        "allowOnline": true,
        "agvContour": {
            "id": "a3da8c0e-c73b-4dce-88d9-b3fb83bc3587",
            "contourName": "1吨AGV叉车轮廓",
            "length": 2.1,
            "width": 1.2,
            "height": 3,
            "centerX": 0.6,
            "centerY": 0.5,
            "createTime": "2022-01-04T17:40:50.670379",
            "updateTime": "2022-01-04T17:40:50.670379"
        },
        "chargeTotal": 0,
        "mileageTotal": 0,
        "createTime": "2022-01-04T17:40:51.122155",
        "updateTime": "2022-01-04T17:40:51.122155"
    },
    "agvState": {
        "updateTime": "2022-01-06T11:06:25.4187296+08:00",
        "stateMessage": {
            "taskId": null,
            "agvId": "SY01",
            "version": "1.5",
            "auto": 0,
            "onlineState": 0,
            "movement": 0,
            "agvState": 0,
            "agvSubState": 0,
            "tasking": 0,
            "progress": "0",
            "event": 0,
            "pose": {
                "x": -70.9,
                "y": -10.38,
                "degree": 60
            },
            "velocity": 0.5,
            "battery": {
                "soc": 100,
                "voltage": 24,
                "current": 5,
                "temperature": 30,
                "is_charging": false
            },
            "loading": {
                "state": 0,
                "height": 0
            },
            "pathID": "0",
            "pointID": "200",
            "odom": {
                "total_mileage": 0,
                "daily_mileage": 0
            },
            "faultErrorCode": []
        }
    },
    "connectState": {
        "updateTime": "2022-01-06T11:06:25.4190798+08:00",
        "connect": true
    },
    "taskState": null
  },
  {
    "agvEntity": {
      "agvId": "SY02",
      "agvType": {
        "id": "7adf8c1f-c205-4724-ae94-87977e00f9ed",
        "typeCode": "1",
        "typeName": "类型1",
        "areaName": "区域1",
        "agvModel": "1吨AGV叉车",
        "agvNavigationType": 1,
        "isAllDireections": true,
        "highBattryThreshold": 100,
        "chargingThreshold": 70,
        "lowBatteryThreshold": 30,
        "createTime": "2022-01-04T17:40:50.670379",
        "updateTime": "2022-01-04T17:40:50.670379"
      },
      "allowOnline": true,
      "agvContour": {
        "id": "a3da8c0e-c73b-4dce-88d9-b3fb83bc3587",
        "contourName": "1吨AGV叉车轮廓",
        "length": 2.1,
        "width": 1.2,
        "height": 3,
        "centerX": 0.6,
        "centerY": 0.5,
        "createTime": "2022-01-04T17:40:50.670379",
        "updateTime": "2022-01-04T17:40:50.670379"
      },
      "chargeTotal": 0,
      "mileageTotal": 0,
      "createTime": "2022-01-04T17:40:51.122154",
      "updateTime": "2022-01-04T17:40:51.122154"
    },
    "agvState": {
      "updateTime": "2022-01-06T11:06:25.480859+08:00",
      "stateMessage": {
        "taskId": null,
        "agvId": "SY02",
        "version": "1.5",
        "auto": 0,
        "onlineState": 0,
        "movement": 0,
        "agvState": 0,
        "agvSubState": 0,
        "tasking": 0,
        "progress": "0",
        "event": 0,
        "pose": {
          "x": -7.9,
          "y": -9.18,
          "degree": 0
        },
        "velocity": 0.5,
        "battery": {
          "soc": 100,
          "voltage": 24,
          "current": 5,
          "temperature": 30,
          "is_charging": false
        },
        "loading": {
          "state": 0,
          "height": 5
        },
        "pathID": "0",
        "pointID": "100",
        "odom": {
          "total_mileage": 0,
          "daily_mileage": 0
        },
        "faultErrorCode": []
      }
    },
    "connectState": {
      "updateTime": "2022-01-06T11:06:25.481303+08:00",
      "connect": true
    },
    "taskState": null
  }
]