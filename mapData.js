export default {
  "topograph": {
    "version": "V1.6.0",
    "date": "2021-10-11T20:50:48",
    "roads": [
      {
        "id": 4,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 201,
            "x": -24.304,
            "y": -5.09
          },
          {
            "id": 1105,
            "x": -24.304,
            "y": -3.56
          }
        ],
        "keypoints_id": [
          {
            "id": 201,
            "x": -24.304,
            "y": -5.09
          },
          {
            "id": 1105,
            "x": -24.304,
            "y": -3.56
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 5,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 202,
            "x": -25.443999999999996,
            "y": -5.09
          },
          {
            "id": 1106,
            "x": -25.443999999999996,
            "y": -3.56
          }
        ],
        "keypoints_id": [
          {
            "id": 202,
            "x": -25.443999999999996,
            "y": -5.09
          },
          {
            "id": 1106,
            "x": -25.443999999999996,
            "y": -3.56
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "end_to_start",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 6,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 203,
            "x": -26.524,
            "y": -5.09
          },
          {
            "id": 682,
            "x": -26.524,
            "y": -3.56
          }
        ],
        "keypoints_id": [
          {
            "id": 203,
            "x": -26.524,
            "y": -5.09
          },
          {
            "id": 682,
            "x": -26.524,
            "y": -3.56
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "end_to_start",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 7,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 204,
            "x": -27.644,
            "y": -5.09
          },
          {
            "id": 683,
            "x": -27.644,
            "y": -3.56
          }
        ],
        "keypoints_id": [
          {
            "id": 204,
            "x": -27.644,
            "y": -5.09
          },
          {
            "id": 683,
            "x": -27.644,
            "y": -3.56
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "end_to_start",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 9,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 301,
            "x": -33.82,
            "y": -5.06
          },
          {
            "id": 686,
            "x": -33.82,
            "y": -3.56
          }
        ],
        "keypoints_id": [
          {
            "id": 301,
            "x": -33.82,
            "y": -5.06
          },
          {
            "id": 686,
            "x": -33.82,
            "y": -3.56
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "end_to_start",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 10,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 302,
            "x": -34.89,
            "y": -5.064166666666666
          },
          {
            "id": 687,
            "x": -34.89,
            "y": -3.56
          }
        ],
        "keypoints_id": [
          {
            "id": 302,
            "x": -34.89,
            "y": -5.064166666666666
          },
          {
            "id": 687,
            "x": -34.89,
            "y": -3.56
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "end_to_start",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 11,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 303,
            "x": -36.12,
            "y": -5.06
          },
          {
            "id": 688,
            "x": -36.12,
            "y": -3.56
          }
        ],
        "keypoints_id": [
          {
            "id": 303,
            "x": -36.12,
            "y": -5.06
          },
          {
            "id": 688,
            "x": -36.12,
            "y": -3.56
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "end_to_start",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 12,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 304,
            "x": -37.35,
            "y": -5.06
          },
          {
            "id": 689,
            "x": -37.35,
            "y": -3.56
          }
        ],
        "keypoints_id": [
          {
            "id": 304,
            "x": -37.35,
            "y": -5.06
          },
          {
            "id": 689,
            "x": -37.35,
            "y": -3.56
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "end_to_start",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 14,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 401,
            "x": -41.160000000000004,
            "y": -5
          },
          {
            "id": 672,
            "x": -41.160000000000004,
            "y": -3.56
          }
        ],
        "keypoints_id": [
          {
            "id": 401,
            "x": -41.160000000000004,
            "y": -5
          },
          {
            "id": 672,
            "x": -41.160000000000004,
            "y": -3.56
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "end_to_start",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 27,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 43,
            "x": -23.01148148148149,
            "y": -2.15
          },
          {
            "id": 42,
            "x": -20.78,
            "y": -2.15
          }
        ],
        "keypoints_id": [
          {
            "id": 43,
            "x": -23.01148148148149,
            "y": -2.15
          },
          {
            "id": 42,
            "x": -20.78,
            "y": -2.15
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 1,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 29,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 44,
            "x": -24.19657407407408,
            "y": -2.15
          },
          {
            "id": 43,
            "x": -23.01148148148149,
            "y": -2.15
          }
        ],
        "keypoints_id": [
          {
            "id": 44,
            "x": -24.19657407407408,
            "y": -2.15
          },
          {
            "id": 43,
            "x": -23.01148148148149,
            "y": -2.15
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 1,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 31,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 45,
            "x": -25.28127314814816,
            "y": -2.15
          },
          {
            "id": 44,
            "x": -24.19657407407408,
            "y": -2.15
          }
        ],
        "keypoints_id": [
          {
            "id": 45,
            "x": -25.28127314814816,
            "y": -2.15
          },
          {
            "id": 44,
            "x": -24.19657407407408,
            "y": -2.15
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 1,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 33,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 46,
            "x": -26.43599537037037,
            "y": -2.15
          },
          {
            "id": 45,
            "x": -25.28127314814816,
            "y": -2.15
          }
        ],
        "keypoints_id": [
          {
            "id": 46,
            "x": -26.43599537037037,
            "y": -2.15
          },
          {
            "id": 45,
            "x": -25.28127314814816,
            "y": -2.15
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 1,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 35,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 47,
            "x": -28.40824,
            "y": -2.15
          },
          {
            "id": 46,
            "x": -26.43599537037037,
            "y": -2.15
          }
        ],
        "keypoints_id": [
          {
            "id": 47,
            "x": -28.40824,
            "y": -2.15
          },
          {
            "id": 46,
            "x": -26.43599537037037,
            "y": -2.15
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 1,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 39,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 49,
            "x": -33.492361111111116,
            "y": -2.15
          },
          {
            "id": 48,
            "x": -32.33558333333333,
            "y": -2.15
          }
        ],
        "keypoints_id": [
          {
            "id": 49,
            "x": -33.492361111111116,
            "y": -2.15
          },
          {
            "id": 48,
            "x": -32.33558333333333,
            "y": -2.15
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 1,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 41,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 50,
            "x": -34.72222222222222,
            "y": -2.15
          },
          {
            "id": 49,
            "x": -33.492361111111116,
            "y": -2.15
          }
        ],
        "keypoints_id": [
          {
            "id": 50,
            "x": -34.72222222222222,
            "y": -2.15
          },
          {
            "id": 49,
            "x": -33.492361111111116,
            "y": -2.15
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 1,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 43,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 51,
            "x": -35.90222222222223,
            "y": -2.15
          },
          {
            "id": 50,
            "x": -34.72222222222222,
            "y": -2.15
          }
        ],
        "keypoints_id": [
          {
            "id": 51,
            "x": -35.90222222222223,
            "y": -2.15
          },
          {
            "id": 50,
            "x": -34.72222222222222,
            "y": -2.15
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 1,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 45,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 52,
            "x": -37.19,
            "y": -2.15
          },
          {
            "id": 51,
            "x": -35.90222222222223,
            "y": -2.15
          }
        ],
        "keypoints_id": [
          {
            "id": 52,
            "x": -37.19,
            "y": -2.15
          },
          {
            "id": 51,
            "x": -35.90222222222223,
            "y": -2.15
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 1,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 47,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 53,
            "x": -39.79666666666667,
            "y": -2.15
          },
          {
            "id": 52,
            "x": -37.19,
            "y": -2.15
          }
        ],
        "keypoints_id": [
          {
            "id": 53,
            "x": -39.79666666666667,
            "y": -2.15
          },
          {
            "id": 52,
            "x": -37.19,
            "y": -2.15
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 1,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 55,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 57,
            "x": -40.9,
            "y": -0.75
          },
          {
            "id": 56,
            "x": -42.78,
            "y": -0.75
          }
        ],
        "keypoints_id": [
          {
            "id": 57,
            "x": -40.9,
            "y": -0.75
          },
          {
            "id": 56,
            "x": -42.78,
            "y": -0.75
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 1,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 57,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 58,
            "x": -39.049599609375,
            "y": -0.75
          },
          {
            "id": 57,
            "x": -40.9,
            "y": -0.75
          }
        ],
        "keypoints_id": [
          {
            "id": 58,
            "x": -39.049599609375,
            "y": -0.75
          },
          {
            "id": 57,
            "x": -40.9,
            "y": -0.75
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 1,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 59,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 59,
            "x": -37.75,
            "y": -0.75
          },
          {
            "id": 58,
            "x": -39.049599609375,
            "y": -0.75
          }
        ],
        "keypoints_id": [
          {
            "id": 59,
            "x": -37.75,
            "y": -0.75
          },
          {
            "id": 58,
            "x": -39.049599609375,
            "y": -0.75
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 1,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 61,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 60,
            "x": -36.6,
            "y": -0.75
          },
          {
            "id": 59,
            "x": -37.75,
            "y": -0.75
          }
        ],
        "keypoints_id": [
          {
            "id": 60,
            "x": -36.6,
            "y": -0.75
          },
          {
            "id": 59,
            "x": -37.75,
            "y": -0.75
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 1,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 63,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 61,
            "x": -35.47,
            "y": -0.75
          },
          {
            "id": 60,
            "x": -36.6,
            "y": -0.75
          }
        ],
        "keypoints_id": [
          {
            "id": 61,
            "x": -35.47,
            "y": -0.75
          },
          {
            "id": 60,
            "x": -36.6,
            "y": -0.75
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 1,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 67,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 63,
            "x": -29.638960000000004,
            "y": -0.75
          },
          {
            "id": 62,
            "x": -32.19,
            "y": -0.75
          }
        ],
        "keypoints_id": [
          {
            "id": 63,
            "x": -29.638960000000004,
            "y": -0.75
          },
          {
            "id": 62,
            "x": -32.19,
            "y": -0.75
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 1,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 69,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 64,
            "x": -28.6,
            "y": -0.75
          },
          {
            "id": 63,
            "x": -29.638960000000004,
            "y": -0.75
          }
        ],
        "keypoints_id": [
          {
            "id": 64,
            "x": -28.6,
            "y": -0.75
          },
          {
            "id": 63,
            "x": -29.638960000000004,
            "y": -0.75
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 1,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 71,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 65,
            "x": -27.30088,
            "y": -0.75
          },
          {
            "id": 64,
            "x": -28.6,
            "y": -0.75
          }
        ],
        "keypoints_id": [
          {
            "id": 65,
            "x": -27.30088,
            "y": -0.75
          },
          {
            "id": 64,
            "x": -28.6,
            "y": -0.75
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 1,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 73,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 66,
            "x": -26.21,
            "y": -0.75
          },
          {
            "id": 65,
            "x": -27.30088,
            "y": -0.75
          }
        ],
        "keypoints_id": [
          {
            "id": 66,
            "x": -26.21,
            "y": -0.75
          },
          {
            "id": 65,
            "x": -27.30088,
            "y": -0.75
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 1,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 75,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 67,
            "x": -24.336212747395834,
            "y": -0.75
          },
          {
            "id": 66,
            "x": -26.21,
            "y": -0.75
          }
        ],
        "keypoints_id": [
          {
            "id": 67,
            "x": -24.336212747395834,
            "y": -0.75
          },
          {
            "id": 66,
            "x": -26.21,
            "y": -0.75
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 1,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 76,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 6,
            "x": -22.5,
            "y": -4.0600000000000005
          },
          {
            "id": 69,
            "x": -22.5,
            "y": -3.4995200000000013
          },
          {
            "id": 70,
            "x": -22.208000000000002,
            "y": -2.536264485596708
          },
          {
            "id": 68,
            "x": -21.47,
            "y": -2.15
          },
          {
            "id": 42,
            "x": -20.78,
            "y": -2.15
          }
        ],
        "keypoints_id": [
          {
            "id": 6,
            "x": -22.5,
            "y": -4.0600000000000005
          },
          {
            "id": 42,
            "x": -20.78,
            "y": -2.15
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "end_to_start",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 77,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 1105,
            "x": -24.304,
            "y": -3.56
          },
          {
            "id": 72,
            "x": -24.304,
            "y": -3.154685185185187
          },
          {
            "id": 73,
            "x": -24.14789096296295,
            "y": -2.4908039259259254
          },
          {
            "id": 71,
            "x": -23.565279115226346,
            "y": -2.15
          },
          {
            "id": 43,
            "x": -23.01148148148149,
            "y": -2.15
          }
        ],
        "keypoints_id": [
          {
            "id": 1105,
            "x": -24.304,
            "y": -3.56
          },
          {
            "id": 43,
            "x": -23.01148148148149,
            "y": -2.15
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "end_to_start",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 78,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 1106,
            "x": -25.443999999999996,
            "y": -3.56
          },
          {
            "id": 75,
            "x": -25.443999999999996,
            "y": -3.1383055555555557
          },
          {
            "id": 76,
            "x": -25.252199074074067,
            "y": -2.512680555555556
          },
          {
            "id": 74,
            "x": -24.63761574074075,
            "y": -2.15
          },
          {
            "id": 44,
            "x": -24.19657407407408,
            "y": -2.15
          }
        ],
        "keypoints_id": [
          {
            "id": 1106,
            "x": -25.443999999999996,
            "y": -3.56
          },
          {
            "id": 44,
            "x": -24.19657407407408,
            "y": -2.15
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "end_to_start",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 79,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 682,
            "x": -26.524,
            "y": -3.56
          },
          {
            "id": 78,
            "x": -26.524,
            "y": -3.1129038888888876
          },
          {
            "id": 79,
            "x": -26.353101851851868,
            "y": -2.466738962962964
          },
          {
            "id": 77,
            "x": -25.8,
            "y": -2.15
          },
          {
            "id": 45,
            "x": -25.28127314814816,
            "y": -2.15
          }
        ],
        "keypoints_id": [
          {
            "id": 682,
            "x": -26.524,
            "y": -3.56
          },
          {
            "id": 45,
            "x": -25.28127314814816,
            "y": -2.15
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "end_to_start",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 80,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 683,
            "x": -27.644,
            "y": -3.56
          },
          {
            "id": 81,
            "x": -27.644,
            "y": -3.126601851851852
          },
          {
            "id": 82,
            "x": -27.433263888888888,
            "y": -2.466088148148149
          },
          {
            "id": 80,
            "x": -26.82898148148149,
            "y": -2.15
          },
          {
            "id": 46,
            "x": -26.43599537037037,
            "y": -2.15
          }
        ],
        "keypoints_id": [
          {
            "id": 683,
            "x": -27.644,
            "y": -3.56
          },
          {
            "id": 46,
            "x": -26.43599537037037,
            "y": -2.15
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "end_to_start",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 81,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 684,
            "x": -30,
            "y": -3.97
          },
          {
            "id": 84,
            "x": -30,
            "y": -3.59
          },
          {
            "id": 85,
            "x": -29.757959036458335,
            "y": -2.5750400000000004
          },
          {
            "id": 83,
            "x": -29,
            "y": -2.15
          },
          {
            "id": 47,
            "x": -28.40824,
            "y": -2.15
          }
        ],
        "keypoints_id": [
          {
            "id": 684,
            "x": -30,
            "y": -3.97
          },
          {
            "id": 47,
            "x": -28.40824,
            "y": -2.15
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "end_to_start",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 82,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 686,
            "x": -33.82,
            "y": -3.56
          },
          {
            "id": 87,
            "x": -33.82,
            "y": -3.163444444444444
          },
          {
            "id": 88,
            "x": -33.53839457031252,
            "y": -2.4645913333333347
          },
          {
            "id": 86,
            "x": -32.88458333333333,
            "y": -2.15
          },
          {
            "id": 48,
            "x": -32.33558333333333,
            "y": -2.15
          }
        ],
        "keypoints_id": [
          {
            "id": 686,
            "x": -33.82,
            "y": -3.56
          },
          {
            "id": 48,
            "x": -32.33558333333333,
            "y": -2.15
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "end_to_start",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 83,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 687,
            "x": -34.89,
            "y": -3.56
          },
          {
            "id": 90,
            "x": -34.89,
            "y": -3.1426111111111106
          },
          {
            "id": 91,
            "x": -34.6261029470486,
            "y": -2.454926666666666
          },
          {
            "id": 89,
            "x": -33.980000000000004,
            "y": -2.15
          },
          {
            "id": 49,
            "x": -33.492361111111116,
            "y": -2.15
          }
        ],
        "keypoints_id": [
          {
            "id": 687,
            "x": -34.89,
            "y": -3.56
          },
          {
            "id": 49,
            "x": -33.492361111111116,
            "y": -2.15
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "end_to_start",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 84,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 688,
            "x": -36.12,
            "y": -3.56
          },
          {
            "id": 93,
            "x": -36.12,
            "y": -3.1195277777777775
          },
          {
            "id": 94,
            "x": -35.86397088975695,
            "y": -2.499792222222221
          },
          {
            "id": 92,
            "x": -35.31847222222222,
            "y": -2.15
          },
          {
            "id": 50,
            "x": -34.72222222222222,
            "y": -2.15
          }
        ],
        "keypoints_id": [
          {
            "id": 688,
            "x": -36.12,
            "y": -3.56
          },
          {
            "id": 50,
            "x": -34.72222222222222,
            "y": -2.15
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "end_to_start",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 85,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 689,
            "x": -37.35,
            "y": -3.56
          },
          {
            "id": 96,
            "x": -37.35,
            "y": -3.1228333333333347
          },
          {
            "id": 97,
            "x": -37.07297143229167,
            "y": -2.5114866666666673
          },
          {
            "id": 95,
            "x": -36.44375,
            "y": -2.15
          },
          {
            "id": 51,
            "x": -35.90222222222223,
            "y": -2.15
          }
        ],
        "keypoints_id": [
          {
            "id": 689,
            "x": -37.35,
            "y": -3.56
          },
          {
            "id": 51,
            "x": -35.90222222222223,
            "y": -2.15
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "end_to_start",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 86,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 670,
            "x": -39,
            "y": -4.17
          },
          {
            "id": 99,
            "x": -39,
            "y": -3.3980000000000006
          },
          {
            "id": 621,
            "x": -38.717247890624996,
            "y": -2.580999999999999
          },
          {
            "id": 98,
            "x": -37.94,
            "y": -2.15
          },
          {
            "id": 52,
            "x": -37.19,
            "y": -2.15
          }
        ],
        "keypoints_id": [
          {
            "id": 670,
            "x": -39,
            "y": -4.17
          },
          {
            "id": 52,
            "x": -37.19,
            "y": -2.15
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "end_to_start",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 88,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 673,
            "x": -46,
            "y": -3.8600000000000003
          },
          {
            "id": 581,
            "x": -46,
            "y": -3.5
          },
          {
            "id": 582,
            "x": -45.7053056901042,
            "y": -2.6113466666666656
          },
          {
            "id": 580,
            "x": -45,
            "y": -2.15
          },
          {
            "id": 54,
            "x": -44.300000000000004,
            "y": -2.15
          }
        ],
        "keypoints_id": [
          {
            "id": 673,
            "x": -46,
            "y": -3.8600000000000003
          },
          {
            "id": 54,
            "x": -44.300000000000004,
            "y": -2.15
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "end_to_start",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 90,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 1105,
            "x": -24.304,
            "y": -3.56
          },
          {
            "id": 586,
            "x": -24.304,
            "y": -2.05
          },
          {
            "id": 112,
            "x": -24.68667418981484,
            "y": -1.315435185185184
          },
          {
            "id": 111,
            "x": -25.520960000000013,
            "y": -0.75
          },
          {
            "id": 66,
            "x": -26.21,
            "y": -0.75
          }
        ],
        "keypoints_id": [
          {
            "id": 1105,
            "x": -24.304,
            "y": -3.56
          },
          {
            "id": 66,
            "x": -26.21,
            "y": -0.75
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 91,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 1106,
            "x": -25.443999999999996,
            "y": -3.56
          },
          {
            "id": 113,
            "x": -25.443999999999996,
            "y": -2.05
          },
          {
            "id": 115,
            "x": -25.773495555555563,
            "y": -1.3006366666666664
          },
          {
            "id": 114,
            "x": -26.669120000000014,
            "y": -0.75
          },
          {
            "id": 65,
            "x": -27.30088,
            "y": -0.75
          }
        ],
        "keypoints_id": [
          {
            "id": 1106,
            "x": -25.443999999999996,
            "y": -3.56
          },
          {
            "id": 65,
            "x": -27.30088,
            "y": -0.75
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 92,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 682,
            "x": -26.524,
            "y": -3.56
          },
          {
            "id": 116,
            "x": -26.524,
            "y": -2.05
          },
          {
            "id": 118,
            "x": -26.978159999999995,
            "y": -1.2634722222222226
          },
          {
            "id": 117,
            "x": -28.007359999999995,
            "y": -0.75
          },
          {
            "id": 64,
            "x": -28.6,
            "y": -0.75
          }
        ],
        "keypoints_id": [
          {
            "id": 682,
            "x": -26.524,
            "y": -3.56
          },
          {
            "id": 64,
            "x": -28.6,
            "y": -0.75
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 93,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 683,
            "x": -27.644,
            "y": -3.56
          },
          {
            "id": 119,
            "x": -27.644,
            "y": -2.05
          },
          {
            "id": 121,
            "x": -28.094444444444452,
            "y": -1.2443981481481483
          },
          {
            "id": 120,
            "x": -29,
            "y": -0.75
          },
          {
            "id": 63,
            "x": -29.638960000000004,
            "y": -0.75
          }
        ],
        "keypoints_id": [
          {
            "id": 683,
            "x": -27.644,
            "y": -3.56
          },
          {
            "id": 63,
            "x": -29.638960000000004,
            "y": -0.75
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 94,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 684,
            "x": -30,
            "y": -3.97
          },
          {
            "id": 122,
            "x": -30,
            "y": -2.12
          },
          {
            "id": 124,
            "x": -30.301180251736106,
            "y": -1.3327648148148148
          },
          {
            "id": 123,
            "x": -31.2,
            "y": -0.75
          },
          {
            "id": 62,
            "x": -32.19,
            "y": -0.75
          }
        ],
        "keypoints_id": [
          {
            "id": 684,
            "x": -30,
            "y": -3.97
          },
          {
            "id": 62,
            "x": -32.19,
            "y": -0.75
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 95,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 686,
            "x": -33.82,
            "y": -3.56
          },
          {
            "id": 125,
            "x": -33.82,
            "y": -2.12
          },
          {
            "id": 127,
            "x": -34.02322652994791,
            "y": -1.3116777777777775
          },
          {
            "id": 126,
            "x": -34.71,
            "y": -0.75
          },
          {
            "id": 61,
            "x": -35.47,
            "y": -0.75
          }
        ],
        "keypoints_id": [
          {
            "id": 686,
            "x": -33.82,
            "y": -3.56
          },
          {
            "id": 61,
            "x": -35.47,
            "y": -0.75
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 96,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 687,
            "x": -34.89,
            "y": -3.56
          },
          {
            "id": 128,
            "x": -34.89,
            "y": -2.12
          },
          {
            "id": 130,
            "x": -35.11176074218749,
            "y": -1.3247999999999995
          },
          {
            "id": 129,
            "x": -35.910000000000004,
            "y": -0.75
          },
          {
            "id": 60,
            "x": -36.6,
            "y": -0.75
          }
        ],
        "keypoints_id": [
          {
            "id": 687,
            "x": -34.89,
            "y": -3.56
          },
          {
            "id": 60,
            "x": -36.6,
            "y": -0.75
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 97,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 688,
            "x": -36.12,
            "y": -3.56
          },
          {
            "id": 131,
            "x": -36.12,
            "y": -2.12
          },
          {
            "id": 133,
            "x": -36.33864013671875,
            "y": -1.34
          },
          {
            "id": 132,
            "x": -37.14,
            "y": -0.75
          },
          {
            "id": 59,
            "x": -37.75,
            "y": -0.75
          }
        ],
        "keypoints_id": [
          {
            "id": 688,
            "x": -36.12,
            "y": -3.56
          },
          {
            "id": 59,
            "x": -37.75,
            "y": -0.75
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 98,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 689,
            "x": -37.35,
            "y": -3.56
          },
          {
            "id": 134,
            "x": -37.35,
            "y": -2.12
          },
          {
            "id": 136,
            "x": -37.5847998046875,
            "y": -1.31
          },
          {
            "id": 135,
            "x": -38.209599609375,
            "y": -0.75
          },
          {
            "id": 58,
            "x": -39.049599609375,
            "y": -0.75
          }
        ],
        "keypoints_id": [
          {
            "id": 689,
            "x": -37.35,
            "y": -3.56
          },
          {
            "id": 58,
            "x": -39.049599609375,
            "y": -0.75
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 99,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 670,
            "x": -39,
            "y": -4.17
          },
          {
            "id": 137,
            "x": -39,
            "y": -2.12
          },
          {
            "id": 139,
            "x": -39.42887939453125,
            "y": -1.32
          },
          {
            "id": 138,
            "x": -40.2,
            "y": -0.75
          },
          {
            "id": 57,
            "x": -40.9,
            "y": -0.75
          }
        ],
        "keypoints_id": [
          {
            "id": 670,
            "x": -39,
            "y": -4.17
          },
          {
            "id": 57,
            "x": -40.9,
            "y": -0.75
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 100,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 672,
            "x": -41.160000000000004,
            "y": -3.56
          },
          {
            "id": 140,
            "x": -41.160000000000004,
            "y": -2.1283333333333334
          },
          {
            "id": 142,
            "x": -41.34166666666668,
            "y": -1.2900000000000007
          },
          {
            "id": 141,
            "x": -42.08,
            "y": -0.75
          },
          {
            "id": 56,
            "x": -42.78,
            "y": -0.75
          }
        ],
        "keypoints_id": [
          {
            "id": 672,
            "x": -41.160000000000004,
            "y": -3.56
          },
          {
            "id": 56,
            "x": -42.78,
            "y": -0.75
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 101,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 673,
            "x": -46,
            "y": -3.8600000000000003
          },
          {
            "id": 143,
            "x": -46,
            "y": -2.12
          },
          {
            "id": 145,
            "x": -46.323603515625,
            "y": -1.3483333333333338
          },
          {
            "id": 144,
            "x": -47.12,
            "y": -0.75
          },
          {
            "id": 55,
            "x": -47.94,
            "y": -0.75
          }
        ],
        "keypoints_id": [
          {
            "id": 673,
            "x": -46,
            "y": -3.8600000000000003
          },
          {
            "id": 55,
            "x": -47.94,
            "y": -0.75
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 151,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 105,
            "x": -22,
            "y": -19.05
          },
          {
            "id": 232,
            "x": -22,
            "y": -17.94280077365451
          }
        ],
        "keypoints_id": [
          {
            "id": 105,
            "x": -22,
            "y": -19.05
          },
          {
            "id": 232,
            "x": -22,
            "y": -17.94280077365451
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 152,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 232,
            "x": -22,
            "y": -17.94280077365451
          },
          {
            "id": 233,
            "x": -22,
            "y": -16.605079586829675
          },
          {
            "id": 235,
            "x": -22.276638888888893,
            "y": -15.451671694155092
          },
          {
            "id": 234,
            "x": -22.48605925933219,
            "y": -14.437164713541673
          },
          {
            "id": 635,
            "x": -22.509479627381985,
            "y": -12.72147022222222
          }
        ],
        "keypoints_id": [
          {
            "id": 232,
            "x": -22,
            "y": -17.94280077365451
          },
          {
            "id": 635,
            "x": -22.509479627381985,
            "y": -12.72147022222222
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 154,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 108,
            "x": -20.5,
            "y": -25
          },
          {
            "id": 239,
            "x": -22,
            "y": -25
          }
        ],
        "keypoints_id": [
          {
            "id": 108,
            "x": -20.5,
            "y": -25
          },
          {
            "id": 239,
            "x": -22,
            "y": -25
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 155,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 107,
            "x": -20.5,
            "y": -23.698883673411064
          },
          {
            "id": 241,
            "x": -22,
            "y": -23.69553469364426
          }
        ],
        "keypoints_id": [
          {
            "id": 107,
            "x": -20.5,
            "y": -23.698883673411064
          },
          {
            "id": 241,
            "x": -22,
            "y": -23.69553469364426
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 157,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 305,
            "x": -33.69,
            "y": -19.05
          },
          {
            "id": 245,
            "x": -33.69,
            "y": -17.341328733746362
          }
        ],
        "keypoints_id": [
          {
            "id": 305,
            "x": -33.69,
            "y": -19.05
          },
          {
            "id": 245,
            "x": -33.69,
            "y": -17.341328733746362
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 158,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 207,
            "x": -27.5,
            "y": -24.5
          },
          {
            "id": 247,
            "x": -29,
            "y": -24.5
          }
        ],
        "keypoints_id": [
          {
            "id": 207,
            "x": -27.5,
            "y": -24.5
          },
          {
            "id": 247,
            "x": -29,
            "y": -24.5
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 159,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 208,
            "x": -27.5,
            "y": -26
          },
          {
            "id": 249,
            "x": -29,
            "y": -26
          }
        ],
        "keypoints_id": [
          {
            "id": 208,
            "x": -27.5,
            "y": -26
          },
          {
            "id": 249,
            "x": -29,
            "y": -26
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 160,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 307,
            "x": -34,
            "y": -23.5
          },
          {
            "id": 251,
            "x": -32.995177469135804,
            "y": -23.5
          }
        ],
        "keypoints_id": [
          {
            "id": 307,
            "x": -34,
            "y": -23.5
          },
          {
            "id": 251,
            "x": -32.995177469135804,
            "y": -23.5
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 161,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 308,
            "x": -34,
            "y": -25
          },
          {
            "id": 253,
            "x": -33,
            "y": -25
          }
        ],
        "keypoints_id": [
          {
            "id": 308,
            "x": -34,
            "y": -25
          },
          {
            "id": 253,
            "x": -33,
            "y": -25
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 162,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 405,
            "x": -40.77,
            "y": -19.2
          },
          {
            "id": 255,
            "x": -40.77,
            "y": -17
          }
        ],
        "keypoints_id": [
          {
            "id": 405,
            "x": -40.77,
            "y": -19.2
          },
          {
            "id": 255,
            "x": -40.77,
            "y": -17
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 163,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 407,
            "x": -42.5,
            "y": -24
          },
          {
            "id": 257,
            "x": -41,
            "y": -24
          }
        ],
        "keypoints_id": [
          {
            "id": 407,
            "x": -42.5,
            "y": -24
          },
          {
            "id": 257,
            "x": -41,
            "y": -24
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 164,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 408,
            "x": -42.5,
            "y": -25.5
          },
          {
            "id": 259,
            "x": -41,
            "y": -25.5
          }
        ],
        "keypoints_id": [
          {
            "id": 408,
            "x": -42.5,
            "y": -25.5
          },
          {
            "id": 259,
            "x": -41,
            "y": -25.5
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 165,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 505,
            "x": -48.14,
            "y": -18.8
          },
          {
            "id": 261,
            "x": -48.14,
            "y": -17.5
          }
        ],
        "keypoints_id": [
          {
            "id": 505,
            "x": -48.14,
            "y": -18.8
          },
          {
            "id": 261,
            "x": -48.14,
            "y": -17.5
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 166,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 508,
            "x": -49.5,
            "y": -25.5
          },
          {
            "id": 263,
            "x": -48,
            "y": -25.5
          }
        ],
        "keypoints_id": [
          {
            "id": 508,
            "x": -49.5,
            "y": -25.5
          },
          {
            "id": 263,
            "x": -48,
            "y": -25.5
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 167,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 507,
            "x": -49.5,
            "y": -24
          },
          {
            "id": 265,
            "x": -48,
            "y": -24
          }
        ],
        "keypoints_id": [
          {
            "id": 507,
            "x": -49.5,
            "y": -24
          },
          {
            "id": 265,
            "x": -48,
            "y": -24
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 168,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 501,
            "x": -48.47,
            "y": -4.855
          },
          {
            "id": 267,
            "x": -48.47,
            "y": -3.56
          }
        ],
        "keypoints_id": [
          {
            "id": 501,
            "x": -48.47,
            "y": -4.855
          },
          {
            "id": 267,
            "x": -48.47,
            "y": -3.56
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 170,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 605,
            "x": -57.5,
            "y": -18.93
          },
          {
            "id": 271,
            "x": -57.5,
            "y": -16.221968000000007
          }
        ],
        "keypoints_id": [
          {
            "id": 605,
            "x": -57.5,
            "y": -18.93
          },
          {
            "id": 271,
            "x": -57.5,
            "y": -16.221968000000007
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 172,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 608,
            "x": -58.704443356249996,
            "y": -26
          },
          {
            "id": 274,
            "x": -56.5,
            "y": -26
          }
        ],
        "keypoints_id": [
          {
            "id": 608,
            "x": -58.704443356249996,
            "y": -26
          },
          {
            "id": 274,
            "x": -56.5,
            "y": -26
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "end_to_start",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 173,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 607,
            "x": -58.7,
            "y": -24
          },
          {
            "id": 277,
            "x": -56.5,
            "y": -24
          }
        ],
        "keypoints_id": [
          {
            "id": 607,
            "x": -58.7,
            "y": -24
          },
          {
            "id": 277,
            "x": -56.5,
            "y": -24
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 176,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 236,
            "x": -23.400000000000002,
            "y": -27
          },
          {
            "id": 279,
            "x": -23.400000000000002,
            "y": -23.5
          }
        ],
        "keypoints_id": [
          {
            "id": 236,
            "x": -23.400000000000002,
            "y": -27
          },
          {
            "id": 279,
            "x": -23.400000000000002,
            "y": -23.5
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 177,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 279,
            "x": -23.400000000000002,
            "y": -23.5
          },
          {
            "id": 278,
            "x": -23.400000000000002,
            "y": -22.2
          }
        ],
        "keypoints_id": [
          {
            "id": 279,
            "x": -23.400000000000002,
            "y": -23.5
          },
          {
            "id": 278,
            "x": -23.400000000000002,
            "y": -22.2
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 184,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 291,
            "x": -30,
            "y": -24.5
          },
          {
            "id": 290,
            "x": -30,
            "y": -23
          }
        ],
        "keypoints_id": [
          {
            "id": 291,
            "x": -30,
            "y": -24.5
          },
          {
            "id": 290,
            "x": -30,
            "y": -23
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "end_to_start",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 185,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 685,
            "x": -30,
            "y": -28.752000000000002
          },
          {
            "id": 291,
            "x": -30,
            "y": -24.5
          }
        ],
        "keypoints_id": [
          {
            "id": 685,
            "x": -30,
            "y": -28.752000000000002
          },
          {
            "id": 291,
            "x": -30,
            "y": -24.5
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "end_to_start",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 186,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 247,
            "x": -29,
            "y": -24.5
          },
          {
            "id": 292,
            "x": -29.289351851851855,
            "y": -24.5
          },
          {
            "id": 294,
            "x": -29.79681877572017,
            "y": -24.15676728708526
          },
          {
            "id": 293,
            "x": -30,
            "y": -23.5
          },
          {
            "id": 290,
            "x": -30,
            "y": -23
          }
        ],
        "keypoints_id": [
          {
            "id": 247,
            "x": -29,
            "y": -24.5
          },
          {
            "id": 290,
            "x": -30,
            "y": -23
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 187,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 249,
            "x": -29,
            "y": -26
          },
          {
            "id": 295,
            "x": -29.240000000000002,
            "y": -26
          },
          {
            "id": 297,
            "x": -29.799632654320998,
            "y": -25.683559801954736
          },
          {
            "id": 296,
            "x": -30,
            "y": -25
          },
          {
            "id": 291,
            "x": -30,
            "y": -24.5
          }
        ],
        "keypoints_id": [
          {
            "id": 249,
            "x": -29,
            "y": -26
          },
          {
            "id": 291,
            "x": -30,
            "y": -24.5
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 189,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 290,
            "x": -30,
            "y": -23
          },
          {
            "id": 298,
            "x": -30,
            "y": -21.650000000000002
          }
        ],
        "keypoints_id": [
          {
            "id": 290,
            "x": -30,
            "y": -23
          },
          {
            "id": 298,
            "x": -30,
            "y": -21.650000000000002
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "end_to_start",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 190,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 251,
            "x": -32.995177469135804,
            "y": -23.5
          },
          {
            "id": 299,
            "x": -31.71740488932292,
            "y": -23.5
          },
          {
            "id": 592,
            "x": -30.613257223958335,
            "y": -23.233086478645827
          },
          {
            "id": 591,
            "x": -30,
            "y": -22.5
          },
          {
            "id": 298,
            "x": -30,
            "y": -21.650000000000002
          }
        ],
        "keypoints_id": [
          {
            "id": 251,
            "x": -32.995177469135804,
            "y": -23.5
          },
          {
            "id": 298,
            "x": -30,
            "y": -21.650000000000002
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 191,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 253,
            "x": -33,
            "y": -25
          },
          {
            "id": 593,
            "x": -31.513640000000017,
            "y": -25
          },
          {
            "id": 595,
            "x": -30.543999999999993,
            "y": -24.729891927083333
          },
          {
            "id": 594,
            "x": -30,
            "y": -24
          },
          {
            "id": 290,
            "x": -30,
            "y": -23
          }
        ],
        "keypoints_id": [
          {
            "id": 253,
            "x": -33,
            "y": -25
          },
          {
            "id": 290,
            "x": -30,
            "y": -23
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 193,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 597,
            "x": -39,
            "y": -23.997847959201387
          },
          {
            "id": 596,
            "x": -39,
            "y": -22.041902307581026
          }
        ],
        "keypoints_id": [
          {
            "id": 597,
            "x": -39,
            "y": -23.997847959201387
          },
          {
            "id": 596,
            "x": -39,
            "y": -22.041902307581026
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "end_to_start",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 194,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 671,
            "x": -39,
            "y": -26.987778279111097
          },
          {
            "id": 597,
            "x": -39,
            "y": -23.997847959201387
          }
        ],
        "keypoints_id": [
          {
            "id": 671,
            "x": -39,
            "y": -26.987778279111097
          },
          {
            "id": 597,
            "x": -39,
            "y": -23.997847959201387
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "end_to_start",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 195,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 257,
            "x": -41,
            "y": -24
          },
          {
            "id": 598,
            "x": -40.76,
            "y": -24
          },
          {
            "id": 620,
            "x": -39.4286322962963,
            "y": -23.704160116753474
          },
          {
            "id": 599,
            "x": -39,
            "y": -22.808814974247692
          },
          {
            "id": 596,
            "x": -39,
            "y": -22.041902307581026
          }
        ],
        "keypoints_id": [
          {
            "id": 257,
            "x": -41,
            "y": -24
          },
          {
            "id": 596,
            "x": -39,
            "y": -22.041902307581026
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 196,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 259,
            "x": -41,
            "y": -25.5
          },
          {
            "id": 310,
            "x": -40.58638888888888,
            "y": -25.5
          },
          {
            "id": 312,
            "x": -39.55352437037037,
            "y": -25.35376460923032
          },
          {
            "id": 311,
            "x": -39,
            "y": -24.611885514756946
          },
          {
            "id": 597,
            "x": -39,
            "y": -23.997847959201387
          }
        ],
        "keypoints_id": [
          {
            "id": 259,
            "x": -41,
            "y": -25.5
          },
          {
            "id": 597,
            "x": -39,
            "y": -23.997847959201387
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 199,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 261,
            "x": -48.14,
            "y": -17.5
          },
          {
            "id": 314,
            "x": -48.14,
            "y": -16.4824
          },
          {
            "id": 316,
            "x": -47.34216000000001,
            "y": -15.127192910156257
          },
          {
            "id": 315,
            "x": -46,
            "y": -14.140627041015623
          },
          {
            "id": 313,
            "x": -46,
            "y": -12.512467041015626
          }
        ],
        "keypoints_id": [
          {
            "id": 261,
            "x": -48.14,
            "y": -17.5
          },
          {
            "id": 313,
            "x": -46,
            "y": -12.512467041015626
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 201,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 318,
            "x": -46,
            "y": -23.76864097656249
          },
          {
            "id": 317,
            "x": -46,
            "y": -22.498559414062502
          }
        ],
        "keypoints_id": [
          {
            "id": 318,
            "x": -46,
            "y": -23.76864097656249
          },
          {
            "id": 317,
            "x": -46,
            "y": -22.498559414062502
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "end_to_start",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 202,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 674,
            "x": -46,
            "y": -26.901887999999996
          },
          {
            "id": 318,
            "x": -46,
            "y": -23.76864097656249
          }
        ],
        "keypoints_id": [
          {
            "id": 674,
            "x": -46,
            "y": -26.901887999999996
          },
          {
            "id": 318,
            "x": -46,
            "y": -23.76864097656249
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "end_to_start",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 203,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 265,
            "x": -48,
            "y": -24
          },
          {
            "id": 319,
            "x": -46.95359999999999,
            "y": -24
          },
          {
            "id": 321,
            "x": -46.358719999999984,
            "y": -23.554639707031246
          },
          {
            "id": 320,
            "x": -46,
            "y": -23.07071941406249
          },
          {
            "id": 317,
            "x": -46,
            "y": -22.498559414062502
          }
        ],
        "keypoints_id": [
          {
            "id": 265,
            "x": -48,
            "y": -24
          },
          {
            "id": 317,
            "x": -46,
            "y": -22.498559414062502
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 204,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 263,
            "x": -48,
            "y": -25.5
          },
          {
            "id": 322,
            "x": -47.15519999999999,
            "y": -25.5
          },
          {
            "id": 324,
            "x": -46.37312000000002,
            "y": -25.194560488281244
          },
          {
            "id": 323,
            "x": -46,
            "y": -24.384000976562504
          },
          {
            "id": 318,
            "x": -46,
            "y": -23.76864097656249
          }
        ],
        "keypoints_id": [
          {
            "id": 263,
            "x": -48,
            "y": -25.5
          },
          {
            "id": 318,
            "x": -46,
            "y": -23.76864097656249
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 207,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 255,
            "x": -40.77,
            "y": -17
          },
          {
            "id": 326,
            "x": -40.77,
            "y": -16.03472
          },
          {
            "id": 328,
            "x": -40.11815999999999,
            "y": -14.637119707031252
          },
          {
            "id": 327,
            "x": -39,
            "y": -13.924079414062499
          },
          {
            "id": 325,
            "x": -39,
            "y": -11.4043994140625
          }
        ],
        "keypoints_id": [
          {
            "id": 255,
            "x": -40.77,
            "y": -17
          },
          {
            "id": 325,
            "x": -39,
            "y": -11.4043994140625
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 211,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 48,
            "x": -32.33558333333333,
            "y": -2.15
          },
          {
            "id": 335,
            "x": -31.08326416015625,
            "y": -2.15
          }
        ],
        "keypoints_id": [
          {
            "id": 48,
            "x": -32.33558333333333,
            "y": -2.15
          },
          {
            "id": 335,
            "x": -31.08326416015625,
            "y": -2.15
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 1,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 212,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 335,
            "x": -31.08326416015625,
            "y": -2.15
          },
          {
            "id": 47,
            "x": -28.40824,
            "y": -2.15
          }
        ],
        "keypoints_id": [
          {
            "id": 335,
            "x": -31.08326416015625,
            "y": -2.15
          },
          {
            "id": 47,
            "x": -28.40824,
            "y": -2.15
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 1,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 214,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 62,
            "x": -32.19,
            "y": -0.75
          },
          {
            "id": 339,
            "x": -33.55084776171875,
            "y": -0.75
          }
        ],
        "keypoints_id": [
          {
            "id": 62,
            "x": -32.19,
            "y": -0.75
          },
          {
            "id": 339,
            "x": -33.55084776171875,
            "y": -0.75
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 1,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 215,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 339,
            "x": -33.55084776171875,
            "y": -0.75
          },
          {
            "id": 61,
            "x": -35.47,
            "y": -0.75
          }
        ],
        "keypoints_id": [
          {
            "id": 339,
            "x": -33.55084776171875,
            "y": -0.75
          },
          {
            "id": 61,
            "x": -35.47,
            "y": -0.75
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 1,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 218,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 343,
            "x": -47.19055555555555,
            "y": -2.15
          },
          {
            "id": 54,
            "x": -44.300000000000004,
            "y": -2.15
          }
        ],
        "keypoints_id": [
          {
            "id": 343,
            "x": -47.19055555555555,
            "y": -2.15
          },
          {
            "id": 54,
            "x": -44.300000000000004,
            "y": -2.15
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 1,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 220,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 344,
            "x": -48.28160000000001,
            "y": -2.15
          },
          {
            "id": 343,
            "x": -47.19055555555555,
            "y": -2.15
          }
        ],
        "keypoints_id": [
          {
            "id": 344,
            "x": -48.28160000000001,
            "y": -2.15
          },
          {
            "id": 343,
            "x": -47.19055555555555,
            "y": -2.15
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 1,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 222,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 345,
            "x": -49.28439999999999,
            "y": -2.15
          },
          {
            "id": 344,
            "x": -48.28160000000001,
            "y": -2.15
          }
        ],
        "keypoints_id": [
          {
            "id": 345,
            "x": -49.28439999999999,
            "y": -2.15
          },
          {
            "id": 344,
            "x": -48.28160000000001,
            "y": -2.15
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 1,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 224,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 346,
            "x": -50.35,
            "y": -2.15
          },
          {
            "id": 345,
            "x": -49.28439999999999,
            "y": -2.15
          }
        ],
        "keypoints_id": [
          {
            "id": 346,
            "x": -50.35,
            "y": -2.15
          },
          {
            "id": 345,
            "x": -49.28439999999999,
            "y": -2.15
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 1,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 225,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 267,
            "x": -48.47,
            "y": -3.56
          },
          {
            "id": 347,
            "x": -48.47,
            "y": -3.0739722222222223
          },
          {
            "id": 349,
            "x": -48.2244177517361,
            "y": -2.459999999999999
          },
          {
            "id": 348,
            "x": -47.7051388888889,
            "y": -2.15
          },
          {
            "id": 343,
            "x": -47.19055555555555,
            "y": -2.15
          }
        ],
        "keypoints_id": [
          {
            "id": 267,
            "x": -48.47,
            "y": -3.56
          },
          {
            "id": 343,
            "x": -47.19055555555555,
            "y": -2.15
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 226,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 502,
            "x": -49.53,
            "y": -4.855
          },
          {
            "id": 351,
            "x": -49.53,
            "y": -3.56
          }
        ],
        "keypoints_id": [
          {
            "id": 502,
            "x": -49.53,
            "y": -4.855
          },
          {
            "id": 351,
            "x": -49.53,
            "y": -3.56
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 227,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 503,
            "x": -50.68,
            "y": -4.855
          },
          {
            "id": 353,
            "x": -50.68,
            "y": -3.56
          }
        ],
        "keypoints_id": [
          {
            "id": 503,
            "x": -50.68,
            "y": -4.855
          },
          {
            "id": 353,
            "x": -50.68,
            "y": -3.56
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 228,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 504,
            "x": -51.82000000000001,
            "y": -4.855
          },
          {
            "id": 355,
            "x": -51.82000000000001,
            "y": -3.56
          }
        ],
        "keypoints_id": [
          {
            "id": 504,
            "x": -51.82000000000001,
            "y": -4.855
          },
          {
            "id": 355,
            "x": -51.82000000000001,
            "y": -3.56
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 229,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 351,
            "x": -49.53,
            "y": -3.56
          },
          {
            "id": 356,
            "x": -49.53,
            "y": -3.0560833333333335
          },
          {
            "id": 358,
            "x": -49.29322113715275,
            "y": -2.446083333333333
          },
          {
            "id": 357,
            "x": -48.77,
            "y": -2.15
          },
          {
            "id": 344,
            "x": -48.28160000000001,
            "y": -2.15
          }
        ],
        "keypoints_id": [
          {
            "id": 351,
            "x": -49.53,
            "y": -3.56
          },
          {
            "id": 344,
            "x": -48.28160000000001,
            "y": -2.15
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 230,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 353,
            "x": -50.68,
            "y": -3.56
          },
          {
            "id": 359,
            "x": -50.68,
            "y": -3.037888888888888
          },
          {
            "id": 361,
            "x": -50.40069769965276,
            "y": -2.427694444444445
          },
          {
            "id": 360,
            "x": -49.87291666666666,
            "y": -2.15
          },
          {
            "id": 345,
            "x": -49.28439999999999,
            "y": -2.15
          }
        ],
        "keypoints_id": [
          {
            "id": 353,
            "x": -50.68,
            "y": -3.56
          },
          {
            "id": 345,
            "x": -49.28439999999999,
            "y": -2.15
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 231,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 355,
            "x": -51.82000000000001,
            "y": -3.56
          },
          {
            "id": 362,
            "x": -51.82000000000001,
            "y": -3.2108888888888893
          },
          {
            "id": 364,
            "x": -51.58049390190975,
            "y": -2.5264777777777785
          },
          {
            "id": 363,
            "x": -50.97275555555557,
            "y": -2.15
          },
          {
            "id": 346,
            "x": -50.35,
            "y": -2.15
          }
        ],
        "keypoints_id": [
          {
            "id": 355,
            "x": -51.82000000000001,
            "y": -3.56
          },
          {
            "id": 346,
            "x": -50.35,
            "y": -2.15
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 234,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 271,
            "x": -57.5,
            "y": -16.221968000000007
          },
          {
            "id": 366,
            "x": -57.5,
            "y": -15.416513995659724
          },
          {
            "id": 368,
            "x": -56.20920933333332,
            "y": -13.064322981336812
          },
          {
            "id": 367,
            "x": -54,
            "y": -11.574412298828124
          },
          {
            "id": 365,
            "x": -54,
            "y": -8.211456298828125
          }
        ],
        "keypoints_id": [
          {
            "id": 271,
            "x": -57.5,
            "y": -16.221968000000007
          },
          {
            "id": 365,
            "x": -54,
            "y": -8.211456298828125
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 235,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 272,
            "x": -53.03004102595217,
            "y": -28.327808000000005
          },
          {
            "id": 369,
            "x": -53.00216009820524,
            "y": -23.9399800703125
          }
        ],
        "keypoints_id": [
          {
            "id": 272,
            "x": -53.03004102595217,
            "y": -28.327808000000005
          },
          {
            "id": 369,
            "x": -53.00216009820524,
            "y": -23.9399800703125
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 236,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 369,
            "x": -53.00216009820524,
            "y": -23.9399800703125
          },
          {
            "id": 370,
            "x": -52.98807271374428,
            "y": -21.722824960937498
          }
        ],
        "keypoints_id": [
          {
            "id": 369,
            "x": -53.00216009820524,
            "y": -23.9399800703125
          },
          {
            "id": 370,
            "x": -52.98807271374428,
            "y": -21.722824960937498
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 238,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 274,
            "x": -56.5,
            "y": -26
          },
          {
            "id": 371,
            "x": -55.306143999999996,
            "y": -26
          },
          {
            "id": 373,
            "x": -54.095687187500026,
            "y": -25.72645159374999
          },
          {
            "id": 372,
            "x": -53.008249685007044,
            "y": -24.898823187500007
          },
          {
            "id": 369,
            "x": -53.00216009820524,
            "y": -23.9399800703125
          }
        ],
        "keypoints_id": [
          {
            "id": 274,
            "x": -56.5,
            "y": -26
          },
          {
            "id": 369,
            "x": -53.00216009820524,
            "y": -23.9399800703125
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 239,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 277,
            "x": -56.5,
            "y": -24
          },
          {
            "id": 374,
            "x": -54.76700800000002,
            "y": -24
          },
          {
            "id": 376,
            "x": -53.64295476562499,
            "y": -23.585042039062504
          },
          {
            "id": 375,
            "x": -52.9937516438187,
            "y": -22.616004078125002
          },
          {
            "id": 370,
            "x": -52.98807271374428,
            "y": -21.722824960937498
          }
        ],
        "keypoints_id": [
          {
            "id": 277,
            "x": -56.5,
            "y": -24
          },
          {
            "id": 370,
            "x": -52.98807271374428,
            "y": -21.722824960937498
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 240,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 273,
            "x": -52.941494140625004,
            "y": -14.392442626953123
          },
          {
            "id": 377,
            "x": -52.93733814956556,
            "y": -13.737727509765625
          },
          {
            "id": 379,
            "x": -53.4699853515625,
            "y": -13.121328125
          },
          {
            "id": 378,
            "x": -54,
            "y": -12.214624740234376
          },
          {
            "id": 268,
            "x": -54,
            "y": -11.850209259765629
          }
        ],
        "keypoints_id": [
          {
            "id": 273,
            "x": -52.941494140625004,
            "y": -14.392442626953123
          },
          {
            "id": 268,
            "x": -54,
            "y": -11.850209259765629
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 266,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 423,
            "x": -52.27166666666668,
            "y": -2.15
          },
          {
            "id": 346,
            "x": -50.35,
            "y": -2.15
          }
        ],
        "keypoints_id": [
          {
            "id": 423,
            "x": -52.27166666666668,
            "y": -2.15
          },
          {
            "id": 346,
            "x": -50.35,
            "y": -2.15
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 1,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 267,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 269,
            "x": -54,
            "y": -3.87
          },
          {
            "id": 424,
            "x": -54,
            "y": -3.47
          },
          {
            "id": 426,
            "x": -53.71689671666665,
            "y": -2.610150933333335
          },
          {
            "id": 425,
            "x": -53,
            "y": -2.15
          },
          {
            "id": 423,
            "x": -52.27166666666668,
            "y": -2.15
          }
        ],
        "keypoints_id": [
          {
            "id": 269,
            "x": -54,
            "y": -3.87
          },
          {
            "id": 423,
            "x": -52.27166666666668,
            "y": -2.15
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 268,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 237,
            "x": -23.400000000000002,
            "y": -18.498470996093758
          },
          {
            "id": 427,
            "x": -23.400000000000002,
            "y": -17.14347119140625
          },
          {
            "id": 429,
            "x": -22.858333333333334,
            "y": -15.963192952473957
          },
          {
            "id": 428,
            "x": -22.50094254621279,
            "y": -14.231048046875
          },
          {
            "id": 635,
            "x": -22.509479627381985,
            "y": -12.72147022222222
          }
        ],
        "keypoints_id": [
          {
            "id": 237,
            "x": -23.400000000000002,
            "y": -18.498470996093758
          },
          {
            "id": 635,
            "x": -22.509479627381985,
            "y": -12.72147022222222
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 269,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 239,
            "x": -22,
            "y": -25
          },
          {
            "id": 430,
            "x": -22.681666666666665,
            "y": -25
          },
          {
            "id": 432,
            "x": -23.1938425925926,
            "y": -24.652412109375
          },
          {
            "id": 431,
            "x": -23.400000000000002,
            "y": -24
          },
          {
            "id": 279,
            "x": -23.400000000000002,
            "y": -23.5
          }
        ],
        "keypoints_id": [
          {
            "id": 239,
            "x": -22,
            "y": -25
          },
          {
            "id": 279,
            "x": -23.400000000000002,
            "y": -23.5
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 270,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 241,
            "x": -22,
            "y": -23.69553469364426
          },
          {
            "id": 433,
            "x": -22.59,
            "y": -23.694220238435456
          },
          {
            "id": 435,
            "x": -23.2174537037037,
            "y": -23.39590386284723
          },
          {
            "id": 434,
            "x": -23.400000000000002,
            "y": -22.7
          },
          {
            "id": 278,
            "x": -23.400000000000002,
            "y": -22.2
          }
        ],
        "keypoints_id": [
          {
            "id": 241,
            "x": -22,
            "y": -23.69553469364426
          },
          {
            "id": 278,
            "x": -23.400000000000002,
            "y": -22.2
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 271,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 436,
            "x": -30,
            "y": -12.696
          },
          {
            "id": 286,
            "x": -30,
            "y": -7.865280273437503
          }
        ],
        "keypoints_id": [
          {
            "id": 436,
            "x": -30,
            "y": -12.696
          },
          {
            "id": 286,
            "x": -30,
            "y": -7.865280273437503
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "end_to_start",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 273,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 205,
            "x": -28.65,
            "y": -19.14
          },
          {
            "id": 437,
            "x": -28.65,
            "y": -17.704400488281262
          }
        ],
        "keypoints_id": [
          {
            "id": 205,
            "x": -28.65,
            "y": -19.14
          },
          {
            "id": 437,
            "x": -28.65,
            "y": -17.704400488281262
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 275,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 437,
            "x": -28.65,
            "y": -17.704400488281262
          },
          {
            "id": 438,
            "x": -28.65,
            "y": -16.85080048828125
          },
          {
            "id": 440,
            "x": -29.113833333333325,
            "y": -15.608266910807293
          },
          {
            "id": 439,
            "x": -30,
            "y": -14.719199999999997
          },
          {
            "id": 436,
            "x": -30,
            "y": -12.696
          }
        ],
        "keypoints_id": [
          {
            "id": 437,
            "x": -28.65,
            "y": -17.704400488281262
          },
          {
            "id": 436,
            "x": -30,
            "y": -12.696
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 276,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 1269,
            "x": -28,
            "y": -13.930000000000003
          },
          {
            "id": 442,
            "x": -28,
            "y": -13.200000000000003
          }
        ],
        "keypoints_id": [
          {
            "id": 1269,
            "x": -28,
            "y": -13.930000000000003
          },
          {
            "id": 442,
            "x": -28,
            "y": -13.200000000000003
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 277,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 442,
            "x": -28,
            "y": -13.200000000000003
          },
          {
            "id": 443,
            "x": -28,
            "y": -12.626
          },
          {
            "id": 445,
            "x": -28.983999999999998,
            "y": -11.672640380859375
          },
          {
            "id": 444,
            "x": -30,
            "y": -10.999280151367184
          },
          {
            "id": 286,
            "x": -30,
            "y": -7.865280273437503
          }
        ],
        "keypoints_id": [
          {
            "id": 442,
            "x": -28,
            "y": -13.200000000000003
          },
          {
            "id": 286,
            "x": -30,
            "y": -7.865280273437503
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": 2,
        "back_safe_level": -1,
        "left_safe_level": 2,
        "right_safe_level": 2,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 278,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 245,
            "x": -33.69,
            "y": -17.341328733746362
          },
          {
            "id": 446,
            "x": -33.69,
            "y": -16.310992244140618
          },
          {
            "id": 448,
            "x": -32.256352000000014,
            "y": -14.036447892578128
          },
          {
            "id": 447,
            "x": -30,
            "y": -12.44889615136719
          },
          {
            "id": 286,
            "x": -30,
            "y": -7.865280273437503
          }
        ],
        "keypoints_id": [
          {
            "id": 245,
            "x": -33.69,
            "y": -17.341328733746362
          },
          {
            "id": 286,
            "x": -30,
            "y": -7.865280273437503
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 279,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 402,
            "x": -42.300000000000004,
            "y": -5
          },
          {
            "id": 450,
            "x": -42.300000000000004,
            "y": -3.56
          }
        ],
        "keypoints_id": [
          {
            "id": 402,
            "x": -42.300000000000004,
            "y": -5
          },
          {
            "id": 450,
            "x": -42.300000000000004,
            "y": -3.56
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 280,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 403,
            "x": -43.38,
            "y": -4.98
          },
          {
            "id": 452,
            "x": -43.38,
            "y": -3.56
          }
        ],
        "keypoints_id": [
          {
            "id": 403,
            "x": -43.38,
            "y": -4.98
          },
          {
            "id": 452,
            "x": -43.38,
            "y": -3.56
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 281,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 404,
            "x": -44.5,
            "y": -4.98
          },
          {
            "id": 454,
            "x": -44.5,
            "y": -3.56
          }
        ],
        "keypoints_id": [
          {
            "id": 404,
            "x": -44.5,
            "y": -4.98
          },
          {
            "id": 454,
            "x": -44.5,
            "y": -3.56
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 282,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 54,
            "x": -44.300000000000004,
            "y": -2.15
          },
          {
            "id": 455,
            "x": -43.230000000000004,
            "y": -2.15
          }
        ],
        "keypoints_id": [
          {
            "id": 54,
            "x": -44.300000000000004,
            "y": -2.15
          },
          {
            "id": 455,
            "x": -43.230000000000004,
            "y": -2.15
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 1,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 283,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 455,
            "x": -43.230000000000004,
            "y": -2.15
          },
          {
            "id": 456,
            "x": -42.116388888888885,
            "y": -2.15
          }
        ],
        "keypoints_id": [
          {
            "id": 455,
            "x": -43.230000000000004,
            "y": -2.15
          },
          {
            "id": 456,
            "x": -42.116388888888885,
            "y": -2.15
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 1,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 284,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 456,
            "x": -42.116388888888885,
            "y": -2.15
          },
          {
            "id": 457,
            "x": -40.97277777777777,
            "y": -2.15
          }
        ],
        "keypoints_id": [
          {
            "id": 456,
            "x": -42.116388888888885,
            "y": -2.15
          },
          {
            "id": 457,
            "x": -40.97277777777777,
            "y": -2.15
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 1,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 285,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 457,
            "x": -40.97277777777777,
            "y": -2.15
          },
          {
            "id": 53,
            "x": -39.79666666666667,
            "y": -2.15
          }
        ],
        "keypoints_id": [
          {
            "id": 457,
            "x": -40.97277777777777,
            "y": -2.15
          },
          {
            "id": 53,
            "x": -39.79666666666667,
            "y": -2.15
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 1,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 286,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 56,
            "x": -42.78,
            "y": -0.75
          },
          {
            "id": 458,
            "x": -44.050000000000004,
            "y": -0.75
          }
        ],
        "keypoints_id": [
          {
            "id": 56,
            "x": -42.78,
            "y": -0.75
          },
          {
            "id": 458,
            "x": -44.050000000000004,
            "y": -0.75
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 1,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 287,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 458,
            "x": -44.050000000000004,
            "y": -0.75
          },
          {
            "id": 459,
            "x": -45.29,
            "y": -0.75
          }
        ],
        "keypoints_id": [
          {
            "id": 458,
            "x": -44.050000000000004,
            "y": -0.75
          },
          {
            "id": 459,
            "x": -45.29,
            "y": -0.75
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 1,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 288,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 459,
            "x": -45.29,
            "y": -0.75
          },
          {
            "id": 460,
            "x": -46.38,
            "y": -0.75
          }
        ],
        "keypoints_id": [
          {
            "id": 459,
            "x": -45.29,
            "y": -0.75
          },
          {
            "id": 460,
            "x": -46.38,
            "y": -0.75
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 1,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 289,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 460,
            "x": -46.38,
            "y": -0.75
          },
          {
            "id": 55,
            "x": -47.94,
            "y": -0.75
          }
        ],
        "keypoints_id": [
          {
            "id": 460,
            "x": -46.38,
            "y": -0.75
          },
          {
            "id": 55,
            "x": -47.94,
            "y": -0.75
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 1,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 290,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 450,
            "x": -42.300000000000004,
            "y": -3.56
          },
          {
            "id": 461,
            "x": -42.300000000000004,
            "y": -3.1149166666666672
          },
          {
            "id": 463,
            "x": -42.0276028726209,
            "y": -2.475480195473252
          },
          {
            "id": 462,
            "x": -41.46638888888891,
            "y": -2.15
          },
          {
            "id": 457,
            "x": -40.97277777777777,
            "y": -2.15
          }
        ],
        "keypoints_id": [
          {
            "id": 450,
            "x": -42.300000000000004,
            "y": -3.56
          },
          {
            "id": 457,
            "x": -40.97277777777777,
            "y": -2.15
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 291,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 452,
            "x": -43.38,
            "y": -3.56
          },
          {
            "id": 464,
            "x": -43.38,
            "y": -3.1082777777777784
          },
          {
            "id": 466,
            "x": -43.127100919495916,
            "y": -2.5047047325102874
          },
          {
            "id": 465,
            "x": -42.60902777777779,
            "y": -2.15
          },
          {
            "id": 456,
            "x": -42.116388888888885,
            "y": -2.15
          }
        ],
        "keypoints_id": [
          {
            "id": 452,
            "x": -43.38,
            "y": -3.56
          },
          {
            "id": 456,
            "x": -42.116388888888885,
            "y": -2.15
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 292,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 454,
            "x": -44.5,
            "y": -3.56
          },
          {
            "id": 467,
            "x": -44.5,
            "y": -3.0865000000000005
          },
          {
            "id": 469,
            "x": -44.30102930893131,
            "y": -2.46899074074074
          },
          {
            "id": 468,
            "x": -43.70125,
            "y": -2.15
          },
          {
            "id": 455,
            "x": -43.230000000000004,
            "y": -2.15
          }
        ],
        "keypoints_id": [
          {
            "id": 454,
            "x": -44.5,
            "y": -3.56
          },
          {
            "id": 455,
            "x": -43.230000000000004,
            "y": -2.15
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 293,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 450,
            "x": -42.300000000000004,
            "y": -3.56
          },
          {
            "id": 470,
            "x": -42.300000000000004,
            "y": -2.12
          },
          {
            "id": 472,
            "x": -42.560347342785484,
            "y": -1.341418467078189
          },
          {
            "id": 471,
            "x": -43.33,
            "y": -0.75
          },
          {
            "id": 458,
            "x": -44.050000000000004,
            "y": -0.75
          }
        ],
        "keypoints_id": [
          {
            "id": 450,
            "x": -42.300000000000004,
            "y": -3.56
          },
          {
            "id": 458,
            "x": -44.050000000000004,
            "y": -0.75
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 294,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 452,
            "x": -43.38,
            "y": -3.56
          },
          {
            "id": 473,
            "x": -43.38,
            "y": -2.12
          },
          {
            "id": 475,
            "x": -43.74981911490484,
            "y": -1.3405452674897118
          },
          {
            "id": 474,
            "x": -44.59,
            "y": -0.75
          },
          {
            "id": 459,
            "x": -45.29,
            "y": -0.75
          }
        ],
        "keypoints_id": [
          {
            "id": 452,
            "x": -43.38,
            "y": -3.56
          },
          {
            "id": 459,
            "x": -45.29,
            "y": -0.75
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 295,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 454,
            "x": -44.5,
            "y": -3.56
          },
          {
            "id": 476,
            "x": -44.5,
            "y": -2.12
          },
          {
            "id": 478,
            "x": -44.92085169913838,
            "y": -1.3213387345679006
          },
          {
            "id": 477,
            "x": -45.730000000000004,
            "y": -0.75
          },
          {
            "id": 460,
            "x": -46.38,
            "y": -0.75
          }
        ],
        "keypoints_id": [
          {
            "id": 454,
            "x": -44.5,
            "y": -3.56
          },
          {
            "id": 460,
            "x": -46.38,
            "y": -0.75
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 296,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 55,
            "x": -47.94,
            "y": -0.75
          },
          {
            "id": 479,
            "x": -50.14,
            "y": -0.75
          }
        ],
        "keypoints_id": [
          {
            "id": 55,
            "x": -47.94,
            "y": -0.75
          },
          {
            "id": 479,
            "x": -50.14,
            "y": -0.75
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 1,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 297,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 479,
            "x": -50.14,
            "y": -0.75
          },
          {
            "id": 480,
            "x": -51.260000000000005,
            "y": -0.75
          }
        ],
        "keypoints_id": [
          {
            "id": 479,
            "x": -50.14,
            "y": -0.75
          },
          {
            "id": 480,
            "x": -51.260000000000005,
            "y": -0.75
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 1,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 298,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 480,
            "x": -51.260000000000005,
            "y": -0.75
          },
          {
            "id": 481,
            "x": -52.55,
            "y": -0.75
          }
        ],
        "keypoints_id": [
          {
            "id": 480,
            "x": -51.260000000000005,
            "y": -0.75
          },
          {
            "id": 481,
            "x": -52.55,
            "y": -0.75
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 1,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 299,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 481,
            "x": -52.55,
            "y": -0.75
          },
          {
            "id": 482,
            "x": -53.989999999999995,
            "y": -0.75
          }
        ],
        "keypoints_id": [
          {
            "id": 481,
            "x": -52.55,
            "y": -0.75
          },
          {
            "id": 482,
            "x": -53.989999999999995,
            "y": -0.75
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 1,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 301,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 267,
            "x": -48.47,
            "y": -3.56
          },
          {
            "id": 483,
            "x": -48.47,
            "y": -2.12
          },
          {
            "id": 485,
            "x": -48.73353411458333,
            "y": -1.345
          },
          {
            "id": 484,
            "x": -49.480000000000004,
            "y": -0.75
          },
          {
            "id": 479,
            "x": -50.14,
            "y": -0.75
          }
        ],
        "keypoints_id": [
          {
            "id": 267,
            "x": -48.47,
            "y": -3.56
          },
          {
            "id": 479,
            "x": -50.14,
            "y": -0.75
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 302,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 351,
            "x": -49.53,
            "y": -3.56
          },
          {
            "id": 486,
            "x": -49.53,
            "y": -1.98
          },
          {
            "id": 488,
            "x": -49.841202734375024,
            "y": -1.2469499999999993
          },
          {
            "id": 487,
            "x": -50.63,
            "y": -0.75
          },
          {
            "id": 480,
            "x": -51.260000000000005,
            "y": -0.75
          }
        ],
        "keypoints_id": [
          {
            "id": 351,
            "x": -49.53,
            "y": -3.56
          },
          {
            "id": 480,
            "x": -51.260000000000005,
            "y": -0.75
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 303,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 353,
            "x": -50.68,
            "y": -3.56
          },
          {
            "id": 489,
            "x": -50.68,
            "y": -2.02
          },
          {
            "id": 491,
            "x": -50.98346686197916,
            "y": -1.2706000000000006
          },
          {
            "id": 490,
            "x": -51.94,
            "y": -0.75
          },
          {
            "id": 481,
            "x": -52.55,
            "y": -0.75
          }
        ],
        "keypoints_id": [
          {
            "id": 353,
            "x": -50.68,
            "y": -3.56
          },
          {
            "id": 481,
            "x": -52.55,
            "y": -0.75
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 304,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 355,
            "x": -51.82000000000001,
            "y": -3.56
          },
          {
            "id": 492,
            "x": -51.82000000000001,
            "y": -2.12
          },
          {
            "id": 494,
            "x": -52.27640234375001,
            "y": -1.3331333333333333
          },
          {
            "id": 493,
            "x": -53.260000000000005,
            "y": -0.75
          },
          {
            "id": 482,
            "x": -53.989999999999995,
            "y": -0.75
          }
        ],
        "keypoints_id": [
          {
            "id": 355,
            "x": -51.82000000000001,
            "y": -3.56
          },
          {
            "id": 482,
            "x": -53.989999999999995,
            "y": -0.75
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 334,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 672,
            "x": -41.160000000000004,
            "y": -3.56
          },
          {
            "id": 577,
            "x": -41.160000000000004,
            "y": -3.151666666666667
          },
          {
            "id": 579,
            "x": -40.91519352146562,
            "y": -2.489901234567901
          },
          {
            "id": 578,
            "x": -40.345277777777774,
            "y": -2.15
          },
          {
            "id": 53,
            "x": -39.79666666666667,
            "y": -2.15
          }
        ],
        "keypoints_id": [
          {
            "id": 672,
            "x": -41.160000000000004,
            "y": -3.56
          },
          {
            "id": 53,
            "x": -39.79666666666667,
            "y": -2.15
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 335,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 482,
            "x": -53.989999999999995,
            "y": -0.75
          },
          {
            "id": 636,
            "x": -55.59000000000001,
            "y": -0.75
          }
        ],
        "keypoints_id": [
          {
            "id": 482,
            "x": -53.989999999999995,
            "y": -0.75
          },
          {
            "id": 636,
            "x": -55.59000000000001,
            "y": -0.75
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 1,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 337,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 269,
            "x": -54,
            "y": -3.87
          },
          {
            "id": 637,
            "x": -54,
            "y": -2.12
          },
          {
            "id": 639,
            "x": -54.27822678385419,
            "y": -1.32048
          },
          {
            "id": 638,
            "x": -55.05,
            "y": -0.75
          },
          {
            "id": 636,
            "x": -55.59000000000001,
            "y": -0.75
          }
        ],
        "keypoints_id": [
          {
            "id": 269,
            "x": -54,
            "y": -3.87
          },
          {
            "id": 636,
            "x": -55.59000000000001,
            "y": -0.75
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 339,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 101,
            "x": -17.736,
            "y": -5.11
          },
          {
            "id": 643,
            "x": -17.736,
            "y": -3.562893518518519
          }
        ],
        "keypoints_id": [
          {
            "id": 101,
            "x": -17.736,
            "y": -5.11
          },
          {
            "id": 643,
            "x": -17.736,
            "y": -3.562893518518519
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.2,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 340,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 102,
            "x": -18.876,
            "y": -5.11
          },
          {
            "id": 645,
            "x": -18.876,
            "y": -3.56
          }
        ],
        "keypoints_id": [
          {
            "id": 102,
            "x": -18.876,
            "y": -5.11
          },
          {
            "id": 645,
            "x": -18.876,
            "y": -3.56
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.2,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 341,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 103,
            "x": -19.956,
            "y": -5.1
          },
          {
            "id": 647,
            "x": -19.956,
            "y": -3.568680555555555
          }
        ],
        "keypoints_id": [
          {
            "id": 103,
            "x": -19.956,
            "y": -5.1
          },
          {
            "id": 647,
            "x": -19.956,
            "y": -3.568680555555555
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.2,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 342,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 104,
            "x": -21.076,
            "y": -5.1
          },
          {
            "id": 649,
            "x": -21.076,
            "y": -3.56
          }
        ],
        "keypoints_id": [
          {
            "id": 104,
            "x": -21.076,
            "y": -5.1
          },
          {
            "id": 649,
            "x": -21.076,
            "y": -3.56
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.2,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 346,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 651,
            "x": -17.35,
            "y": -2.15
          },
          {
            "id": 650,
            "x": -16.519679999999997,
            "y": -2.15
          }
        ],
        "keypoints_id": [
          {
            "id": 651,
            "x": -17.35,
            "y": -2.15
          },
          {
            "id": 650,
            "x": -16.519679999999997,
            "y": -2.15
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 1,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 348,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 652,
            "x": -18.592337962962965,
            "y": -2.15
          },
          {
            "id": 651,
            "x": -17.35,
            "y": -2.15
          }
        ],
        "keypoints_id": [
          {
            "id": 652,
            "x": -18.592337962962965,
            "y": -2.15
          },
          {
            "id": 651,
            "x": -17.35,
            "y": -2.15
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 1,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 349,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 42,
            "x": -20.78,
            "y": -2.15
          },
          {
            "id": 653,
            "x": -19.875694444444445,
            "y": -2.15
          }
        ],
        "keypoints_id": [
          {
            "id": 42,
            "x": -20.78,
            "y": -2.15
          },
          {
            "id": 653,
            "x": -19.875694444444445,
            "y": -2.15
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 1,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 350,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 653,
            "x": -19.875694444444445,
            "y": -2.15
          },
          {
            "id": 652,
            "x": -18.592337962962965,
            "y": -2.15
          }
        ],
        "keypoints_id": [
          {
            "id": 653,
            "x": -19.875694444444445,
            "y": -2.15
          },
          {
            "id": 652,
            "x": -18.592337962962965,
            "y": -2.15
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 1,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 351,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 643,
            "x": -17.736,
            "y": -3.562893518518519
          },
          {
            "id": 654,
            "x": -17.736,
            "y": -3.1660000000000004
          },
          {
            "id": 656,
            "x": -17.47080004882812,
            "y": -2.512
          },
          {
            "id": 655,
            "x": -16.89,
            "y": -2.15
          },
          {
            "id": 650,
            "x": -16.519679999999997,
            "y": -2.15
          }
        ],
        "keypoints_id": [
          {
            "id": 643,
            "x": -17.736,
            "y": -3.562893518518519
          },
          {
            "id": 650,
            "x": -16.519679999999997,
            "y": -2.15
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 352,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 645,
            "x": -18.876,
            "y": -3.56
          },
          {
            "id": 657,
            "x": -18.876,
            "y": -3.1399999999999997
          },
          {
            "id": 659,
            "x": -18.543600585937494,
            "y": -2.499199951171875
          },
          {
            "id": 658,
            "x": -17.9,
            "y": -2.15
          },
          {
            "id": 651,
            "x": -17.35,
            "y": -2.15
          }
        ],
        "keypoints_id": [
          {
            "id": 645,
            "x": -18.876,
            "y": -3.56
          },
          {
            "id": 651,
            "x": -17.35,
            "y": -2.15
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 353,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 647,
            "x": -19.956,
            "y": -3.568680555555555
          },
          {
            "id": 660,
            "x": -19.956,
            "y": -3.1188935185185183
          },
          {
            "id": 662,
            "x": -19.767886257595485,
            "y": -2.4392361721462663
          },
          {
            "id": 661,
            "x": -19.108680555555555,
            "y": -2.15
          },
          {
            "id": 652,
            "x": -18.592337962962965,
            "y": -2.15
          }
        ],
        "keypoints_id": [
          {
            "id": 647,
            "x": -19.956,
            "y": -3.568680555555555
          },
          {
            "id": 652,
            "x": -18.592337962962965,
            "y": -2.15
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 354,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 649,
            "x": -21.076,
            "y": -3.56
          },
          {
            "id": 663,
            "x": -21.076,
            "y": -3.1141712962962975
          },
          {
            "id": 665,
            "x": -20.896685818142366,
            "y": -2.4793556043836804
          },
          {
            "id": 664,
            "x": -20.364722222222223,
            "y": -2.15
          },
          {
            "id": 653,
            "x": -19.875694444444445,
            "y": -2.15
          }
        ],
        "keypoints_id": [
          {
            "id": 649,
            "x": -21.076,
            "y": -3.56
          },
          {
            "id": 653,
            "x": -19.875694444444445,
            "y": -2.15
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 356,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 666,
            "x": -19.36,
            "y": -0.75
          },
          {
            "id": 667,
            "x": -20.539920000000002,
            "y": -0.75
          }
        ],
        "keypoints_id": [
          {
            "id": 666,
            "x": -19.36,
            "y": -0.75
          },
          {
            "id": 667,
            "x": -20.539920000000002,
            "y": -0.75
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 1,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 357,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 667,
            "x": -20.539920000000002,
            "y": -0.75
          },
          {
            "id": 668,
            "x": -21.728080000000002,
            "y": -0.75
          }
        ],
        "keypoints_id": [
          {
            "id": 667,
            "x": -20.539920000000002,
            "y": -0.75
          },
          {
            "id": 668,
            "x": -21.728080000000002,
            "y": -0.75
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 1,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 358,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 668,
            "x": -21.728080000000002,
            "y": -0.75
          },
          {
            "id": 669,
            "x": -22.974479999999996,
            "y": -0.75
          }
        ],
        "keypoints_id": [
          {
            "id": 668,
            "x": -21.728080000000002,
            "y": -0.75
          },
          {
            "id": 669,
            "x": -22.974479999999996,
            "y": -0.75
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 1,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 359,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 669,
            "x": -22.974479999999996,
            "y": -0.75
          },
          {
            "id": 67,
            "x": -24.336212747395834,
            "y": -0.75
          }
        ],
        "keypoints_id": [
          {
            "id": 669,
            "x": -22.974479999999996,
            "y": -0.75
          },
          {
            "id": 67,
            "x": -24.336212747395834,
            "y": -0.75
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 1,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 362,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 647,
            "x": -19.956,
            "y": -3.568680555555555
          },
          {
            "id": 676,
            "x": -19.956,
            "y": -2.05
          },
          {
            "id": 678,
            "x": -20.21088,
            "y": -1.2672799999999997
          },
          {
            "id": 677,
            "x": -21.1836,
            "y": -0.75
          },
          {
            "id": 668,
            "x": -21.728080000000002,
            "y": -0.75
          }
        ],
        "keypoints_id": [
          {
            "id": 647,
            "x": -19.956,
            "y": -3.568680555555555
          },
          {
            "id": 668,
            "x": -21.728080000000002,
            "y": -0.75
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 363,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 649,
            "x": -21.076,
            "y": -3.56
          },
          {
            "id": 679,
            "x": -21.076,
            "y": -2.05
          },
          {
            "id": 681,
            "x": -21.385,
            "y": -1.23272
          },
          {
            "id": 680,
            "x": -22.330879999999997,
            "y": -0.75
          },
          {
            "id": 669,
            "x": -22.974479999999996,
            "y": -0.75
          }
        ],
        "keypoints_id": [
          {
            "id": 649,
            "x": -21.076,
            "y": -3.56
          },
          {
            "id": 669,
            "x": -22.974479999999996,
            "y": -0.75
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 364,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 645,
            "x": -18.876,
            "y": -3.56
          },
          {
            "id": 690,
            "x": -18.876,
            "y": -2.05
          },
          {
            "id": 692,
            "x": -19.156096,
            "y": -1.2532506666666665
          },
          {
            "id": 691,
            "x": -20.03544,
            "y": -0.75
          },
          {
            "id": 667,
            "x": -20.539920000000002,
            "y": -0.75
          }
        ],
        "keypoints_id": [
          {
            "id": 645,
            "x": -18.876,
            "y": -3.56
          },
          {
            "id": 667,
            "x": -20.539920000000002,
            "y": -0.75
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 365,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 643,
            "x": -17.736,
            "y": -3.562893518518519
          },
          {
            "id": 693,
            "x": -17.736,
            "y": -2.0455200000000002
          },
          {
            "id": 695,
            "x": -18.006016,
            "y": -1.25
          },
          {
            "id": 694,
            "x": -18.7864,
            "y": -0.75
          },
          {
            "id": 666,
            "x": -19.36,
            "y": -0.75
          }
        ],
        "keypoints_id": [
          {
            "id": 643,
            "x": -17.736,
            "y": -3.562893518518519
          },
          {
            "id": 666,
            "x": -19.36,
            "y": -0.75
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 368,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 697,
            "x": -7.959328014648436,
            "y": -0.75
          },
          {
            "id": 698,
            "x": -9.155535726562498,
            "y": -0.75
          }
        ],
        "keypoints_id": [
          {
            "id": 697,
            "x": -7.959328014648436,
            "y": -0.75
          },
          {
            "id": 698,
            "x": -9.155535726562498,
            "y": -0.75
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 1,
        "min_speed": 0.5,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 369,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 698,
            "x": -9.155535726562498,
            "y": -0.75
          },
          {
            "id": 699,
            "x": -11.005120239257813,
            "y": -0.75
          }
        ],
        "keypoints_id": [
          {
            "id": 698,
            "x": -9.155535726562498,
            "y": -0.75
          },
          {
            "id": 699,
            "x": -11.005120239257813,
            "y": -0.75
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 1,
        "min_speed": 0.5,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 370,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 699,
            "x": -11.005120239257813,
            "y": -0.75
          },
          {
            "id": 820,
            "x": -12.748399531250005,
            "y": -0.75
          }
        ],
        "keypoints_id": [
          {
            "id": 699,
            "x": -11.005120239257813,
            "y": -0.75
          },
          {
            "id": 820,
            "x": -12.748399531250005,
            "y": -0.75
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 1,
        "min_speed": 0.5,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 380,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 820,
            "x": -12.748399531250005,
            "y": -0.75
          },
          {
            "id": 825,
            "x": -14.545439726562503,
            "y": -0.75
          }
        ],
        "keypoints_id": [
          {
            "id": 820,
            "x": -12.748399531250005,
            "y": -0.75
          },
          {
            "id": 825,
            "x": -14.545439726562503,
            "y": -0.75
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 1,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 381,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 825,
            "x": -14.545439726562503,
            "y": -0.75
          },
          {
            "id": 826,
            "x": -17.286155858506945,
            "y": -0.75
          }
        ],
        "keypoints_id": [
          {
            "id": 825,
            "x": -14.545439726562503,
            "y": -0.75
          },
          {
            "id": 826,
            "x": -17.286155858506945,
            "y": -0.75
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 1,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 382,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 826,
            "x": -17.286155858506945,
            "y": -0.75
          },
          {
            "id": 827,
            "x": -18.08659390067998,
            "y": -0.75
          }
        ],
        "keypoints_id": [
          {
            "id": 826,
            "x": -17.286155858506945,
            "y": -0.75
          },
          {
            "id": 827,
            "x": -18.08659390067998,
            "y": -0.75
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 1,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 383,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 827,
            "x": -18.08659390067998,
            "y": -0.75
          },
          {
            "id": 666,
            "x": -19.36,
            "y": -0.75
          }
        ],
        "keypoints_id": [
          {
            "id": 827,
            "x": -18.08659390067998,
            "y": -0.75
          },
          {
            "id": 666,
            "x": -19.36,
            "y": -0.75
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 1,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 384,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 650,
            "x": -16.519679999999997,
            "y": -2.15
          },
          {
            "id": 1550,
            "x": -14.609940730034719,
            "y": -2.15
          }
        ],
        "keypoints_id": [
          {
            "id": 650,
            "x": -16.519679999999997,
            "y": -2.15
          },
          {
            "id": 1550,
            "x": -14.609940730034719,
            "y": -2.15
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 1,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 385,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 1550,
            "x": -14.609940730034719,
            "y": -2.15
          },
          {
            "id": 1627,
            "x": -13.586928897931134,
            "y": -2.15
          }
        ],
        "keypoints_id": [
          {
            "id": 1550,
            "x": -14.609940730034719,
            "y": -2.15
          },
          {
            "id": 1627,
            "x": -13.586928897931134,
            "y": -2.15
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 1,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 386,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 1627,
            "x": -13.586928897931134,
            "y": -2.15
          },
          {
            "id": 824,
            "x": -11.45552001953125,
            "y": -2.15
          }
        ],
        "keypoints_id": [
          {
            "id": 1627,
            "x": -13.586928897931134,
            "y": -2.15
          },
          {
            "id": 824,
            "x": -11.45552001953125,
            "y": -2.15
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 1,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 387,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 636,
            "x": -55.59000000000001,
            "y": -0.75
          },
          {
            "id": 830,
            "x": -58.64920062499999,
            "y": -0.75
          }
        ],
        "keypoints_id": [
          {
            "id": 636,
            "x": -55.59000000000001,
            "y": -0.75
          },
          {
            "id": 830,
            "x": -58.64920062499999,
            "y": -0.75
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 1,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 390,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 711,
            "x": -55.54687996093752,
            "y": -2.15
          },
          {
            "id": 423,
            "x": -52.27166666666668,
            "y": -2.15
          }
        ],
        "keypoints_id": [
          {
            "id": 711,
            "x": -55.54687996093752,
            "y": -2.15
          },
          {
            "id": 423,
            "x": -52.27166666666668,
            "y": -2.15
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 1,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 392,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 712,
            "x": -56.631357773437514,
            "y": -2.15
          },
          {
            "id": 711,
            "x": -55.54687996093752,
            "y": -2.15
          }
        ],
        "keypoints_id": [
          {
            "id": 712,
            "x": -56.631357773437514,
            "y": -2.15
          },
          {
            "id": 711,
            "x": -55.54687996093752,
            "y": -2.15
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 1,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 396,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 714,
            "x": -54,
            "y": -5.495040283203125
          },
          {
            "id": 269,
            "x": -54,
            "y": -3.87
          }
        ],
        "keypoints_id": [
          {
            "id": 714,
            "x": -54,
            "y": -5.495040283203125
          },
          {
            "id": 269,
            "x": -54,
            "y": -3.87
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 397,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 365,
            "x": -54,
            "y": -8.211456298828125
          },
          {
            "id": 715,
            "x": -54,
            "y": -6.894719848632812
          }
        ],
        "keypoints_id": [
          {
            "id": 365,
            "x": -54,
            "y": -8.211456298828125
          },
          {
            "id": 715,
            "x": -54,
            "y": -6.894719848632812
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 398,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 715,
            "x": -54,
            "y": -6.894719848632812
          },
          {
            "id": 714,
            "x": -54,
            "y": -5.495040283203125
          }
        ],
        "keypoints_id": [
          {
            "id": 715,
            "x": -54,
            "y": -6.894719848632812
          },
          {
            "id": 714,
            "x": -54,
            "y": -5.495040283203125
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 399,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 268,
            "x": -54,
            "y": -11.850209259765629
          },
          {
            "id": 716,
            "x": -54,
            "y": -10.229760131835938
          }
        ],
        "keypoints_id": [
          {
            "id": 268,
            "x": -54,
            "y": -11.850209259765629
          },
          {
            "id": 716,
            "x": -54,
            "y": -10.229760131835938
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 400,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 716,
            "x": -54,
            "y": -10.229760131835938
          },
          {
            "id": 365,
            "x": -54,
            "y": -8.211456298828125
          }
        ],
        "keypoints_id": [
          {
            "id": 716,
            "x": -54,
            "y": -10.229760131835938
          },
          {
            "id": 365,
            "x": -54,
            "y": -8.211456298828125
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 401,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 717,
            "x": -46,
            "y": -6.151680297851563
          },
          {
            "id": 673,
            "x": -46,
            "y": -3.8600000000000003
          }
        ],
        "keypoints_id": [
          {
            "id": 717,
            "x": -46,
            "y": -6.151680297851563
          },
          {
            "id": 673,
            "x": -46,
            "y": -3.8600000000000003
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "end_to_start",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 402,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 718,
            "x": -46,
            "y": -8.536320190429688
          },
          {
            "id": 717,
            "x": -46,
            "y": -6.151680297851563
          }
        ],
        "keypoints_id": [
          {
            "id": 718,
            "x": -46,
            "y": -8.536320190429688
          },
          {
            "id": 717,
            "x": -46,
            "y": -6.151680297851563
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "end_to_start",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 403,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 719,
            "x": -46,
            "y": -10.60991943359375
          },
          {
            "id": 718,
            "x": -46,
            "y": -8.536320190429688
          }
        ],
        "keypoints_id": [
          {
            "id": 719,
            "x": -46,
            "y": -10.60991943359375
          },
          {
            "id": 718,
            "x": -46,
            "y": -8.536320190429688
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "end_to_start",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 404,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 313,
            "x": -46,
            "y": -12.512467041015626
          },
          {
            "id": 719,
            "x": -46,
            "y": -10.60991943359375
          }
        ],
        "keypoints_id": [
          {
            "id": 313,
            "x": -46,
            "y": -12.512467041015626
          },
          {
            "id": 719,
            "x": -46,
            "y": -10.60991943359375
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "end_to_start",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 405,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 720,
            "x": -46,
            "y": -14.964479980468749
          },
          {
            "id": 313,
            "x": -46,
            "y": -12.512467041015626
          }
        ],
        "keypoints_id": [
          {
            "id": 720,
            "x": -46,
            "y": -14.964479980468749
          },
          {
            "id": 313,
            "x": -46,
            "y": -12.512467041015626
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "end_to_start",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 406,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 721,
            "x": -46,
            "y": -17.08991943359375
          },
          {
            "id": 720,
            "x": -46,
            "y": -14.964479980468749
          }
        ],
        "keypoints_id": [
          {
            "id": 721,
            "x": -46,
            "y": -17.08991943359375
          },
          {
            "id": 720,
            "x": -46,
            "y": -14.964479980468749
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "end_to_start",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 407,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 722,
            "x": -46,
            "y": -19.94112060546875
          },
          {
            "id": 721,
            "x": -46,
            "y": -17.08991943359375
          }
        ],
        "keypoints_id": [
          {
            "id": 722,
            "x": -46,
            "y": -19.94112060546875
          },
          {
            "id": 721,
            "x": -46,
            "y": -17.08991943359375
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "end_to_start",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 408,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 317,
            "x": -46,
            "y": -22.498559414062502
          },
          {
            "id": 722,
            "x": -46,
            "y": -19.94112060546875
          }
        ],
        "keypoints_id": [
          {
            "id": 317,
            "x": -46,
            "y": -22.498559414062502
          },
          {
            "id": 722,
            "x": -46,
            "y": -19.94112060546875
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "end_to_start",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 410,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 596,
            "x": -39,
            "y": -22.041902307581026
          },
          {
            "id": 723,
            "x": -39,
            "y": -20.010240478515627
          }
        ],
        "keypoints_id": [
          {
            "id": 596,
            "x": -39,
            "y": -22.041902307581026
          },
          {
            "id": 723,
            "x": -39,
            "y": -20.010240478515627
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "end_to_start",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 412,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 723,
            "x": -39,
            "y": -20.010240478515627
          },
          {
            "id": 724,
            "x": -39,
            "y": -17.71199951171875
          }
        ],
        "keypoints_id": [
          {
            "id": 723,
            "x": -39,
            "y": -20.010240478515627
          },
          {
            "id": 724,
            "x": -39,
            "y": -17.71199951171875
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "end_to_start",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 413,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 725,
            "x": -39,
            "y": -15.223680419921873
          },
          {
            "id": 325,
            "x": -39,
            "y": -11.4043994140625
          }
        ],
        "keypoints_id": [
          {
            "id": 725,
            "x": -39,
            "y": -15.223680419921873
          },
          {
            "id": 325,
            "x": -39,
            "y": -11.4043994140625
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "end_to_start",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 414,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 724,
            "x": -39,
            "y": -17.71199951171875
          },
          {
            "id": 725,
            "x": -39,
            "y": -15.223680419921873
          }
        ],
        "keypoints_id": [
          {
            "id": 724,
            "x": -39,
            "y": -17.71199951171875
          },
          {
            "id": 725,
            "x": -39,
            "y": -15.223680419921873
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "end_to_start",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 416,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 325,
            "x": -39,
            "y": -11.4043994140625
          },
          {
            "id": 726,
            "x": -39,
            "y": -9.210239868164063
          }
        ],
        "keypoints_id": [
          {
            "id": 325,
            "x": -39,
            "y": -11.4043994140625
          },
          {
            "id": 726,
            "x": -39,
            "y": -9.210239868164063
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "end_to_start",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 417,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 727,
            "x": -39,
            "y": -6.393599853515625
          },
          {
            "id": 670,
            "x": -39,
            "y": -4.17
          }
        ],
        "keypoints_id": [
          {
            "id": 727,
            "x": -39,
            "y": -6.393599853515625
          },
          {
            "id": 670,
            "x": -39,
            "y": -4.17
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "end_to_start",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 418,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 726,
            "x": -39,
            "y": -9.210239868164063
          },
          {
            "id": 727,
            "x": -39,
            "y": -6.393599853515625
          }
        ],
        "keypoints_id": [
          {
            "id": 726,
            "x": -39,
            "y": -9.210239868164063
          },
          {
            "id": 727,
            "x": -39,
            "y": -6.393599853515625
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "end_to_start",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 419,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 728,
            "x": -30,
            "y": -5.840640258789063
          },
          {
            "id": 684,
            "x": -30,
            "y": -3.97
          }
        ],
        "keypoints_id": [
          {
            "id": 728,
            "x": -30,
            "y": -5.840640258789063
          },
          {
            "id": 684,
            "x": -30,
            "y": -3.97
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "end_to_start",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 420,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 286,
            "x": -30,
            "y": -7.865280273437503
          },
          {
            "id": 728,
            "x": -30,
            "y": -5.840640258789063
          }
        ],
        "keypoints_id": [
          {
            "id": 286,
            "x": -30,
            "y": -7.865280273437503
          },
          {
            "id": 728,
            "x": -30,
            "y": -5.840640258789063
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "end_to_start",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 421,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 729,
            "x": -30,
            "y": -15.966719970703128
          },
          {
            "id": 436,
            "x": -30,
            "y": -12.696
          }
        ],
        "keypoints_id": [
          {
            "id": 729,
            "x": -30,
            "y": -15.966719970703128
          },
          {
            "id": 436,
            "x": -30,
            "y": -12.696
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "end_to_start",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 422,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 730,
            "x": -30,
            "y": -18.8352001953125
          },
          {
            "id": 729,
            "x": -30,
            "y": -15.966719970703128
          }
        ],
        "keypoints_id": [
          {
            "id": 730,
            "x": -30,
            "y": -18.8352001953125
          },
          {
            "id": 729,
            "x": -30,
            "y": -15.966719970703128
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "end_to_start",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 423,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 298,
            "x": -30,
            "y": -21.650000000000002
          },
          {
            "id": 730,
            "x": -30,
            "y": -18.8352001953125
          }
        ],
        "keypoints_id": [
          {
            "id": 298,
            "x": -30,
            "y": -21.650000000000002
          },
          {
            "id": 730,
            "x": -30,
            "y": -18.8352001953125
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "end_to_start",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 424,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 278,
            "x": -23.400000000000002,
            "y": -22.2
          },
          {
            "id": 731,
            "x": -23.400000000000002,
            "y": -20.3731201171875
          }
        ],
        "keypoints_id": [
          {
            "id": 278,
            "x": -23.400000000000002,
            "y": -22.2
          },
          {
            "id": 731,
            "x": -23.400000000000002,
            "y": -20.3731201171875
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 425,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 731,
            "x": -23.400000000000002,
            "y": -20.3731201171875
          },
          {
            "id": 237,
            "x": -23.400000000000002,
            "y": -18.498470996093758
          }
        ],
        "keypoints_id": [
          {
            "id": 731,
            "x": -23.400000000000002,
            "y": -20.3731201171875
          },
          {
            "id": 237,
            "x": -23.400000000000002,
            "y": -18.498470996093758
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 426,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 635,
            "x": -22.509479627381985,
            "y": -12.72147022222222
          },
          {
            "id": 732,
            "x": -22.496332833671755,
            "y": -10.032719726562501
          }
        ],
        "keypoints_id": [
          {
            "id": 635,
            "x": -22.509479627381985,
            "y": -12.72147022222222
          },
          {
            "id": 732,
            "x": -22.496332833671755,
            "y": -10.032719726562501
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 427,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 732,
            "x": -22.496332833671755,
            "y": -10.032719726562501
          },
          {
            "id": 733,
            "x": -22.5,
            "y": -8.138880004882813
          }
        ],
        "keypoints_id": [
          {
            "id": 732,
            "x": -22.496332833671755,
            "y": -10.032719726562501
          },
          {
            "id": 733,
            "x": -22.5,
            "y": -8.138880004882813
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 428,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 733,
            "x": -22.5,
            "y": -8.138880004882813
          },
          {
            "id": 734,
            "x": -22.5,
            "y": -6.082559814453125
          }
        ],
        "keypoints_id": [
          {
            "id": 733,
            "x": -22.5,
            "y": -8.138880004882813
          },
          {
            "id": 734,
            "x": -22.5,
            "y": -6.082559814453125
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 429,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 734,
            "x": -22.5,
            "y": -6.082559814453125
          },
          {
            "id": 6,
            "x": -22.5,
            "y": -4.0600000000000005
          }
        ],
        "keypoints_id": [
          {
            "id": 734,
            "x": -22.5,
            "y": -6.082559814453125
          },
          {
            "id": 6,
            "x": -22.5,
            "y": -4.0600000000000005
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 430,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 370,
            "x": -52.98807271374428,
            "y": -21.722824960937498
          },
          {
            "id": 735,
            "x": -52.97759765625,
            "y": -20.074664306640624
          }
        ],
        "keypoints_id": [
          {
            "id": 370,
            "x": -52.98807271374428,
            "y": -21.722824960937498
          },
          {
            "id": 735,
            "x": -52.97759765625,
            "y": -20.074664306640624
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 431,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 735,
            "x": -52.97759765625,
            "y": -20.074664306640624
          },
          {
            "id": 736,
            "x": -52.9631982421875,
            "y": -17.808660888671877
          }
        ],
        "keypoints_id": [
          {
            "id": 735,
            "x": -52.97759765625,
            "y": -20.074664306640624
          },
          {
            "id": 736,
            "x": -52.9631982421875,
            "y": -17.808660888671877
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 432,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 736,
            "x": -52.9631982421875,
            "y": -17.808660888671877
          },
          {
            "id": 273,
            "x": -52.941494140625004,
            "y": -14.392442626953123
          }
        ],
        "keypoints_id": [
          {
            "id": 736,
            "x": -52.9631982421875,
            "y": -17.808660888671877
          },
          {
            "id": 273,
            "x": -52.941494140625004,
            "y": -14.392442626953123
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 433,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 306,
            "x": -34.76,
            "y": -19.05
          },
          {
            "id": 738,
            "x": -34.76,
            "y": -17.39
          }
        ],
        "keypoints_id": [
          {
            "id": 306,
            "x": -34.76,
            "y": -19.05
          },
          {
            "id": 738,
            "x": -34.76,
            "y": -17.39
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 434,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 738,
            "x": -34.76,
            "y": -17.39
          },
          {
            "id": 739,
            "x": -34.76,
            "y": -16.75
          },
          {
            "id": 741,
            "x": -32.545,
            "y": -14.407640380859373
          },
          {
            "id": 740,
            "x": -30,
            "y": -12.985280151367188
          },
          {
            "id": 286,
            "x": -30,
            "y": -7.865280273437503
          }
        ],
        "keypoints_id": [
          {
            "id": 738,
            "x": -34.76,
            "y": -17.39
          },
          {
            "id": 286,
            "x": -30,
            "y": -7.865280273437503
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 435,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 406,
            "x": -41.9,
            "y": -19.2
          },
          {
            "id": 743,
            "x": -41.9,
            "y": -17.06
          }
        ],
        "keypoints_id": [
          {
            "id": 406,
            "x": -41.9,
            "y": -19.2
          },
          {
            "id": 743,
            "x": -41.9,
            "y": -17.06
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 436,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 743,
            "x": -41.9,
            "y": -17.06
          },
          {
            "id": 744,
            "x": -41.9,
            "y": -16.52
          },
          {
            "id": 746,
            "x": -40.695,
            "y": -15.282039794921875
          },
          {
            "id": 745,
            "x": -39,
            "y": -14.59407958984375
          },
          {
            "id": 325,
            "x": -39,
            "y": -11.4043994140625
          }
        ],
        "keypoints_id": [
          {
            "id": 743,
            "x": -41.9,
            "y": -17.06
          },
          {
            "id": 325,
            "x": -39,
            "y": -11.4043994140625
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 437,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 506,
            "x": -49.17,
            "y": -18.8
          },
          {
            "id": 748,
            "x": -49.17,
            "y": -17.40095947265625
          }
        ],
        "keypoints_id": [
          {
            "id": 506,
            "x": -49.17,
            "y": -18.8
          },
          {
            "id": 748,
            "x": -49.17,
            "y": -17.40095947265625
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 438,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 748,
            "x": -49.17,
            "y": -17.40095947265625
          },
          {
            "id": 749,
            "x": -49.17,
            "y": -16.78079947265625
          },
          {
            "id": 751,
            "x": -47.770877265625,
            "y": -15.37431345703125
          },
          {
            "id": 750,
            "x": -46,
            "y": -14.659027441406256
          },
          {
            "id": 313,
            "x": -46,
            "y": -12.512467041015626
          }
        ],
        "keypoints_id": [
          {
            "id": 748,
            "x": -49.17,
            "y": -17.40095947265625
          },
          {
            "id": 313,
            "x": -46,
            "y": -12.512467041015626
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 439,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 106,
            "x": -20.93,
            "y": -19.05
          },
          {
            "id": 753,
            "x": -20.93,
            "y": -18.05201917860243
          }
        ],
        "keypoints_id": [
          {
            "id": 106,
            "x": -20.93,
            "y": -19.05
          },
          {
            "id": 753,
            "x": -20.93,
            "y": -18.05201917860243
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 440,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 753,
            "x": -20.93,
            "y": -18.05201917860243
          },
          {
            "id": 754,
            "x": -20.93,
            "y": -17.33194140082466
          },
          {
            "id": 756,
            "x": -21.61219645182291,
            "y": -16.497169520399297
          },
          {
            "id": 755,
            "x": -22.47935945705652,
            "y": -14.927997639973963
          },
          {
            "id": 635,
            "x": -22.509479627381985,
            "y": -12.72147022222222
          }
        ],
        "keypoints_id": [
          {
            "id": 753,
            "x": -20.93,
            "y": -18.05201917860243
          },
          {
            "id": 635,
            "x": -22.509479627381985,
            "y": -12.72147022222222
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 441,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 206,
            "x": -27.579999999999995,
            "y": -19.14
          },
          {
            "id": 758,
            "x": -27.579999999999995,
            "y": -17.755266666666675
          }
        ],
        "keypoints_id": [
          {
            "id": 206,
            "x": -27.579999999999995,
            "y": -19.14
          },
          {
            "id": 758,
            "x": -27.579999999999995,
            "y": -17.755266666666675
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 442,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 758,
            "x": -27.579999999999995,
            "y": -17.755266666666675
          },
          {
            "id": 759,
            "x": -27.579999999999995,
            "y": -17.265233333333327
          },
          {
            "id": 761,
            "x": -29.04890000000001,
            "y": -15.74366715494791
          },
          {
            "id": 760,
            "x": -30,
            "y": -14.14399975585938
          },
          {
            "id": 436,
            "x": -30,
            "y": -12.696
          }
        ],
        "keypoints_id": [
          {
            "id": 758,
            "x": -27.579999999999995,
            "y": -17.755266666666675
          },
          {
            "id": 436,
            "x": -30,
            "y": -12.696
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 443,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 762,
            "x": -61.95,
            "y": -12.083040000000004
          },
          {
            "id": 763,
            "x": -61.95,
            "y": -4.798611145019532
          }
        ],
        "keypoints_id": [
          {
            "id": 762,
            "x": -61.95,
            "y": -12.083040000000004
          },
          {
            "id": 763,
            "x": -61.95,
            "y": -4.798611145019532
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 444,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 705,
            "x": -62.789999999999985,
            "y": -19.17
          },
          {
            "id": 765,
            "x": -62.79,
            "y": -17.752959999999998
          }
        ],
        "keypoints_id": [
          {
            "id": 705,
            "x": -62.789999999999985,
            "y": -19.17
          },
          {
            "id": 765,
            "x": -62.79,
            "y": -17.752959999999998
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 445,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 601,
            "x": -56.59000000000001,
            "y": -4.8
          },
          {
            "id": 767,
            "x": -56.59000000000001,
            "y": -3.56
          }
        ],
        "keypoints_id": [
          {
            "id": 601,
            "x": -56.59000000000001,
            "y": -4.8
          },
          {
            "id": 767,
            "x": -56.59000000000001,
            "y": -3.56
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 446,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 602,
            "x": -57.79,
            "y": -4.8
          },
          {
            "id": 769,
            "x": -57.79,
            "y": -3.56
          }
        ],
        "keypoints_id": [
          {
            "id": 602,
            "x": -57.79,
            "y": -4.8
          },
          {
            "id": 769,
            "x": -57.79,
            "y": -3.56
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 447,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 603,
            "x": -58.989999999999995,
            "y": -4.8
          },
          {
            "id": 771,
            "x": -58.989999999999995,
            "y": -3.56
          }
        ],
        "keypoints_id": [
          {
            "id": 603,
            "x": -58.989999999999995,
            "y": -4.8
          },
          {
            "id": 771,
            "x": -58.989999999999995,
            "y": -3.56
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 448,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 604,
            "x": -60.19,
            "y": -4.8
          },
          {
            "id": 773,
            "x": -60.19,
            "y": -3.56
          }
        ],
        "keypoints_id": [
          {
            "id": 604,
            "x": -60.19,
            "y": -4.8
          },
          {
            "id": 773,
            "x": -60.19,
            "y": -3.56
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 450,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 774,
            "x": -57.8838767795139,
            "y": -2.15
          },
          {
            "id": 712,
            "x": -56.631357773437514,
            "y": -2.15
          }
        ],
        "keypoints_id": [
          {
            "id": 774,
            "x": -57.8838767795139,
            "y": -2.15
          },
          {
            "id": 712,
            "x": -56.631357773437514,
            "y": -2.15
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 1,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 452,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 775,
            "x": -59.07985302083334,
            "y": -2.15
          },
          {
            "id": 774,
            "x": -57.8838767795139,
            "y": -2.15
          }
        ],
        "keypoints_id": [
          {
            "id": 775,
            "x": -59.07985302083334,
            "y": -2.15
          },
          {
            "id": 774,
            "x": -57.8838767795139,
            "y": -2.15
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 1,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 454,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 776,
            "x": -60.33839984375,
            "y": -2.15
          },
          {
            "id": 775,
            "x": -59.07985302083334,
            "y": -2.15
          }
        ],
        "keypoints_id": [
          {
            "id": 776,
            "x": -60.33839984375,
            "y": -2.15
          },
          {
            "id": 775,
            "x": -59.07985302083334,
            "y": -2.15
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 1,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 456,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 777,
            "x": -63.743278437500024,
            "y": -0.75
          },
          {
            "id": 634,
            "x": -65.24141312000002,
            "y": -0.75
          }
        ],
        "keypoints_id": [
          {
            "id": 777,
            "x": -63.743278437500024,
            "y": -0.75
          },
          {
            "id": 634,
            "x": -65.24141312000002,
            "y": -0.75
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 1,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 457,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 767,
            "x": -56.59000000000001,
            "y": -3.56
          },
          {
            "id": 778,
            "x": -56.59000000000001,
            "y": -3.117317777777777
          },
          {
            "id": 780,
            "x": -56.36404464409727,
            "y": -2.4459422222222225
          },
          {
            "id": 779,
            "x": -55.8929999609375,
            "y": -2.15
          },
          {
            "id": 711,
            "x": -55.54687996093752,
            "y": -2.15
          }
        ],
        "keypoints_id": [
          {
            "id": 767,
            "x": -56.59000000000001,
            "y": -3.56
          },
          {
            "id": 711,
            "x": -55.54687996093752,
            "y": -2.15
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 458,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 769,
            "x": -57.79,
            "y": -3.56
          },
          {
            "id": 781,
            "x": -57.79,
            "y": -3.2403675555555536
          },
          {
            "id": 783,
            "x": -57.61626688368058,
            "y": -2.4008122222222203
          },
          {
            "id": 782,
            "x": -57.07731555121527,
            "y": -2.15
          },
          {
            "id": 712,
            "x": -56.631357773437514,
            "y": -2.15
          }
        ],
        "keypoints_id": [
          {
            "id": 769,
            "x": -57.79,
            "y": -3.56
          },
          {
            "id": 712,
            "x": -56.631357773437514,
            "y": -2.15
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 459,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 771,
            "x": -58.989999999999995,
            "y": -3.56
          },
          {
            "id": 784,
            "x": -58.989999999999995,
            "y": -3.096097777777777
          },
          {
            "id": 786,
            "x": -58.7765811675347,
            "y": -2.4229077777777794
          },
          {
            "id": 785,
            "x": -58.264854557291656,
            "y": -2.15
          },
          {
            "id": 774,
            "x": -57.8838767795139,
            "y": -2.15
          }
        ],
        "keypoints_id": [
          {
            "id": 771,
            "x": -58.989999999999995,
            "y": -3.56
          },
          {
            "id": 774,
            "x": -57.8838767795139,
            "y": -2.15
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 460,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 773,
            "x": -60.19,
            "y": -3.56
          },
          {
            "id": 787,
            "x": -60.19,
            "y": -3.1024999999999983
          },
          {
            "id": 789,
            "x": -59.995714288194435,
            "y": -2.4358733333333342
          },
          {
            "id": 788,
            "x": -59.4301996875,
            "y": -2.15
          },
          {
            "id": 775,
            "x": -59.07985302083334,
            "y": -2.15
          }
        ],
        "keypoints_id": [
          {
            "id": 773,
            "x": -60.19,
            "y": -3.56
          },
          {
            "id": 775,
            "x": -59.07985302083334,
            "y": -2.15
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 461,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 763,
            "x": -61.95,
            "y": -4.798611145019532
          },
          {
            "id": 790,
            "x": -61.95,
            "y": -3.634131145019532
          },
          {
            "id": 792,
            "x": -61.723799921875,
            "y": -2.5932254199218767
          },
          {
            "id": 791,
            "x": -60.99455984374997,
            "y": -2.15
          },
          {
            "id": 776,
            "x": -60.33839984375,
            "y": -2.15
          }
        ],
        "keypoints_id": [
          {
            "id": 763,
            "x": -61.95,
            "y": -4.798611145019532
          },
          {
            "id": 776,
            "x": -60.33839984375,
            "y": -2.15
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 462,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 763,
            "x": -61.95,
            "y": -4.798611145019532
          },
          {
            "id": 793,
            "x": -61.95,
            "y": -1.9838911450195316
          },
          {
            "id": 795,
            "x": -62.31323921875001,
            "y": -1.3027854199218754
          },
          {
            "id": 794,
            "x": -63.12575843750001,
            "y": -0.75
          },
          {
            "id": 777,
            "x": -63.743278437500024,
            "y": -0.75
          }
        ],
        "keypoints_id": [
          {
            "id": 763,
            "x": -61.95,
            "y": -4.798611145019532
          },
          {
            "id": 777,
            "x": -63.743278437500024,
            "y": -0.75
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 463,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 830,
            "x": -58.64920062499999,
            "y": -0.75
          },
          {
            "id": 796,
            "x": -59.719680781250005,
            "y": -0.75
          }
        ],
        "keypoints_id": [
          {
            "id": 830,
            "x": -58.64920062499999,
            "y": -0.75
          },
          {
            "id": 796,
            "x": -59.719680781250005,
            "y": -0.75
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 1,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 464,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 796,
            "x": -59.719680781250005,
            "y": -0.75
          },
          {
            "id": 713,
            "x": -60.946561406250034,
            "y": -0.75
          }
        ],
        "keypoints_id": [
          {
            "id": 796,
            "x": -59.719680781250005,
            "y": -0.75
          },
          {
            "id": 713,
            "x": -60.946561406250034,
            "y": -0.75
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 1,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 465,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 713,
            "x": -60.946561406250034,
            "y": -0.75
          },
          {
            "id": 797,
            "x": -62.328961328125004,
            "y": -0.75
          }
        ],
        "keypoints_id": [
          {
            "id": 713,
            "x": -60.946561406250034,
            "y": -0.75
          },
          {
            "id": 797,
            "x": -62.328961328125004,
            "y": -0.75
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 1,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 466,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 797,
            "x": -62.328961328125004,
            "y": -0.75
          },
          {
            "id": 777,
            "x": -63.743278437500024,
            "y": -0.75
          }
        ],
        "keypoints_id": [
          {
            "id": 797,
            "x": -62.328961328125004,
            "y": -0.75
          },
          {
            "id": 777,
            "x": -63.743278437500024,
            "y": -0.75
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 1,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 467,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 767,
            "x": -56.59000000000001,
            "y": -3.56
          },
          {
            "id": 798,
            "x": -56.59000000000001,
            "y": -2.1183360000000007
          },
          {
            "id": 800,
            "x": -57.05229631249998,
            "y": -1.095579200000001
          },
          {
            "id": 799,
            "x": -58.09536062500001,
            "y": -0.75
          },
          {
            "id": 830,
            "x": -58.64920062499999,
            "y": -0.75
          }
        ],
        "keypoints_id": [
          {
            "id": 767,
            "x": -56.59000000000001,
            "y": -3.56
          },
          {
            "id": 830,
            "x": -58.64920062499999,
            "y": -0.75
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 468,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 769,
            "x": -57.79,
            "y": -3.56
          },
          {
            "id": 1220,
            "x": -57.79,
            "y": -1.8530400000000007
          },
          {
            "id": 1164,
            "x": -58.39556039062499,
            "y": -1.24644
          },
          {
            "id": 1221,
            "x": -59.17952078124999,
            "y": -0.75
          },
          {
            "id": 796,
            "x": -59.719680781250005,
            "y": -0.75
          }
        ],
        "keypoints_id": [
          {
            "id": 769,
            "x": -57.79,
            "y": -3.56
          },
          {
            "id": 796,
            "x": -59.719680781250005,
            "y": -0.75
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 470,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 773,
            "x": -60.19,
            "y": -3.56
          },
          {
            "id": 807,
            "x": -60.19,
            "y": -2.1857600000000006
          },
          {
            "id": 809,
            "x": -60.73220066406249,
            "y": -1.3000400000000005
          },
          {
            "id": 808,
            "x": -61.70880132812498,
            "y": -0.75
          },
          {
            "id": 797,
            "x": -62.328961328125004,
            "y": -0.75
          }
        ],
        "keypoints_id": [
          {
            "id": 773,
            "x": -60.19,
            "y": -3.56
          },
          {
            "id": 797,
            "x": -62.328961328125004,
            "y": -0.75
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 471,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 765,
            "x": -62.79,
            "y": -17.752959999999998
          },
          {
            "id": 810,
            "x": -62.79,
            "y": -15.92992
          },
          {
            "id": 812,
            "x": -62.534120000000016,
            "y": -14.774479999999997
          },
          {
            "id": 811,
            "x": -61.95,
            "y": -13.74
          },
          {
            "id": 762,
            "x": -61.95,
            "y": -12.083040000000004
          }
        ],
        "keypoints_id": [
          {
            "id": 765,
            "x": -62.79,
            "y": -17.752959999999998
          },
          {
            "id": 762,
            "x": -61.95,
            "y": -12.083040000000004
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 472,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 606,
            "x": -58.52,
            "y": -18.92
          },
          {
            "id": 814,
            "x": -58.510000000000005,
            "y": -16.29
          }
        ],
        "keypoints_id": [
          {
            "id": 606,
            "x": -58.52,
            "y": -18.92
          },
          {
            "id": 814,
            "x": -58.510000000000005,
            "y": -16.29
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 473,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 706,
            "x": -63.84000000000001,
            "y": -19.17
          },
          {
            "id": 816,
            "x": -63.84000000000001,
            "y": -17.7535205078125
          }
        ],
        "keypoints_id": [
          {
            "id": 706,
            "x": -63.84000000000001,
            "y": -19.17
          },
          {
            "id": 816,
            "x": -63.84000000000001,
            "y": -17.7535205078125
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 474,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 816,
            "x": -63.84000000000001,
            "y": -17.7535205078125
          },
          {
            "id": 817,
            "x": -63.84000000000001,
            "y": -16.7635205078125
          },
          {
            "id": 819,
            "x": -63.17,
            "y": -15.353280029296878
          },
          {
            "id": 818,
            "x": -61.95,
            "y": -14.423039550781251
          },
          {
            "id": 762,
            "x": -61.95,
            "y": -12.083040000000004
          }
        ],
        "keypoints_id": [
          {
            "id": 816,
            "x": -63.84000000000001,
            "y": -17.7535205078125
          },
          {
            "id": 762,
            "x": -61.95,
            "y": -12.083040000000004
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 476,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 1,
            "x": -11.23,
            "y": -6.0200000000000005
          },
          {
            "id": 833,
            "x": -11.23,
            "y": -3.9
          }
        ],
        "keypoints_id": [
          {
            "id": 1,
            "x": -11.23,
            "y": -6.0200000000000005
          },
          {
            "id": 833,
            "x": -11.23,
            "y": -3.9
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "end_to_start",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 478,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 2,
            "x": -9.45,
            "y": -6.01
          },
          {
            "id": 838,
            "x": -9.45,
            "y": -3.9
          }
        ],
        "keypoints_id": [
          {
            "id": 2,
            "x": -9.45,
            "y": -6.01
          },
          {
            "id": 838,
            "x": -9.45,
            "y": -3.9
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "end_to_start",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 479,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 824,
            "x": -11.45552001953125,
            "y": -2.15
          },
          {
            "id": 840,
            "x": -9.63,
            "y": -2.15
          }
        ],
        "keypoints_id": [
          {
            "id": 824,
            "x": -11.45552001953125,
            "y": -2.15
          },
          {
            "id": 840,
            "x": -9.63,
            "y": -2.15
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 1,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 484,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 6,
            "x": -22.5,
            "y": -4.0600000000000005
          },
          {
            "id": 861,
            "x": -22.5,
            "y": -2.2055199999999995
          },
          {
            "id": 863,
            "x": -22.8646671875,
            "y": -1.25
          },
          {
            "id": 862,
            "x": -23.86005193359375,
            "y": -0.75
          },
          {
            "id": 67,
            "x": -24.336212747395834,
            "y": -0.75
          }
        ],
        "keypoints_id": [
          {
            "id": 6,
            "x": -22.5,
            "y": -4.0600000000000005
          },
          {
            "id": 67,
            "x": -24.336212747395834,
            "y": -0.75
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 498,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 1267,
            "x": -21.01,
            "y": -14.0444
          },
          {
            "id": 892,
            "x": -21.01,
            "y": -13.41166705729167
          }
        ],
        "keypoints_id": [
          {
            "id": 1267,
            "x": -21.01,
            "y": -14.0444
          },
          {
            "id": 892,
            "x": -21.01,
            "y": -13.41166705729167
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 499,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 892,
            "x": -21.01,
            "y": -13.41166705729167
          },
          {
            "id": 893,
            "x": -21.01,
            "y": -12.897400797526043
          },
          {
            "id": 895,
            "x": -21.777585937500007,
            "y": -12.266058430989583
          },
          {
            "id": 894,
            "x": -22.503775478659414,
            "y": -11.554383951822913
          },
          {
            "id": 732,
            "x": -22.496332833671755,
            "y": -10.032719726562501
          }
        ],
        "keypoints_id": [
          {
            "id": 892,
            "x": -21.01,
            "y": -13.41166705729167
          },
          {
            "id": 732,
            "x": -22.496332833671755,
            "y": -10.032719726562501
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": 2,
        "back_safe_level": -1,
        "left_safe_level": 2,
        "right_safe_level": 2,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 500,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 701,
            "x": -63.55499999999999,
            "y": -4.88
          },
          {
            "id": 897,
            "x": -63.55499999999999,
            "y": -3.56
          }
        ],
        "keypoints_id": [
          {
            "id": 701,
            "x": -63.55499999999999,
            "y": -4.88
          },
          {
            "id": 897,
            "x": -63.55499999999999,
            "y": -3.56
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 501,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 702,
            "x": -64.675,
            "y": -4.88
          },
          {
            "id": 899,
            "x": -64.675,
            "y": -3.56
          }
        ],
        "keypoints_id": [
          {
            "id": 702,
            "x": -64.675,
            "y": -4.88
          },
          {
            "id": 899,
            "x": -64.675,
            "y": -3.56
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 509,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 924,
            "x": -62.146000000000015,
            "y": -2.15
          },
          {
            "id": 776,
            "x": -60.33839984375,
            "y": -2.15
          }
        ],
        "keypoints_id": [
          {
            "id": 924,
            "x": -62.146000000000015,
            "y": -2.15
          },
          {
            "id": 776,
            "x": -60.33839984375,
            "y": -2.15
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 1,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 511,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 925,
            "x": -63.27,
            "y": -2.15
          },
          {
            "id": 924,
            "x": -62.146000000000015,
            "y": -2.15
          }
        ],
        "keypoints_id": [
          {
            "id": 925,
            "x": -63.27,
            "y": -2.15
          },
          {
            "id": 924,
            "x": -62.146000000000015,
            "y": -2.15
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 1,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 513,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 897,
            "x": -63.55499999999999,
            "y": -3.56
          },
          {
            "id": 927,
            "x": -63.55499999999999,
            "y": -3.1597500000000007
          },
          {
            "id": 929,
            "x": -63.27658333333333,
            "y": -2.5235833333333337
          },
          {
            "id": 928,
            "x": -62.67733333333331,
            "y": -2.15
          },
          {
            "id": 924,
            "x": -62.146000000000015,
            "y": -2.15
          }
        ],
        "keypoints_id": [
          {
            "id": 897,
            "x": -63.55499999999999,
            "y": -3.56
          },
          {
            "id": 924,
            "x": -62.146000000000015,
            "y": -2.15
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 514,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 899,
            "x": -64.675,
            "y": -3.56
          },
          {
            "id": 930,
            "x": -64.675,
            "y": -3.1198611111111108
          },
          {
            "id": 932,
            "x": -64.39733333333334,
            "y": -2.4986388888888884
          },
          {
            "id": 931,
            "x": -63.823888888888895,
            "y": -2.15
          },
          {
            "id": 925,
            "x": -63.27,
            "y": -2.15
          }
        ],
        "keypoints_id": [
          {
            "id": 899,
            "x": -64.675,
            "y": -3.56
          },
          {
            "id": 925,
            "x": -63.27,
            "y": -2.15
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 515,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 634,
            "x": -65.24141312000002,
            "y": -0.75
          },
          {
            "id": 933,
            "x": -66.4,
            "y": -0.75
          }
        ],
        "keypoints_id": [
          {
            "id": 634,
            "x": -65.24141312000002,
            "y": -0.75
          },
          {
            "id": 933,
            "x": -66.4,
            "y": -0.75
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 1,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 517,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 899,
            "x": -64.675,
            "y": -3.56
          },
          {
            "id": 934,
            "x": -64.675,
            "y": -1.94
          },
          {
            "id": 936,
            "x": -65.04,
            "y": -1.2750000000000001
          },
          {
            "id": 935,
            "x": -65.91,
            "y": -0.75
          },
          {
            "id": 933,
            "x": -66.4,
            "y": -0.75
          }
        ],
        "keypoints_id": [
          {
            "id": 899,
            "x": -64.675,
            "y": -3.56
          },
          {
            "id": 933,
            "x": -66.4,
            "y": -0.75
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 518,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 897,
            "x": -63.55499999999999,
            "y": -3.56
          },
          {
            "id": 937,
            "x": -63.55499999999999,
            "y": -1.8800000000000001
          },
          {
            "id": 939,
            "x": -63.875703124999994,
            "y": -1.245
          },
          {
            "id": 938,
            "x": -64.7014111328125,
            "y": -0.75
          },
          {
            "id": 634,
            "x": -65.24141312000002,
            "y": -0.75
          }
        ],
        "keypoints_id": [
          {
            "id": 897,
            "x": -63.55499999999999,
            "y": -3.56
          },
          {
            "id": 634,
            "x": -65.24141312000002,
            "y": -0.75
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 519,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 1735,
            "x": -37.86,
            "y": -14.74
          },
          {
            "id": 949,
            "x": -37.86,
            "y": -14.180000000000003
          }
        ],
        "keypoints_id": [
          {
            "id": 1735,
            "x": -37.86,
            "y": -14.74
          },
          {
            "id": 949,
            "x": -37.86,
            "y": -14.180000000000003
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 520,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 949,
            "x": -37.86,
            "y": -14.180000000000003
          },
          {
            "id": 950,
            "x": -37.86,
            "y": -13.74
          },
          {
            "id": 952,
            "x": -38.42,
            "y": -13.177039794921878
          },
          {
            "id": 951,
            "x": -39,
            "y": -12.51407958984375
          },
          {
            "id": 325,
            "x": -39,
            "y": -11.4043994140625
          }
        ],
        "keypoints_id": [
          {
            "id": 949,
            "x": -37.86,
            "y": -14.180000000000003
          },
          {
            "id": 325,
            "x": -39,
            "y": -11.4043994140625
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 521,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 953,
            "x": -44.61,
            "y": -14.75
          },
          {
            "id": 954,
            "x": -44.61,
            "y": -14.080000000000002
          }
        ],
        "keypoints_id": [
          {
            "id": 953,
            "x": -44.61,
            "y": -14.75
          },
          {
            "id": 954,
            "x": -44.61,
            "y": -14.080000000000002
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 522,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 954,
            "x": -44.61,
            "y": -14.080000000000002
          },
          {
            "id": 955,
            "x": -44.61,
            "y": -13.66
          },
          {
            "id": 957,
            "x": -45.225,
            "y": -12.684959716796875
          },
          {
            "id": 956,
            "x": -46,
            "y": -11.68991943359375
          },
          {
            "id": 719,
            "x": -46,
            "y": -10.60991943359375
          }
        ],
        "keypoints_id": [
          {
            "id": 954,
            "x": -44.61,
            "y": -14.080000000000002
          },
          {
            "id": 719,
            "x": -46,
            "y": -10.60991943359375
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 523,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 0,
            "x": -12.98,
            "y": -6.01
          },
          {
            "id": 959,
            "x": -12.98,
            "y": -3.9
          }
        ],
        "keypoints_id": [
          {
            "id": 0,
            "x": -12.98,
            "y": -6.01
          },
          {
            "id": 959,
            "x": -12.98,
            "y": -3.9
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 525,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 833,
            "x": -11.23,
            "y": -3.9
          },
          {
            "id": 963,
            "x": -11.23,
            "y": -3.55
          },
          {
            "id": 965,
            "x": -10.967000000000002,
            "y": -2.606
          },
          {
            "id": 964,
            "x": -10.07,
            "y": -2.15
          },
          {
            "id": 840,
            "x": -9.63,
            "y": -2.15
          }
        ],
        "keypoints_id": [
          {
            "id": 833,
            "x": -11.23,
            "y": -3.9
          },
          {
            "id": 840,
            "x": -9.63,
            "y": -2.15
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 526,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 959,
            "x": -12.98,
            "y": -3.9
          },
          {
            "id": 966,
            "x": -12.98,
            "y": -3.304000000000001
          },
          {
            "id": 968,
            "x": -12.677760009765626,
            "y": -2.5160000000000005
          },
          {
            "id": 967,
            "x": -11.90552001953125,
            "y": -2.15
          },
          {
            "id": 824,
            "x": -11.45552001953125,
            "y": -2.15
          }
        ],
        "keypoints_id": [
          {
            "id": 959,
            "x": -12.98,
            "y": -3.9
          },
          {
            "id": 824,
            "x": -11.45552001953125,
            "y": -2.15
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 527,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 838,
            "x": -9.45,
            "y": -3.9
          },
          {
            "id": 969,
            "x": -9.45,
            "y": -2.05
          },
          {
            "id": 971,
            "x": -9.677559814453126,
            "y": -1.245
          },
          {
            "id": 970,
            "x": -10.473279628906251,
            "y": -0.75
          },
          {
            "id": 699,
            "x": -11.005120239257813,
            "y": -0.75
          }
        ],
        "keypoints_id": [
          {
            "id": 838,
            "x": -9.45,
            "y": -3.9
          },
          {
            "id": 699,
            "x": -11.005120239257813,
            "y": -0.75
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 528,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 833,
            "x": -11.23,
            "y": -3.9
          },
          {
            "id": 972,
            "x": -11.23,
            "y": -2.05
          },
          {
            "id": 974,
            "x": -11.458759765625002,
            "y": -1.25
          },
          {
            "id": 973,
            "x": -12.21751953125,
            "y": -0.75
          },
          {
            "id": 820,
            "x": -12.748399531250005,
            "y": -0.75
          }
        ],
        "keypoints_id": [
          {
            "id": 833,
            "x": -11.23,
            "y": -3.9
          },
          {
            "id": 820,
            "x": -12.748399531250005,
            "y": -0.75
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 529,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 959,
            "x": -12.98,
            "y": -3.9
          },
          {
            "id": 975,
            "x": -12.98,
            "y": -2.05
          },
          {
            "id": 977,
            "x": -13.229959863281252,
            "y": -1.25
          },
          {
            "id": 976,
            "x": -14.028159726562501,
            "y": -0.75
          },
          {
            "id": 825,
            "x": -14.545439726562503,
            "y": -0.75
          }
        ],
        "keypoints_id": [
          {
            "id": 959,
            "x": -12.98,
            "y": -3.9
          },
          {
            "id": 825,
            "x": -14.545439726562503,
            "y": -0.75
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 538,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 703,
            "x": -65.79,
            "y": -4.91
          },
          {
            "id": 1060,
            "x": -65.79,
            "y": -3.56
          }
        ],
        "keypoints_id": [
          {
            "id": 703,
            "x": -65.79,
            "y": -4.91
          },
          {
            "id": 1060,
            "x": -65.79,
            "y": -3.56
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 539,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 704,
            "x": -66.97,
            "y": -4.91
          },
          {
            "id": 1062,
            "x": -66.97,
            "y": -3.56
          }
        ],
        "keypoints_id": [
          {
            "id": 704,
            "x": -66.97,
            "y": -4.91
          },
          {
            "id": 1062,
            "x": -66.97,
            "y": -3.56
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 540,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 1063,
            "x": -64.47200000000001,
            "y": -2.15
          },
          {
            "id": 925,
            "x": -63.27,
            "y": -2.15
          }
        ],
        "keypoints_id": [
          {
            "id": 1063,
            "x": -64.47200000000001,
            "y": -2.15
          },
          {
            "id": 925,
            "x": -63.27,
            "y": -2.15
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 1,
        "min_speed": 0.2,
        "agv_direction": "end_to_start",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 541,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 1064,
            "x": -65.58600000000001,
            "y": -2.15
          },
          {
            "id": 1063,
            "x": -64.47200000000001,
            "y": -2.15
          }
        ],
        "keypoints_id": [
          {
            "id": 1064,
            "x": -65.58600000000001,
            "y": -2.15
          },
          {
            "id": 1063,
            "x": -64.47200000000001,
            "y": -2.15
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 1,
        "min_speed": 0.2,
        "agv_direction": "end_to_start",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 543,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 933,
            "x": -66.4,
            "y": -0.75
          },
          {
            "id": 1065,
            "x": -67.39,
            "y": -0.75
          }
        ],
        "keypoints_id": [
          {
            "id": 933,
            "x": -66.4,
            "y": -0.75
          },
          {
            "id": 1065,
            "x": -67.39,
            "y": -0.75
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 1,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 544,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 1065,
            "x": -67.39,
            "y": -0.75
          },
          {
            "id": 1066,
            "x": -68.37,
            "y": -0.75
          }
        ],
        "keypoints_id": [
          {
            "id": 1065,
            "x": -67.39,
            "y": -0.75
          },
          {
            "id": 1066,
            "x": -68.37,
            "y": -0.75
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 1,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 546,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 1060,
            "x": -65.79,
            "y": -3.56
          },
          {
            "id": 1067,
            "x": -65.79,
            "y": -3.079250000000001
          },
          {
            "id": 1069,
            "x": -65.53830555555555,
            "y": -2.468527777777778
          },
          {
            "id": 1068,
            "x": -64.93055555555556,
            "y": -2.15
          },
          {
            "id": 1063,
            "x": -64.47200000000001,
            "y": -2.15
          }
        ],
        "keypoints_id": [
          {
            "id": 1060,
            "x": -65.79,
            "y": -3.56
          },
          {
            "id": 1063,
            "x": -64.47200000000001,
            "y": -2.15
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 547,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 1062,
            "x": -66.97,
            "y": -3.56
          },
          {
            "id": 1070,
            "x": -66.97,
            "y": -3.1164444444444452
          },
          {
            "id": 1072,
            "x": -66.73116666666664,
            "y": -2.453694444444445
          },
          {
            "id": 1071,
            "x": -66.08297222222222,
            "y": -2.15
          },
          {
            "id": 1064,
            "x": -65.58600000000001,
            "y": -2.15
          }
        ],
        "keypoints_id": [
          {
            "id": 1062,
            "x": -66.97,
            "y": -3.56
          },
          {
            "id": 1064,
            "x": -65.58600000000001,
            "y": -2.15
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 548,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 1060,
            "x": -65.79,
            "y": -3.56
          },
          {
            "id": 1073,
            "x": -65.79,
            "y": -1.9
          },
          {
            "id": 1075,
            "x": -66.17,
            "y": -1.2550000000000001
          },
          {
            "id": 1074,
            "x": -66.94,
            "y": -0.75
          },
          {
            "id": 1065,
            "x": -67.39,
            "y": -0.75
          }
        ],
        "keypoints_id": [
          {
            "id": 1060,
            "x": -65.79,
            "y": -3.56
          },
          {
            "id": 1065,
            "x": -67.39,
            "y": -0.75
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 549,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 1062,
            "x": -66.97,
            "y": -3.56
          },
          {
            "id": 1076,
            "x": -66.97,
            "y": -2.07
          },
          {
            "id": 1078,
            "x": -67.25,
            "y": -1.295
          },
          {
            "id": 1077,
            "x": -67.93,
            "y": -0.75
          },
          {
            "id": 1066,
            "x": -68.37,
            "y": -0.75
          }
        ],
        "keypoints_id": [
          {
            "id": 1062,
            "x": -66.97,
            "y": -3.56
          },
          {
            "id": 1066,
            "x": -68.37,
            "y": -0.75
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 578,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 1146,
            "x": -72.127,
            "y": -0.95
          },
          {
            "id": 1147,
            "x": -69.796,
            "y": -0.95
          }
        ],
        "keypoints_id": [
          {
            "id": 1146,
            "x": -72.127,
            "y": -0.95
          },
          {
            "id": 1147,
            "x": -69.796,
            "y": -0.95
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 580,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 1147,
            "x": -69.796,
            "y": -0.95
          },
          {
            "id": 1151,
            "x": -69.12440000000001,
            "y": -1.013041399416908
          },
          {
            "id": 1153,
            "x": -67.574,
            "y": -2.139
          },
          {
            "id": 1152,
            "x": -66.2,
            "y": -2.15
          },
          {
            "id": 1064,
            "x": -65.58600000000001,
            "y": -2.15
          }
        ],
        "keypoints_id": [
          {
            "id": 1147,
            "x": -69.796,
            "y": -0.95
          },
          {
            "id": 1064,
            "x": -65.58600000000001,
            "y": -2.15
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 581,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 1143,
            "x": -70.73,
            "y": -1.8394
          },
          {
            "id": 1154,
            "x": -70.73,
            "y": -1.6115999999999997
          },
          {
            "id": 1156,
            "x": -70.57999999999998,
            "y": -1.1412
          },
          {
            "id": 1155,
            "x": -70.11800000000001,
            "y": -0.95
          },
          {
            "id": 1147,
            "x": -69.796,
            "y": -0.95
          }
        ],
        "keypoints_id": [
          {
            "id": 1143,
            "x": -70.73,
            "y": -1.8394
          },
          {
            "id": 1147,
            "x": -69.796,
            "y": -0.95
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 582,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 1145,
            "x": -73.31,
            "y": -1.935
          },
          {
            "id": 1157,
            "x": -73.31,
            "y": -1.65
          },
          {
            "id": 1159,
            "x": -73.16560000000001,
            "y": -1.1488
          },
          {
            "id": 1158,
            "x": -72.5006,
            "y": -0.95
          },
          {
            "id": 1146,
            "x": -72.127,
            "y": -0.95
          }
        ],
        "keypoints_id": [
          {
            "id": 1145,
            "x": -73.31,
            "y": -1.935
          },
          {
            "id": 1146,
            "x": -72.127,
            "y": -0.95
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 469,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 771,
            "x": -58.989999999999995,
            "y": -3.56
          },
          {
            "id": 1163,
            "x": -58.989999999999995,
            "y": -1.989520000000001
          },
          {
            "id": 806,
            "x": -59.43276070312503,
            "y": -1.2400400000000005
          },
          {
            "id": 805,
            "x": -60.27456140624999,
            "y": -0.75
          },
          {
            "id": 713,
            "x": -60.946561406250034,
            "y": -0.75
          }
        ],
        "keypoints_id": [
          {
            "id": 771,
            "x": -58.989999999999995,
            "y": -3.56
          },
          {
            "id": 713,
            "x": -60.946561406250034,
            "y": -0.75
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "laser_reflector",
        "car_profile": "1"
      },
      {
        "id": 584,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 804,
            "x": -70.73,
            "y": -2.7
          },
          {
            "id": 1143,
            "x": -70.73,
            "y": -1.8394
          }
        ],
        "keypoints_id": [
          {
            "id": 804,
            "x": -70.73,
            "y": -2.7
          },
          {
            "id": 1143,
            "x": -70.73,
            "y": -1.8394
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "end_to_start",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 585,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 803,
            "x": -73.31289351851852,
            "y": -2.7299999999999995
          },
          {
            "id": 1145,
            "x": -73.31,
            "y": -1.935
          }
        ],
        "keypoints_id": [
          {
            "id": 803,
            "x": -73.31289351851852,
            "y": -2.7299999999999995
          },
          {
            "id": 1145,
            "x": -73.31,
            "y": -1.935
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "end_to_start",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 591,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 777,
            "x": -63.743278437500024,
            "y": -0.75
          },
          {
            "id": 1175,
            "x": -64.36727636718751,
            "y": -0.75
          },
          {
            "id": 1177,
            "x": -65.90244140625,
            "y": -0.375
          },
          {
            "id": 1176,
            "x": -67.5384015625,
            "y": 0.2
          },
          {
            "id": 1173,
            "x": -68.6592001953125,
            "y": 0.2
          }
        ],
        "keypoints_id": [
          {
            "id": 777,
            "x": -63.743278437500024,
            "y": -0.75
          },
          {
            "id": 1173,
            "x": -68.6592001953125,
            "y": 0.2
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 593,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 1174,
            "x": -75.63,
            "y": 0.2
          },
          {
            "id": 1180,
            "x": -76.0524,
            "y": 0.2
          },
          {
            "id": 1182,
            "x": -77.5814,
            "y": -0.13520000610351587
          },
          {
            "id": 1181,
            "x": -77.8,
            "y": -1.5072000122070313
          },
          {
            "id": 1178,
            "x": -77.8,
            "y": -2.1024000122070308
          }
        ],
        "keypoints_id": [
          {
            "id": 1174,
            "x": -75.63,
            "y": 0.2
          },
          {
            "id": 1178,
            "x": -77.8,
            "y": -2.1024000122070308
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 596,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 1184,
            "x": -76.6,
            "y": -2.556000067138672
          },
          {
            "id": 1186,
            "x": -76.6,
            "y": -2.0616000671386714
          },
          {
            "id": 1188,
            "x": -76.30759921875,
            "y": -1.2466001098632817
          },
          {
            "id": 1187,
            "x": -75.5183984375,
            "y": -0.95
          },
          {
            "id": 1185,
            "x": -74.93759843750001,
            "y": -0.95
          }
        ],
        "keypoints_id": [
          {
            "id": 1184,
            "x": -76.6,
            "y": -2.556000067138672
          },
          {
            "id": 1185,
            "x": -74.93759843750001,
            "y": -0.95
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 599,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 1192,
            "x": -75.4631982421875,
            "y": -8.180000000000001
          },
          {
            "id": 1193,
            "x": -76.89800000000002,
            "y": -8.180000000000001
          },
          {
            "id": 1195,
            "x": -77.55940156250001,
            "y": -8.513866910807293
          },
          {
            "id": 1194,
            "x": -77.8,
            "y": -9.204000488281249
          },
          {
            "id": 1179,
            "x": -77.8,
            "y": -9.64800029296875
          }
        ],
        "keypoints_id": [
          {
            "id": 1192,
            "x": -75.4631982421875,
            "y": -8.180000000000001
          },
          {
            "id": 1179,
            "x": -77.8,
            "y": -9.64800029296875
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 601,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 1196,
            "x": -77.8,
            "y": -8.495999926757815
          },
          {
            "id": 1179,
            "x": -77.8,
            "y": -9.64800029296875
          }
        ],
        "keypoints_id": [
          {
            "id": 1196,
            "x": -77.8,
            "y": -8.495999926757815
          },
          {
            "id": 1179,
            "x": -77.8,
            "y": -9.64800029296875
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 602,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 1190,
            "x": -75.4343994140625,
            "y": -7.090000000000002
          },
          {
            "id": 1197,
            "x": -76.82279941406249,
            "y": -7.090000000000002
          },
          {
            "id": 1199,
            "x": -77.562797265625,
            "y": -7.4313996582031265
          },
          {
            "id": 1198,
            "x": -77.8,
            "y": -8.123999926757811
          },
          {
            "id": 1196,
            "x": -77.8,
            "y": -8.495999926757815
          }
        ],
        "keypoints_id": [
          {
            "id": 1190,
            "x": -75.4343994140625,
            "y": -7.090000000000002
          },
          {
            "id": 1196,
            "x": -77.8,
            "y": -8.495999926757815
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 603,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 1192,
            "x": -75.4631982421875,
            "y": -8.180000000000001
          },
          {
            "id": 1200,
            "x": -75.82716666666668,
            "y": -8.180000000000001
          },
          {
            "id": 1202,
            "x": -76.3292602291667,
            "y": -8.030891830989585
          },
          {
            "id": 1201,
            "x": -76.60000000000001,
            "y": -7.624799584960934
          },
          {
            "id": 1183,
            "x": -76.60000000000001,
            "y": -7.244342155859374
          }
        ],
        "keypoints_id": [
          {
            "id": 1192,
            "x": -75.4631982421875,
            "y": -8.180000000000001
          },
          {
            "id": 1183,
            "x": -76.60000000000001,
            "y": -7.244342155859374
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 604,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 1183,
            "x": -76.60000000000001,
            "y": -7.244342155859374
          },
          {
            "id": 1203,
            "x": -76.6,
            "y": -6.091199951171874
          }
        ],
        "keypoints_id": [
          {
            "id": 1183,
            "x": -76.60000000000001,
            "y": -7.244342155859374
          },
          {
            "id": 1203,
            "x": -76.6,
            "y": -6.091199951171874
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 606,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 1190,
            "x": -75.4343994140625,
            "y": -7.090000000000002
          },
          {
            "id": 1204,
            "x": -75.77274181406247,
            "y": -7.090000000000002
          },
          {
            "id": 1206,
            "x": -76.40353059895835,
            "y": -6.886391975585939
          },
          {
            "id": 1205,
            "x": -76.6,
            "y": -6.489350351171877
          },
          {
            "id": 1203,
            "x": -76.6,
            "y": -6.091199951171874
          }
        ],
        "keypoints_id": [
          {
            "id": 1190,
            "x": -75.4343994140625,
            "y": -7.090000000000002
          },
          {
            "id": 1203,
            "x": -76.6,
            "y": -6.091199951171874
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 608,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 1207,
            "x": -74.6640017578125,
            "y": 0.2
          },
          {
            "id": 1174,
            "x": -75.63,
            "y": 0.2
          }
        ],
        "keypoints_id": [
          {
            "id": 1207,
            "x": -74.6640017578125,
            "y": 0.2
          },
          {
            "id": 1174,
            "x": -75.63,
            "y": 0.2
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 610,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 1208,
            "x": -72.4247978515625,
            "y": 0.2
          },
          {
            "id": 1207,
            "x": -74.6640017578125,
            "y": 0.2
          }
        ],
        "keypoints_id": [
          {
            "id": 1208,
            "x": -72.4247978515625,
            "y": 0.2
          },
          {
            "id": 1207,
            "x": -74.6640017578125,
            "y": 0.2
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 611,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 1143,
            "x": -70.73,
            "y": -1.8394
          },
          {
            "id": 1209,
            "x": -70.73,
            "y": -0.7044000366210943
          },
          {
            "id": 1211,
            "x": -71.05659648437499,
            "y": -0.050600018310547265
          },
          {
            "id": 1210,
            "x": -71.8943978515625,
            "y": 0.2
          },
          {
            "id": 1208,
            "x": -72.4247978515625,
            "y": 0.2
          }
        ],
        "keypoints_id": [
          {
            "id": 1143,
            "x": -70.73,
            "y": -1.8394
          },
          {
            "id": 1208,
            "x": -72.4247978515625,
            "y": 0.2
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 612,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 1145,
            "x": -73.31,
            "y": -1.935
          },
          {
            "id": 1212,
            "x": -73.31,
            "y": -0.5124000000000006
          },
          {
            "id": 1214,
            "x": -73.63179843750001,
            "y": 0.01659999999999954
          },
          {
            "id": 1213,
            "x": -74.2632017578125,
            "y": 0.2
          },
          {
            "id": 1207,
            "x": -74.6640017578125,
            "y": 0.2
          }
        ],
        "keypoints_id": [
          {
            "id": 1145,
            "x": -73.31,
            "y": -1.935
          },
          {
            "id": 1207,
            "x": -74.6640017578125,
            "y": 0.2
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 613,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 1178,
            "x": -77.8,
            "y": -2.1024000122070308
          },
          {
            "id": 1215,
            "x": -77.8,
            "y": -4.2612478637695315
          }
        ],
        "keypoints_id": [
          {
            "id": 1178,
            "x": -77.8,
            "y": -2.1024000122070308
          },
          {
            "id": 1215,
            "x": -77.8,
            "y": -4.2612478637695315
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 615,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 1203,
            "x": -76.6,
            "y": -6.091199951171874
          },
          {
            "id": 1216,
            "x": -76.60000000000001,
            "y": -4.333823852539062
          }
        ],
        "keypoints_id": [
          {
            "id": 1203,
            "x": -76.6,
            "y": -6.091199951171874
          },
          {
            "id": 1216,
            "x": -76.60000000000001,
            "y": -4.333823852539062
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 616,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 1216,
            "x": -76.60000000000001,
            "y": -4.333823852539062
          },
          {
            "id": 1184,
            "x": -76.6,
            "y": -2.556000067138672
          }
        ],
        "keypoints_id": [
          {
            "id": 1216,
            "x": -76.60000000000001,
            "y": -4.333823852539062
          },
          {
            "id": 1184,
            "x": -76.6,
            "y": -2.556000067138672
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 617,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 1215,
            "x": -77.8,
            "y": -4.2612478637695315
          },
          {
            "id": 1217,
            "x": -77.8,
            "y": -6.272639770507813
          }
        ],
        "keypoints_id": [
          {
            "id": 1215,
            "x": -77.8,
            "y": -4.2612478637695315
          },
          {
            "id": 1217,
            "x": -77.8,
            "y": -6.272639770507813
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 618,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 1217,
            "x": -77.8,
            "y": -6.272639770507813
          },
          {
            "id": 1196,
            "x": -77.8,
            "y": -8.495999926757815
          }
        ],
        "keypoints_id": [
          {
            "id": 1217,
            "x": -77.8,
            "y": -6.272639770507813
          },
          {
            "id": 1196,
            "x": -77.8,
            "y": -8.495999926757815
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 619,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 1173,
            "x": -68.6592001953125,
            "y": 0.2
          },
          {
            "id": 1218,
            "x": -70.1913623046875,
            "y": 0.2
          }
        ],
        "keypoints_id": [
          {
            "id": 1173,
            "x": -68.6592001953125,
            "y": 0.2
          },
          {
            "id": 1218,
            "x": -70.1913623046875,
            "y": 0.2
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 620,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 1218,
            "x": -70.1913623046875,
            "y": 0.2
          },
          {
            "id": 1208,
            "x": -72.4247978515625,
            "y": 0.2
          }
        ],
        "keypoints_id": [
          {
            "id": 1218,
            "x": -70.1913623046875,
            "y": 0.2
          },
          {
            "id": 1208,
            "x": -72.4247978515625,
            "y": 0.2
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 621,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 1219,
            "x": -73.57132812500001,
            "y": -0.95
          },
          {
            "id": 1146,
            "x": -72.127,
            "y": -0.95
          }
        ],
        "keypoints_id": [
          {
            "id": 1219,
            "x": -73.57132812500001,
            "y": -0.95
          },
          {
            "id": 1146,
            "x": -72.127,
            "y": -0.95
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "end_to_start",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 622,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 1185,
            "x": -74.93759843750001,
            "y": -0.95
          },
          {
            "id": 1219,
            "x": -73.57132812500001,
            "y": -0.95
          }
        ],
        "keypoints_id": [
          {
            "id": 1185,
            "x": -74.93759843750001,
            "y": -0.95
          },
          {
            "id": 1219,
            "x": -73.57132812500001,
            "y": -0.95
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "end_to_start",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 623,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 801,
            "x": -74.548,
            "y": -7.09139540823617
          },
          {
            "id": 1190,
            "x": -75.4343994140625,
            "y": -7.090000000000002
          }
        ],
        "keypoints_id": [
          {
            "id": 801,
            "x": -74.548,
            "y": -7.09139540823617
          },
          {
            "id": 1190,
            "x": -75.4343994140625,
            "y": -7.090000000000002
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "end_to_start",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 624,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 802,
            "x": -74.45633333333335,
            "y": -8.180000000000001
          },
          {
            "id": 1192,
            "x": -75.4631982421875,
            "y": -8.180000000000001
          }
        ],
        "keypoints_id": [
          {
            "id": 802,
            "x": -74.45633333333335,
            "y": -8.180000000000001
          },
          {
            "id": 1192,
            "x": -75.4631982421875,
            "y": -8.180000000000001
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "end_to_start",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 638,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 8,
            "x": 0.71,
            "y": -7.153999999999999
          },
          {
            "id": 1508,
            "x": -0.49800000000000005,
            "y": -7.153999999999999
          }
        ],
        "keypoints_id": [
          {
            "id": 8,
            "x": 0.71,
            "y": -7.153999999999999
          },
          {
            "id": 1508,
            "x": -0.49800000000000005,
            "y": -7.153999999999999
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.8,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 639,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 9,
            "x": 0.684,
            "y": -8.335
          },
          {
            "id": 1509,
            "x": -0.49800000000000005,
            "y": -8.335
          }
        ],
        "keypoints_id": [
          {
            "id": 9,
            "x": 0.684,
            "y": -8.335
          },
          {
            "id": 1509,
            "x": -0.49800000000000005,
            "y": -8.335
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.8,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 640,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 10,
            "x": 0.7489999999999999,
            "y": -9.526
          },
          {
            "id": 1510,
            "x": -0.49800000000000005,
            "y": -9.526
          }
        ],
        "keypoints_id": [
          {
            "id": 10,
            "x": 0.7489999999999999,
            "y": -9.526
          },
          {
            "id": 1510,
            "x": -0.49800000000000005,
            "y": -9.526
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.8,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 641,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 11,
            "x": 0.708,
            "y": -10.721999999999998
          },
          {
            "id": 1511,
            "x": -0.49800000000000005,
            "y": -10.721999999999998
          }
        ],
        "keypoints_id": [
          {
            "id": 11,
            "x": 0.708,
            "y": -10.721999999999998
          },
          {
            "id": 1511,
            "x": -0.49800000000000005,
            "y": -10.721999999999998
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.8,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 642,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 12,
            "x": 0.756,
            "y": -11.943
          },
          {
            "id": 1512,
            "x": -0.49800000000000005,
            "y": -11.943
          }
        ],
        "keypoints_id": [
          {
            "id": 12,
            "x": 0.756,
            "y": -11.943
          },
          {
            "id": 1512,
            "x": -0.49800000000000005,
            "y": -11.943
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.8,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 643,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 13,
            "x": 0.733,
            "y": -13.163999999999998
          },
          {
            "id": 1513,
            "x": -0.4979999999999999,
            "y": -13.163999999999998
          }
        ],
        "keypoints_id": [
          {
            "id": 13,
            "x": 0.733,
            "y": -13.163999999999998
          },
          {
            "id": 1513,
            "x": -0.4979999999999999,
            "y": -13.163999999999998
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.8,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 644,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 14,
            "x": 0.753,
            "y": -14.384
          },
          {
            "id": 1514,
            "x": -0.49800000000000005,
            "y": -14.384
          }
        ],
        "keypoints_id": [
          {
            "id": 14,
            "x": 0.753,
            "y": -14.384
          },
          {
            "id": 1514,
            "x": -0.49800000000000005,
            "y": -14.384
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.8,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 648,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 1307,
            "x": -7.9,
            "y": -15.180000000000003
          },
          {
            "id": 1308,
            "x": -6.914400000000003,
            "y": -15.180000000000003
          }
        ],
        "keypoints_id": [
          {
            "id": 1307,
            "x": -7.9,
            "y": -15.180000000000003
          },
          {
            "id": 1308,
            "x": -6.914400000000003,
            "y": -15.180000000000003
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.8,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 649,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 902,
            "x": -7.9,
            "y": -13.98
          },
          {
            "id": 1310,
            "x": -6.912000000000004,
            "y": -13.98
          }
        ],
        "keypoints_id": [
          {
            "id": 902,
            "x": -7.9,
            "y": -13.98
          },
          {
            "id": 1310,
            "x": -6.912000000000004,
            "y": -13.98
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.8,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 650,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 900,
            "x": -7.9,
            "y": -12.780000000000001
          },
          {
            "id": 1312,
            "x": -6.9444,
            "y": -12.780000000000001
          }
        ],
        "keypoints_id": [
          {
            "id": 900,
            "x": -7.9,
            "y": -12.780000000000001
          },
          {
            "id": 1312,
            "x": -6.9444,
            "y": -12.780000000000001
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.8,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 651,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 300,
            "x": -7.9,
            "y": -11.58
          },
          {
            "id": 1314,
            "x": -6.911999999999999,
            "y": -11.58
          }
        ],
        "keypoints_id": [
          {
            "id": 300,
            "x": -7.9,
            "y": -11.58
          },
          {
            "id": 1314,
            "x": -6.911999999999999,
            "y": -11.58
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.8,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 652,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 200,
            "x": -7.9,
            "y": -10.38
          },
          {
            "id": 1316,
            "x": -6.847200000000001,
            "y": -10.38
          }
        ],
        "keypoints_id": [
          {
            "id": 200,
            "x": -7.9,
            "y": -10.38
          },
          {
            "id": 1316,
            "x": -6.847200000000001,
            "y": -10.38
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.8,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 653,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 100,
            "x": -7.9,
            "y": -9.18
          },
          {
            "id": 1318,
            "x": -6.890399999999998,
            "y": -9.18
          }
        ],
        "keypoints_id": [
          {
            "id": 100,
            "x": -7.9,
            "y": -9.18
          },
          {
            "id": 1318,
            "x": -6.890399999999998,
            "y": -9.18
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.8,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 658,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 1514,
            "x": -0.49800000000000005,
            "y": -14.384
          },
          {
            "id": 1330,
            "x": -0.8872560305175778,
            "y": -14.384
          },
          {
            "id": 1332,
            "x": -2.187851938964844,
            "y": -13.79040000000001
          },
          {
            "id": 1331,
            "x": -2.7,
            "y": -12.595287999999998
          },
          {
            "id": 1304,
            "x": -2.7,
            "y": -12.015608000000007
          }
        ],
        "keypoints_id": [
          {
            "id": 1514,
            "x": -0.49800000000000005,
            "y": -14.384
          },
          {
            "id": 1304,
            "x": -2.7,
            "y": -12.015608000000007
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.8,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 662,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 1304,
            "x": -2.7,
            "y": -12.015608000000007
          },
          {
            "id": 1342,
            "x": -2.7,
            "y": -10.956096089843754
          }
        ],
        "keypoints_id": [
          {
            "id": 1304,
            "x": -2.7,
            "y": -12.015608000000007
          },
          {
            "id": 1342,
            "x": -2.7,
            "y": -10.956096089843754
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.8,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 663,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 1342,
            "x": -2.7,
            "y": -10.956096089843754
          },
          {
            "id": 1343,
            "x": -2.7,
            "y": -9.906624140624999
          }
        ],
        "keypoints_id": [
          {
            "id": 1342,
            "x": -2.7,
            "y": -10.956096089843754
          },
          {
            "id": 1343,
            "x": -2.7,
            "y": -9.906624140624999
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.8,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 664,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 1343,
            "x": -2.7,
            "y": -9.906624140624999
          },
          {
            "id": 1344,
            "x": -2.7,
            "y": -8.546112216796875
          }
        ],
        "keypoints_id": [
          {
            "id": 1343,
            "x": -2.7,
            "y": -9.906624140624999
          },
          {
            "id": 1344,
            "x": -2.7,
            "y": -8.546112216796875
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.8,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 665,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 1344,
            "x": -2.7,
            "y": -8.546112216796875
          },
          {
            "id": 1345,
            "x": -2.7,
            "y": -7.250111808593755
          }
        ],
        "keypoints_id": [
          {
            "id": 1344,
            "x": -2.7,
            "y": -8.546112216796875
          },
          {
            "id": 1345,
            "x": -2.7,
            "y": -7.250111808593755
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.8,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 666,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 1345,
            "x": -2.7,
            "y": -7.250111808593755
          },
          {
            "id": 1346,
            "x": -2.7,
            "y": -6.032448033203124
          }
        ],
        "keypoints_id": [
          {
            "id": 1345,
            "x": -2.7,
            "y": -7.250111808593755
          },
          {
            "id": 1346,
            "x": -2.7,
            "y": -6.032448033203124
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.8,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 667,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 1346,
            "x": -2.7,
            "y": -6.032448033203124
          },
          {
            "id": 1347,
            "x": -2.7,
            "y": -5.384447918945313
          }
        ],
        "keypoints_id": [
          {
            "id": 1346,
            "x": -2.7,
            "y": -6.032448033203124
          },
          {
            "id": 1347,
            "x": -2.7,
            "y": -5.384447918945313
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.8,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 668,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 1347,
            "x": -2.7,
            "y": -5.384447918945313
          },
          {
            "id": 1303,
            "x": -2.7,
            "y": -4.131129499023437
          }
        ],
        "keypoints_id": [
          {
            "id": 1347,
            "x": -2.7,
            "y": -5.384447918945313
          },
          {
            "id": 1303,
            "x": -2.7,
            "y": -4.131129499023437
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.8,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 669,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 1513,
            "x": -0.4979999999999999,
            "y": -13.163999999999998
          },
          {
            "id": 1348,
            "x": -0.9106800305175783,
            "y": -13.163999999999998
          },
          {
            "id": 1350,
            "x": -1.9588132722981768,
            "y": -12.710033378255215
          },
          {
            "id": 1349,
            "x": -2.7,
            "y": -11.686341423177085
          },
          {
            "id": 1342,
            "x": -2.7,
            "y": -10.956096089843754
          }
        ],
        "keypoints_id": [
          {
            "id": 1513,
            "x": -0.4979999999999999,
            "y": -13.163999999999998
          },
          {
            "id": 1342,
            "x": -2.7,
            "y": -10.956096089843754
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.8,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 670,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 1512,
            "x": -0.49800000000000005,
            "y": -11.943
          },
          {
            "id": 1351,
            "x": -0.9288240305175781,
            "y": -11.943
          },
          {
            "id": 1353,
            "x": -1.918571938964843,
            "y": -11.564368070312502
          },
          {
            "id": 1352,
            "x": -2.7,
            "y": -10.466976140625004
          },
          {
            "id": 1343,
            "x": -2.7,
            "y": -9.906624140624999
          }
        ],
        "keypoints_id": [
          {
            "id": 1512,
            "x": -0.49800000000000005,
            "y": -11.943
          },
          {
            "id": 1343,
            "x": -2.7,
            "y": -9.906624140624999
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.8,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 671,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 1511,
            "x": -0.49800000000000005,
            "y": -10.721999999999998
          },
          {
            "id": 1354,
            "x": -0.895416030517578,
            "y": -10.721999999999998
          },
          {
            "id": 1356,
            "x": -2.066131938964845,
            "y": -10.383272718749993
          },
          {
            "id": 1355,
            "x": -2.7,
            "y": -9.345504216796876
          },
          {
            "id": 1344,
            "x": -2.7,
            "y": -8.546112216796875
          }
        ],
        "keypoints_id": [
          {
            "id": 1511,
            "x": -0.49800000000000005,
            "y": -10.721999999999998
          },
          {
            "id": 1344,
            "x": -2.7,
            "y": -8.546112216796875
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.8,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 672,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 1510,
            "x": -0.49800000000000005,
            "y": -9.526
          },
          {
            "id": 1357,
            "x": -0.7825200305175786,
            "y": -9.526
          },
          {
            "id": 1359,
            "x": -2.0706359389648425,
            "y": -9.09911990429687
          },
          {
            "id": 1358,
            "x": -2.7,
            "y": -7.915295808593752
          },
          {
            "id": 1345,
            "x": -2.7,
            "y": -7.250111808593755
          }
        ],
        "keypoints_id": [
          {
            "id": 1510,
            "x": -0.49800000000000005,
            "y": -9.526
          },
          {
            "id": 1345,
            "x": -2.7,
            "y": -7.250111808593755
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.8,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 673,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 1509,
            "x": -0.49800000000000005,
            "y": -8.335
          },
          {
            "id": 1360,
            "x": -0.888504030517578,
            "y": -8.335
          },
          {
            "id": 1362,
            "x": -2.013899938964843,
            "y": -7.9519200166015604
          },
          {
            "id": 1361,
            "x": -2.7,
            "y": -6.750048033203123
          },
          {
            "id": 1346,
            "x": -2.7,
            "y": -6.032448033203124
          }
        ],
        "keypoints_id": [
          {
            "id": 1509,
            "x": -0.49800000000000005,
            "y": -8.335
          },
          {
            "id": 1346,
            "x": -2.7,
            "y": -6.032448033203124
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.8,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 674,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 1508,
            "x": -0.49800000000000005,
            "y": -7.153999999999999
          },
          {
            "id": 1363,
            "x": -1.1281200305175787,
            "y": -7.153999999999999
          },
          {
            "id": 1365,
            "x": -1.928939938964842,
            "y": -6.902639654296877
          },
          {
            "id": 1364,
            "x": -2.7,
            "y": -5.990591918945312
          },
          {
            "id": 1347,
            "x": -2.7,
            "y": -5.384447918945313
          }
        ],
        "keypoints_id": [
          {
            "id": 1508,
            "x": -0.49800000000000005,
            "y": -7.153999999999999
          },
          {
            "id": 1347,
            "x": -2.7,
            "y": -5.384447918945313
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.8,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 766,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 1490,
            "x": 0,
            "y": 0
          },
          {
            "id": 1491,
            "x": -5,
            "y": 0
          }
        ],
        "keypoints_id": [
          {
            "id": 1490,
            "x": 0,
            "y": 0
          },
          {
            "id": 1491,
            "x": -5,
            "y": 0
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 767,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 1491,
            "x": -5,
            "y": 0
          },
          {
            "id": 1492,
            "x": -10,
            "y": 0
          }
        ],
        "keypoints_id": [
          {
            "id": 1491,
            "x": -5,
            "y": 0
          },
          {
            "id": 1492,
            "x": -10,
            "y": 0
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 768,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 840,
            "x": -9.63,
            "y": -2.15
          },
          {
            "id": 1493,
            "x": -7.693200122070311,
            "y": -2.15
          }
        ],
        "keypoints_id": [
          {
            "id": 840,
            "x": -9.63,
            "y": -2.15
          },
          {
            "id": 1493,
            "x": -7.693200122070311,
            "y": -2.15
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 769,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 1493,
            "x": -7.693200122070311,
            "y": -2.15
          },
          {
            "id": 823,
            "x": -7.073236078125002,
            "y": -2.15
          }
        ],
        "keypoints_id": [
          {
            "id": 1493,
            "x": -7.693200122070311,
            "y": -2.15
          },
          {
            "id": 823,
            "x": -7.073236078125002,
            "y": -2.15
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 770,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 838,
            "x": -9.45,
            "y": -3.9
          },
          {
            "id": 1494,
            "x": -9.45,
            "y": -3.420000000000001
          },
          {
            "id": 1496,
            "x": -9.110999755859376,
            "y": -2.4609999999999994
          },
          {
            "id": 1495,
            "x": -8.280000122070312,
            "y": -2.15
          },
          {
            "id": 1493,
            "x": -7.693200122070311,
            "y": -2.15
          }
        ],
        "keypoints_id": [
          {
            "id": 838,
            "x": -9.45,
            "y": -3.9
          },
          {
            "id": 1493,
            "x": -7.693200122070311,
            "y": -2.15
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 773,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 1502,
            "x": -9.1,
            "y": -9.18
          },
          {
            "id": 100,
            "x": -7.9,
            "y": -9.18
          }
        ],
        "keypoints_id": [
          {
            "id": 1502,
            "x": -9.1,
            "y": -9.18
          },
          {
            "id": 100,
            "x": -7.9,
            "y": -9.18
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "end_to_start",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 775,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 1503,
            "x": -9.1,
            "y": -10.38
          },
          {
            "id": 200,
            "x": -7.9,
            "y": -10.38
          }
        ],
        "keypoints_id": [
          {
            "id": 1503,
            "x": -9.1,
            "y": -10.38
          },
          {
            "id": 200,
            "x": -7.9,
            "y": -10.38
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "end_to_start",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 777,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 1504,
            "x": -9.1,
            "y": -11.58
          },
          {
            "id": 300,
            "x": -7.9,
            "y": -11.58
          }
        ],
        "keypoints_id": [
          {
            "id": 1504,
            "x": -9.1,
            "y": -11.58
          },
          {
            "id": 300,
            "x": -7.9,
            "y": -11.58
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "end_to_start",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 779,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 1505,
            "x": -9.1,
            "y": -12.780000000000001
          },
          {
            "id": 900,
            "x": -7.9,
            "y": -12.780000000000001
          }
        ],
        "keypoints_id": [
          {
            "id": 1505,
            "x": -9.1,
            "y": -12.780000000000001
          },
          {
            "id": 900,
            "x": -7.9,
            "y": -12.780000000000001
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "end_to_start",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 781,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 1506,
            "x": -9.1,
            "y": -13.98
          },
          {
            "id": 902,
            "x": -7.9,
            "y": -13.98
          }
        ],
        "keypoints_id": [
          {
            "id": 1506,
            "x": -9.1,
            "y": -13.98
          },
          {
            "id": 902,
            "x": -7.9,
            "y": -13.98
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "end_to_start",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 783,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 1507,
            "x": -9.1,
            "y": -15.180000000000003
          },
          {
            "id": 1307,
            "x": -7.9,
            "y": -15.180000000000003
          }
        ],
        "keypoints_id": [
          {
            "id": 1507,
            "x": -9.1,
            "y": -15.180000000000003
          },
          {
            "id": 1307,
            "x": -7.9,
            "y": -15.180000000000003
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "end_to_start",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 795,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 1540,
            "x": -6.275650258031252,
            "y": -0.75
          },
          {
            "id": 697,
            "x": -7.959328014648436,
            "y": -0.75
          }
        ],
        "keypoints_id": [
          {
            "id": 1540,
            "x": -6.275650258031252,
            "y": -0.75
          },
          {
            "id": 697,
            "x": -7.959328014648436,
            "y": -0.75
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "end_to_start",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 796,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 1303,
            "x": -2.7,
            "y": -4.131129499023437
          },
          {
            "id": 1541,
            "x": -2.6933052631578915,
            "y": -3.4951294990234367
          },
          {
            "id": 1543,
            "x": -3.0062400410156256,
            "y": -1.3803888828125
          },
          {
            "id": 1542,
            "x": -5.237808082031251,
            "y": -0.75
          },
          {
            "id": 1540,
            "x": -6.275650258031252,
            "y": -0.75
          }
        ],
        "keypoints_id": [
          {
            "id": 1303,
            "x": -2.7,
            "y": -4.131129499023437
          },
          {
            "id": 1540,
            "x": -6.275650258031252,
            "y": -0.75
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.8,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 815,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 26,
            "x": -6.681,
            "y": -17.68
          },
          {
            "id": 25,
            "x": -7.900999999999999,
            "y": -17.68
          }
        ],
        "keypoints_id": [
          {
            "id": 26,
            "x": -6.681,
            "y": -17.68
          },
          {
            "id": 25,
            "x": -7.900999999999999,
            "y": -17.68
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "end_to_start",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 817,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 25,
            "x": -7.900999999999999,
            "y": -17.68
          },
          {
            "id": 24,
            "x": -8.921,
            "y": -17.68
          }
        ],
        "keypoints_id": [
          {
            "id": 25,
            "x": -7.900999999999999,
            "y": -17.68
          },
          {
            "id": 24,
            "x": -8.921,
            "y": -17.68
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "end_to_start",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 819,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 24,
            "x": -8.921,
            "y": -17.68
          },
          {
            "id": 23,
            "x": -9.941,
            "y": -17.68
          }
        ],
        "keypoints_id": [
          {
            "id": 24,
            "x": -8.921,
            "y": -17.68
          },
          {
            "id": 23,
            "x": -9.941,
            "y": -17.68
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "end_to_start",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 820,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 22,
            "x": -10.960999999999999,
            "y": -17.68
          },
          {
            "id": 21,
            "x": -11.981,
            "y": -17.68
          }
        ],
        "keypoints_id": [
          {
            "id": 22,
            "x": -10.960999999999999,
            "y": -17.68
          },
          {
            "id": 21,
            "x": -11.981,
            "y": -17.68
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "end_to_start",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 821,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 23,
            "x": -9.941,
            "y": -17.68
          },
          {
            "id": 22,
            "x": -10.960999999999999,
            "y": -17.68
          }
        ],
        "keypoints_id": [
          {
            "id": 23,
            "x": -9.941,
            "y": -17.68
          },
          {
            "id": 22,
            "x": -10.960999999999999,
            "y": -17.68
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "end_to_start",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 824,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 15,
            "x": -11.98080048828125,
            "y": -16.38
          },
          {
            "id": 1630,
            "x": -13.39300048828125,
            "y": -16.38
          },
          {
            "id": 1631,
            "x": -14.47080024414063,
            "y": -15.851858984375
          },
          {
            "id": 1629,
            "x": -15.180000000000003,
            "y": -14.760917968749999
          },
          {
            "id": 1724,
            "x": -15.180000000000003,
            "y": -13.933898974609374
          }
        ],
        "keypoints_id": [
          {
            "id": 15,
            "x": -11.98080048828125,
            "y": -16.38
          },
          {
            "id": 1724,
            "x": -15.180000000000003,
            "y": -13.933898974609374
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "end_to_start",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 828,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 16,
            "x": -10.960999999999999,
            "y": -16.38
          },
          {
            "id": 15,
            "x": -11.98080048828125,
            "y": -16.38
          }
        ],
        "keypoints_id": [
          {
            "id": 16,
            "x": -10.960999999999999,
            "y": -16.38
          },
          {
            "id": 15,
            "x": -11.98080048828125,
            "y": -16.38
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 830,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 17,
            "x": -9.941,
            "y": -16.38
          },
          {
            "id": 16,
            "x": -10.960999999999999,
            "y": -16.38
          }
        ],
        "keypoints_id": [
          {
            "id": 17,
            "x": -9.941,
            "y": -16.38
          },
          {
            "id": 16,
            "x": -10.960999999999999,
            "y": -16.38
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 832,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 18,
            "x": -8.921,
            "y": -16.38
          },
          {
            "id": 17,
            "x": -9.941,
            "y": -16.38
          }
        ],
        "keypoints_id": [
          {
            "id": 18,
            "x": -8.921,
            "y": -16.38
          },
          {
            "id": 17,
            "x": -9.941,
            "y": -16.38
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 833,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 20,
            "x": -6.681,
            "y": -16.38
          },
          {
            "id": 19,
            "x": -7.900999999999999,
            "y": -16.38
          }
        ],
        "keypoints_id": [
          {
            "id": 20,
            "x": -6.681,
            "y": -16.38
          },
          {
            "id": 19,
            "x": -7.900999999999999,
            "y": -16.38
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 834,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 19,
            "x": -7.900999999999999,
            "y": -16.38
          },
          {
            "id": 18,
            "x": -8.921,
            "y": -16.38
          }
        ],
        "keypoints_id": [
          {
            "id": 19,
            "x": -7.900999999999999,
            "y": -16.38
          },
          {
            "id": 18,
            "x": -8.921,
            "y": -16.38
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 840,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 1652,
            "x": -15.180000000000003,
            "y": -5.073948629991317
          },
          {
            "id": 1626,
            "x": -15.180000000000003,
            "y": -3.852239999999999
          }
        ],
        "keypoints_id": [
          {
            "id": 1652,
            "x": -15.180000000000003,
            "y": -5.073948629991317
          },
          {
            "id": 1626,
            "x": -15.180000000000003,
            "y": -3.852239999999999
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 842,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 1653,
            "x": -15.180000000000003,
            "y": -8.039931531276036
          },
          {
            "id": 1652,
            "x": -15.180000000000003,
            "y": -5.073948629991317
          }
        ],
        "keypoints_id": [
          {
            "id": 1653,
            "x": -15.180000000000003,
            "y": -8.039931531276036
          },
          {
            "id": 1652,
            "x": -15.180000000000003,
            "y": -5.073948629991317
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 844,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 1654,
            "x": -15.180000000000003,
            "y": -9.305279760742188
          },
          {
            "id": 1653,
            "x": -15.180000000000003,
            "y": -8.039931531276036
          }
        ],
        "keypoints_id": [
          {
            "id": 1654,
            "x": -15.180000000000003,
            "y": -9.305279760742188
          },
          {
            "id": 1653,
            "x": -15.180000000000003,
            "y": -8.039931531276036
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 846,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 1655,
            "x": -15.180000000000003,
            "y": -10.529591980468748
          },
          {
            "id": 1654,
            "x": -15.180000000000003,
            "y": -9.305279760742188
          }
        ],
        "keypoints_id": [
          {
            "id": 1655,
            "x": -15.180000000000003,
            "y": -10.529591980468748
          },
          {
            "id": 1654,
            "x": -15.180000000000003,
            "y": -9.305279760742188
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 851,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 1132,
            "x": -16.38,
            "y": -14.577292848828113
          },
          {
            "id": 1133,
            "x": -16.38,
            "y": -10.947202276796885
          }
        ],
        "keypoints_id": [
          {
            "id": 1132,
            "x": -16.38,
            "y": -14.577292848828113
          },
          {
            "id": 1133,
            "x": -16.38,
            "y": -10.947202276796885
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "backward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 853,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 15,
            "x": -11.98080048828125,
            "y": -16.38
          },
          {
            "id": 1687,
            "x": -13.39300048828125,
            "y": -16.38
          },
          {
            "id": 1689,
            "x": -15.945629204140626,
            "y": -16.31921902875
          },
          {
            "id": 1688,
            "x": -16.38,
            "y": -14.064959716796874
          },
          {
            "id": 1133,
            "x": -16.38,
            "y": -10.947202276796885
          }
        ],
        "keypoints_id": [
          {
            "id": 15,
            "x": -11.98080048828125,
            "y": -16.38
          },
          {
            "id": 1133,
            "x": -16.38,
            "y": -10.947202276796885
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "backward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 854,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 21,
            "x": -11.981,
            "y": -17.68
          },
          {
            "id": 1690,
            "x": -13.749511755859373,
            "y": -17.68
          },
          {
            "id": 1692,
            "x": -15.783895688281257,
            "y": -17.660844206249998
          },
          {
            "id": 1691,
            "x": -16.38,
            "y": -16.535193633203132
          },
          {
            "id": 1132,
            "x": -16.38,
            "y": -14.577292848828113
          }
        ],
        "keypoints_id": [
          {
            "id": 21,
            "x": -11.981,
            "y": -17.68
          },
          {
            "id": 1132,
            "x": -16.38,
            "y": -14.577292848828113
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "backward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 855,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 1685,
            "x": -16.38,
            "y": -4.232931766757813
          },
          {
            "id": 1693,
            "x": -16.38,
            "y": -3.390627884277344
          },
          {
            "id": 1695,
            "x": -16.05425009375,
            "y": -2.494698094726562
          },
          {
            "id": 1694,
            "x": -15.210324187499996,
            "y": -2.15
          },
          {
            "id": 1550,
            "x": -14.609940730034719,
            "y": -2.15
          }
        ],
        "keypoints_id": [
          {
            "id": 1685,
            "x": -16.38,
            "y": -4.232931766757813
          },
          {
            "id": 1550,
            "x": -14.609940730034719,
            "y": -2.15
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "backward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 869,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 1724,
            "x": -15.180000000000003,
            "y": -13.933898974609374
          },
          {
            "id": 1715,
            "x": -15.180000000000003,
            "y": -12.047615966796876
          }
        ],
        "keypoints_id": [
          {
            "id": 1724,
            "x": -15.180000000000003,
            "y": -13.933898974609374
          },
          {
            "id": 1715,
            "x": -15.180000000000003,
            "y": -12.047615966796876
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 870,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 1715,
            "x": -15.180000000000003,
            "y": -12.047615966796876
          },
          {
            "id": 1655,
            "x": -15.180000000000003,
            "y": -10.529591980468748
          }
        ],
        "keypoints_id": [
          {
            "id": 1715,
            "x": -15.180000000000003,
            "y": -12.047615966796876
          },
          {
            "id": 1655,
            "x": -15.180000000000003,
            "y": -10.529591980468748
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 871,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 1133,
            "x": -16.38,
            "y": -10.947202276796885
          },
          {
            "id": 1716,
            "x": -16.38,
            "y": -9.879045621953129
          }
        ],
        "keypoints_id": [
          {
            "id": 1133,
            "x": -16.38,
            "y": -10.947202276796885
          },
          {
            "id": 1716,
            "x": -16.38,
            "y": -9.879045621953129
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "backward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 872,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 1716,
            "x": -16.38,
            "y": -9.879045621953129
          },
          {
            "id": 1702,
            "x": -16.38,
            "y": -8.949243170039063
          }
        ],
        "keypoints_id": [
          {
            "id": 1716,
            "x": -16.38,
            "y": -9.879045621953129
          },
          {
            "id": 1702,
            "x": -16.38,
            "y": -8.949243170039063
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "backward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 874,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 1626,
            "x": -15.180000000000003,
            "y": -3.852239999999999
          },
          {
            "id": 1718,
            "x": -15.180000000000003,
            "y": -2.272079938964844
          },
          {
            "id": 1720,
            "x": -15.581237421874999,
            "y": -1.2475199694824222
          },
          {
            "id": 1719,
            "x": -16.690956064453125,
            "y": -0.75
          },
          {
            "id": 826,
            "x": -17.286155858506945,
            "y": -0.75
          }
        ],
        "keypoints_id": [
          {
            "id": 1626,
            "x": -15.180000000000003,
            "y": -3.852239999999999
          },
          {
            "id": 826,
            "x": -17.286155858506945,
            "y": -0.75
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 875,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 1626,
            "x": -15.180000000000003,
            "y": -3.852239999999999
          },
          {
            "id": 1721,
            "x": -15.180000000000003,
            "y": -3.352079990234375
          },
          {
            "id": 1723,
            "x": -14.884105312499997,
            "y": -2.4659199951171873
          },
          {
            "id": 1722,
            "x": -14.138929404296878,
            "y": -2.15
          },
          {
            "id": 1627,
            "x": -13.586928897931134,
            "y": -2.15
          }
        ],
        "keypoints_id": [
          {
            "id": 1626,
            "x": -15.180000000000003,
            "y": -3.852239999999999
          },
          {
            "id": 1627,
            "x": -13.586928897931134,
            "y": -2.15
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 876,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 1725,
            "x": -15.180000000000003,
            "y": -15.332879999999998
          },
          {
            "id": 1724,
            "x": -15.180000000000003,
            "y": -13.933898974609374
          }
        ],
        "keypoints_id": [
          {
            "id": 1725,
            "x": -15.180000000000003,
            "y": -15.332879999999998
          },
          {
            "id": 1724,
            "x": -15.180000000000003,
            "y": -13.933898974609374
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "end_to_start",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 877,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 21,
            "x": -11.981,
            "y": -17.68
          },
          {
            "id": 1726,
            "x": -12.947719755859373,
            "y": -17.68
          },
          {
            "id": 1728,
            "x": -14.517276168281242,
            "y": -17.079978560000008
          },
          {
            "id": 1727,
            "x": -15.180000000000003,
            "y": -15.962639999999999
          },
          {
            "id": 1725,
            "x": -15.180000000000003,
            "y": -15.332879999999998
          }
        ],
        "keypoints_id": [
          {
            "id": 21,
            "x": -11.981,
            "y": -17.68
          },
          {
            "id": 1725,
            "x": -15.180000000000003,
            "y": -15.332879999999998
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 880,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 1702,
            "x": -16.38,
            "y": -8.949243170039063
          },
          {
            "id": 1685,
            "x": -16.38,
            "y": -4.232931766757813
          }
        ],
        "keypoints_id": [
          {
            "id": 1702,
            "x": -16.38,
            "y": -8.949243170039063
          },
          {
            "id": 1685,
            "x": -16.38,
            "y": -4.232931766757813
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "backward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 883,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 823,
            "x": -7.073236078125002,
            "y": -2.15
          },
          {
            "id": 1739,
            "x": -6.52987578125,
            "y": -2.15
          },
          {
            "id": 1741,
            "x": -5.312547490624999,
            "y": -2.5485467119921874
          },
          {
            "id": 1740,
            "x": -5,
            "y": -3.651633278808595
          },
          {
            "id": 1305,
            "x": -5,
            "y": -4.393713279999998
          }
        ],
        "keypoints_id": [
          {
            "id": 823,
            "x": -7.073236078125002,
            "y": -2.15
          },
          {
            "id": 1305,
            "x": -5,
            "y": -4.393713279999998
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 884,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 1308,
            "x": -6.914400000000003,
            "y": -15.180000000000003
          },
          {
            "id": 1742,
            "x": -6.443327999999998,
            "y": -15.180000000000003
          },
          {
            "id": 1744,
            "x": -5.398239999999999,
            "y": -15.461080972656257
          },
          {
            "id": 1743,
            "x": -5,
            "y": -16.353872724609374
          },
          {
            "id": 1369,
            "x": -5,
            "y": -16.88225624114062
          }
        ],
        "keypoints_id": [
          {
            "id": 1308,
            "x": -6.914400000000003,
            "y": -15.180000000000003
          },
          {
            "id": 1369,
            "x": -5,
            "y": -16.88225624114062
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 886,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 1745,
            "x": -5,
            "y": -15.679296298828131
          },
          {
            "id": 1369,
            "x": -5,
            "y": -16.88225624114062
          }
        ],
        "keypoints_id": [
          {
            "id": 1745,
            "x": -5,
            "y": -15.679296298828131
          },
          {
            "id": 1369,
            "x": -5,
            "y": -16.88225624114062
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 888,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 1746,
            "x": -5,
            "y": -14.53305629101563
          },
          {
            "id": 1745,
            "x": -5,
            "y": -15.679296298828131
          }
        ],
        "keypoints_id": [
          {
            "id": 1746,
            "x": -5,
            "y": -14.53305629101563
          },
          {
            "id": 1745,
            "x": -5,
            "y": -15.679296298828131
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 890,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 1747,
            "x": -5,
            "y": -13.236192050781252
          },
          {
            "id": 1746,
            "x": -5,
            "y": -14.53305629101563
          }
        ],
        "keypoints_id": [
          {
            "id": 1747,
            "x": -5,
            "y": -13.236192050781252
          },
          {
            "id": 1746,
            "x": -5,
            "y": -14.53305629101563
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 892,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 1748,
            "x": -5,
            "y": -12.178080566406251
          },
          {
            "id": 1747,
            "x": -5,
            "y": -13.236192050781252
          }
        ],
        "keypoints_id": [
          {
            "id": 1748,
            "x": -5,
            "y": -12.178080566406251
          },
          {
            "id": 1747,
            "x": -5,
            "y": -13.236192050781252
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 894,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 1749,
            "x": -5,
            "y": -10.908864155273436
          },
          {
            "id": 1748,
            "x": -5,
            "y": -12.178080566406251
          }
        ],
        "keypoints_id": [
          {
            "id": 1749,
            "x": -5,
            "y": -10.908864155273436
          },
          {
            "id": 1748,
            "x": -5,
            "y": -12.178080566406251
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 895,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 1310,
            "x": -6.912000000000004,
            "y": -13.98
          },
          {
            "id": 1750,
            "x": -6.352799877929689,
            "y": -13.98
          },
          {
            "id": 1752,
            "x": -5.348800244140624,
            "y": -14.254127539062498
          },
          {
            "id": 1751,
            "x": -5,
            "y": -15.169056298828128
          },
          {
            "id": 1745,
            "x": -5,
            "y": -15.679296298828131
          }
        ],
        "keypoints_id": [
          {
            "id": 1310,
            "x": -6.912000000000004,
            "y": -13.98
          },
          {
            "id": 1745,
            "x": -5,
            "y": -15.679296298828131
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 896,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 1312,
            "x": -6.9444,
            "y": -12.780000000000001
          },
          {
            "id": 1753,
            "x": -6.464400122070314,
            "y": -12.780000000000001
          },
          {
            "id": 1755,
            "x": -5.336199755859373,
            "y": -13.090223535156245
          },
          {
            "id": 1754,
            "x": -5,
            "y": -13.99804829101563
          },
          {
            "id": 1746,
            "x": -5,
            "y": -14.53305629101563
          }
        ],
        "keypoints_id": [
          {
            "id": 1312,
            "x": -6.9444,
            "y": -12.780000000000001
          },
          {
            "id": 1746,
            "x": -5,
            "y": -14.53305629101563
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 897,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 1314,
            "x": -6.911999999999999,
            "y": -11.58
          },
          {
            "id": 1756,
            "x": -6.187200000000001,
            "y": -11.58
          },
          {
            "id": 1758,
            "x": -5.323599999999999,
            "y": -11.908176025390626
          },
          {
            "id": 1757,
            "x": -5,
            "y": -12.754752050781253
          },
          {
            "id": 1747,
            "x": -5,
            "y": -13.236192050781252
          }
        ],
        "keypoints_id": [
          {
            "id": 1314,
            "x": -6.911999999999999,
            "y": -11.58
          },
          {
            "id": 1747,
            "x": -5,
            "y": -13.236192050781252
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 898,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 1316,
            "x": -6.847200000000001,
            "y": -10.38
          },
          {
            "id": 1759,
            "x": -6.331199999999999,
            "y": -10.38
          },
          {
            "id": 1761,
            "x": -5.330799999999999,
            "y": -10.675440283203125
          },
          {
            "id": 1760,
            "x": -5,
            "y": -11.597280566406248
          },
          {
            "id": 1748,
            "x": -5,
            "y": -12.178080566406251
          }
        ],
        "keypoints_id": [
          {
            "id": 1316,
            "x": -6.847200000000001,
            "y": -10.38
          },
          {
            "id": 1748,
            "x": -5,
            "y": -12.178080566406251
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 899,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 1318,
            "x": -6.890399999999998,
            "y": -9.18
          },
          {
            "id": 1762,
            "x": -6.338399755859377,
            "y": -9.18
          },
          {
            "id": 1764,
            "x": -5.298399877929687,
            "y": -9.435792382812501
          },
          {
            "id": 1763,
            "x": -5,
            "y": -10.389984155273437
          },
          {
            "id": 1749,
            "x": -5,
            "y": -10.908864155273436
          }
        ],
        "keypoints_id": [
          {
            "id": 1318,
            "x": -6.890399999999998,
            "y": -9.18
          },
          {
            "id": 1749,
            "x": -5,
            "y": -10.908864155273436
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forwardAndbackward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 901,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 1765,
            "x": -5,
            "y": -9.662054180273437
          },
          {
            "id": 1749,
            "x": -5,
            "y": -10.908864155273436
          }
        ],
        "keypoints_id": [
          {
            "id": 1765,
            "x": -5,
            "y": -9.662054180273437
          },
          {
            "id": 1749,
            "x": -5,
            "y": -10.908864155273436
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 902,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 1305,
            "x": -5,
            "y": -4.393713279999998
          },
          {
            "id": 1766,
            "x": -5,
            "y": -6.275520000000003
          }
        ],
        "keypoints_id": [
          {
            "id": 1305,
            "x": -5,
            "y": -4.393713279999998
          },
          {
            "id": 1766,
            "x": -5,
            "y": -6.275520000000003
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 911,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 1318,
            "x": -6.890399999999998,
            "y": -9.18
          },
          {
            "id": 1788,
            "x": -5.536999999999999,
            "y": -9.18
          },
          {
            "id": 1790,
            "x": -3.72967989013672,
            "y": -8.576303901367188
          },
          {
            "id": 1789,
            "x": -2.7,
            "y": -6.742847802734376
          },
          {
            "id": 1346,
            "x": -2.7,
            "y": -6.032448033203124
          }
        ],
        "keypoints_id": [
          {
            "id": 1318,
            "x": -6.890399999999998,
            "y": -9.18
          },
          {
            "id": 1346,
            "x": -2.7,
            "y": -6.032448033203124
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 912,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 1316,
            "x": -6.847200000000001,
            "y": -10.38
          },
          {
            "id": 1791,
            "x": -5.536999999999999,
            "y": -10.38
          },
          {
            "id": 1793,
            "x": -3.664879853515626,
            "y": -9.799535957031251
          },
          {
            "id": 1792,
            "x": -2.7,
            "y": -7.9173119140625
          },
          {
            "id": 1345,
            "x": -2.7,
            "y": -7.250111808593755
          }
        ],
        "keypoints_id": [
          {
            "id": 1316,
            "x": -6.847200000000001,
            "y": -10.38
          },
          {
            "id": 1345,
            "x": -2.7,
            "y": -7.250111808593755
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 913,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 1314,
            "x": -6.911999999999999,
            "y": -11.58
          },
          {
            "id": 1794,
            "x": -5.536999999999999,
            "y": -11.58
          },
          {
            "id": 1796,
            "x": -3.7761920610351574,
            "y": -11.117663851562499
          },
          {
            "id": 1795,
            "x": -2.7,
            "y": -9.358176313476562
          },
          {
            "id": 1344,
            "x": -2.7,
            "y": -8.546112216796875
          }
        ],
        "keypoints_id": [
          {
            "id": 1314,
            "x": -6.911999999999999,
            "y": -11.58
          },
          {
            "id": 1344,
            "x": -2.7,
            "y": -8.546112216796875
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 914,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 1312,
            "x": -6.9444,
            "y": -12.780000000000001
          },
          {
            "id": 1797,
            "x": -5.536999999999999,
            "y": -12.780000000000001
          },
          {
            "id": 1799,
            "x": -3.670856012207031,
            "y": -12.294095871093745
          },
          {
            "id": 1798,
            "x": -2.7,
            "y": -10.4591997421875
          },
          {
            "id": 1343,
            "x": -2.7,
            "y": -9.906624140624999
          }
        ],
        "keypoints_id": [
          {
            "id": 1312,
            "x": -6.9444,
            "y": -12.780000000000001
          },
          {
            "id": 1343,
            "x": -2.7,
            "y": -9.906624140624999
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 915,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 1310,
            "x": -6.912000000000004,
            "y": -13.98
          },
          {
            "id": 1800,
            "x": -5.536999999999999,
            "y": -13.98
          },
          {
            "id": 1802,
            "x": -3.873248061035158,
            "y": -13.348560087890638
          },
          {
            "id": 1801,
            "x": -2.7,
            "y": -11.697024175781253
          },
          {
            "id": 1342,
            "x": -2.7,
            "y": -10.956096089843754
          }
        ],
        "keypoints_id": [
          {
            "id": 1310,
            "x": -6.912000000000004,
            "y": -13.98
          },
          {
            "id": 1342,
            "x": -2.7,
            "y": -10.956096089843754
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 916,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 1308,
            "x": -6.914400000000003,
            "y": -15.180000000000003
          },
          {
            "id": 1803,
            "x": -5.537280024414063,
            "y": -15.180000000000003
          },
          {
            "id": 1805,
            "x": -3.8079200122070325,
            "y": -14.593243720703125
          },
          {
            "id": 1804,
            "x": -2.7,
            "y": -12.58232744140625
          },
          {
            "id": 1304,
            "x": -2.7,
            "y": -12.015608000000007
          }
        ],
        "keypoints_id": [
          {
            "id": 1308,
            "x": -6.914400000000003,
            "y": -15.180000000000003
          },
          {
            "id": 1304,
            "x": -2.7,
            "y": -12.015608000000007
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 917,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 1766,
            "x": -5,
            "y": -6.275520000000003
          },
          {
            "id": 1806,
            "x": -5,
            "y": -7.869312133789064
          }
        ],
        "keypoints_id": [
          {
            "id": 1766,
            "x": -5,
            "y": -6.275520000000003
          },
          {
            "id": 1806,
            "x": -5,
            "y": -7.869312133789064
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 918,
        "road_type": "line",
        "controlpoints": [
          {
            "id": 1806,
            "x": -5,
            "y": -7.869312133789064
          },
          {
            "id": 1765,
            "x": -5,
            "y": -9.662054180273437
          }
        ],
        "keypoints_id": [
          {
            "id": 1806,
            "x": -5,
            "y": -7.869312133789064
          },
          {
            "id": 1765,
            "x": -5,
            "y": -9.662054180273437
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 919,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 1514,
            "x": -0.49800000000000005,
            "y": -14.384
          },
          {
            "id": 1807,
            "x": -2.3079839923706054,
            "y": -14.384
          },
          {
            "id": 1809,
            "x": -4.042791938964844,
            "y": -15.00605676757813
          },
          {
            "id": 1808,
            "x": -5,
            "y": -16.447185291015632
          },
          {
            "id": 1369,
            "x": -5,
            "y": -16.88225624114062
          }
        ],
        "keypoints_id": [
          {
            "id": 1514,
            "x": -0.49800000000000005,
            "y": -14.384
          },
          {
            "id": 1369,
            "x": -5,
            "y": -16.88225624114062
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 920,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 1513,
            "x": -0.4979999999999999,
            "y": -13.163999999999998
          },
          {
            "id": 1810,
            "x": -2.876357752370605,
            "y": -13.21754258894767
          },
          {
            "id": 1812,
            "x": -4.460967938964845,
            "y": -13.928783734375003
          },
          {
            "id": 1811,
            "x": -5,
            "y": -15.104832445312502
          },
          {
            "id": 1745,
            "x": -5,
            "y": -15.679296298828131
          }
        ],
        "keypoints_id": [
          {
            "id": 1513,
            "x": -0.4979999999999999,
            "y": -13.163999999999998
          },
          {
            "id": 1745,
            "x": -5,
            "y": -15.679296298828131
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 921,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 1512,
            "x": -0.49800000000000005,
            "y": -11.943
          },
          {
            "id": 1813,
            "x": -2.8471199923706045,
            "y": -11.943
          },
          {
            "id": 1815,
            "x": -4.239783938964845,
            "y": -12.452267828125002
          },
          {
            "id": 1814,
            "x": -5,
            "y": -13.614720388671874
          },
          {
            "id": 1746,
            "x": -5,
            "y": -14.53305629101563
          }
        ],
        "keypoints_id": [
          {
            "id": 1512,
            "x": -0.49800000000000005,
            "y": -11.943
          },
          {
            "id": 1746,
            "x": -5,
            "y": -14.53305629101563
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 922,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 1511,
            "x": -0.49800000000000005,
            "y": -10.721999999999998
          },
          {
            "id": 1816,
            "x": -2.276879992370605,
            "y": -10.721999999999998
          },
          {
            "id": 1818,
            "x": -4.011687938964844,
            "y": -11.343672269531252
          },
          {
            "id": 1817,
            "x": -5,
            "y": -12.628895806640626
          },
          {
            "id": 1747,
            "x": -5,
            "y": -13.236192050781252
          }
        ],
        "keypoints_id": [
          {
            "id": 1511,
            "x": -0.49800000000000005,
            "y": -10.721999999999998
          },
          {
            "id": 1747,
            "x": -5,
            "y": -13.236192050781252
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 923,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 1510,
            "x": -0.49800000000000005,
            "y": -9.526
          },
          {
            "id": 1819,
            "x": -2.3598239923706053,
            "y": -9.526
          },
          {
            "id": 1821,
            "x": -4.2190479389648425,
            "y": -10.366953015624999
          },
          {
            "id": 1820,
            "x": -5,
            "y": -11.6640970546875
          },
          {
            "id": 1748,
            "x": -5,
            "y": -12.178080566406251
          }
        ],
        "keypoints_id": [
          {
            "id": 1510,
            "x": -0.49800000000000005,
            "y": -9.526
          },
          {
            "id": 1748,
            "x": -5,
            "y": -12.178080566406251
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 924,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 1509,
            "x": -0.49800000000000005,
            "y": -8.335
          },
          {
            "id": 1822,
            "x": -2.307983992370605,
            "y": -8.335
          },
          {
            "id": 1824,
            "x": -4.011687938964845,
            "y": -8.846540163085935
          },
          {
            "id": 1823,
            "x": -5,
            "y": -10.104576326171873
          },
          {
            "id": 1749,
            "x": -5,
            "y": -10.908864155273436
          }
        ],
        "keypoints_id": [
          {
            "id": 1509,
            "x": -0.49800000000000005,
            "y": -8.335
          },
          {
            "id": 1749,
            "x": -5,
            "y": -10.908864155273436
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      },
      {
        "id": 925,
        "road_type": "bspline",
        "controlpoints": [
          {
            "id": 1508,
            "x": -0.49800000000000005,
            "y": -7.153999999999999
          },
          {
            "id": 1825,
            "x": -2.2105247923706055,
            "y": -7.153999999999999
          },
          {
            "id": 1827,
            "x": -4.165134338964842,
            "y": -7.682401753320313
          },
          {
            "id": 1826,
            "x": -5,
            "y": -9.1936896625
          },
          {
            "id": 1765,
            "x": -5,
            "y": -9.662054180273437
          }
        ],
        "keypoints_id": [
          {
            "id": 1508,
            "x": -0.49800000000000005,
            "y": -7.153999999999999
          },
          {
            "id": 1765,
            "x": -5,
            "y": -9.662054180273437
          }
        ],
        "rotate_direction": "clockwise",
        "max_speed": 0.5,
        "min_speed": 0.2,
        "agv_direction": "start_to_end",
        "running_type": "turn",
        "agv_angle": 0,
        "move_direction": "forward",
        "front_safe_level": -1,
        "back_safe_level": -1,
        "left_safe_level": -1,
        "right_safe_level": -1,
        "turnlight": "front",
        "precise_park": false,
        "navigate_mode": "hybrid_navigation",
        "car_profile": "1"
      }
    ],
    "waypoints": [
      {
        "id": 670,
        "x": -390,
        "y": -41.7,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": true,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 671,
        "x": -390,
        "y": -269.877782791111,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": true,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 672,
        "x": -411.6,
        "y": -35.6,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": true,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 673,
        "x": -460,
        "y": -38.6,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": true,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 674,
        "x": -460,
        "y": -269.01887999999997,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": true,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 68,
        "x": -214.7,
        "y": -21.5,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 69,
        "x": -225,
        "y": -34.99520000000001,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 70,
        "x": -222.08,
        "y": -25.362644855967076,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 71,
        "x": -235.65279115226346,
        "y": -21.5,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 72,
        "x": -243.04,
        "y": -31.54685185185187,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 73,
        "x": -241.4789096296295,
        "y": -24.908039259259255,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 74,
        "x": -246.3761574074075,
        "y": -21.5,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 75,
        "x": -254.43999999999994,
        "y": -31.383055555555558,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 76,
        "x": -252.52199074074068,
        "y": -25.126805555555556,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 77,
        "x": -258,
        "y": -21.5,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 78,
        "x": -265.24,
        "y": -31.129038888888875,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 79,
        "x": -263.5310185185187,
        "y": -24.66738962962964,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 80,
        "x": -268.2898148148149,
        "y": -21.5,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 81,
        "x": -276.44,
        "y": -31.26601851851852,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 82,
        "x": -274.3326388888889,
        "y": -24.66088148148149,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 83,
        "x": -290,
        "y": -21.5,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 84,
        "x": -300,
        "y": -35.9,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 85,
        "x": -297.57959036458334,
        "y": -25.750400000000006,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 86,
        "x": -328.8458333333333,
        "y": -21.5,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 87,
        "x": -338.2,
        "y": -31.63444444444444,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 88,
        "x": -335.38394570312516,
        "y": -24.645913333333347,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 89,
        "x": -339.80000000000007,
        "y": -21.5,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 90,
        "x": -348.9,
        "y": -31.426111111111105,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 91,
        "x": -346.261029470486,
        "y": -24.549266666666657,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 92,
        "x": -353.1847222222222,
        "y": -21.5,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 93,
        "x": -361.2,
        "y": -31.195277777777775,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 94,
        "x": -358.6397088975695,
        "y": -24.997922222222208,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 95,
        "x": -364.4375,
        "y": -21.5,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 96,
        "x": -373.5,
        "y": -31.228333333333346,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 97,
        "x": -370.7297143229167,
        "y": -25.11486666666667,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 98,
        "x": -379.4,
        "y": -21.5,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 99,
        "x": -390,
        "y": -33.980000000000004,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 621,
        "x": -387.17247890625,
        "y": -25.80999999999999,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 580,
        "x": -450,
        "y": -21.5,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 581,
        "x": -460,
        "y": -35,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 582,
        "x": -457.05305690104194,
        "y": -26.113466666666657,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 586,
        "x": -243.04,
        "y": -20.5,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 111,
        "x": -255.20960000000014,
        "y": -7.5,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 112,
        "x": -246.8667418981484,
        "y": -13.154351851851839,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 113,
        "x": -254.43999999999994,
        "y": -20.5,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 114,
        "x": -266.69120000000015,
        "y": -7.5,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 115,
        "x": -257.73495555555564,
        "y": -13.006366666666665,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 116,
        "x": -265.24,
        "y": -20.5,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 117,
        "x": -280.07359999999994,
        "y": -7.5,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 118,
        "x": -269.78159999999997,
        "y": -12.634722222222226,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 119,
        "x": -276.44,
        "y": -20.5,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 120,
        "x": -290,
        "y": -7.5,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 121,
        "x": -280.9444444444445,
        "y": -12.443981481481483,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 122,
        "x": -300,
        "y": -21.200000000000003,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 123,
        "x": -312,
        "y": -7.5,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 124,
        "x": -303.01180251736105,
        "y": -13.327648148148148,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 125,
        "x": -338.2,
        "y": -21.200000000000003,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 126,
        "x": -347.1,
        "y": -7.5,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 127,
        "x": -340.23226529947914,
        "y": -13.116777777777775,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 128,
        "x": -348.9,
        "y": -21.200000000000003,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 129,
        "x": -359.1,
        "y": -7.5,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 130,
        "x": -351.11760742187494,
        "y": -13.247999999999996,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 131,
        "x": -361.2,
        "y": -21.200000000000003,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 132,
        "x": -371.4,
        "y": -7.5,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 133,
        "x": -363.38640136718755,
        "y": -13.4,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 134,
        "x": -373.5,
        "y": -21.200000000000003,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 135,
        "x": -382.09599609375,
        "y": -7.5,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 136,
        "x": -375.847998046875,
        "y": -13.100000000000001,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 137,
        "x": -390,
        "y": -21.200000000000003,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 138,
        "x": -402,
        "y": -7.5,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 139,
        "x": -394.2887939453125,
        "y": -13.200000000000001,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 140,
        "x": -411.6,
        "y": -21.283333333333335,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 141,
        "x": -420.79999999999995,
        "y": -7.5,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 142,
        "x": -413.41666666666686,
        "y": -12.900000000000007,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 143,
        "x": -460,
        "y": -21.200000000000003,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 144,
        "x": -471.2,
        "y": -7.5,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 145,
        "x": -463.23603515625,
        "y": -13.483333333333338,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 233,
        "x": -220,
        "y": -166.05079586829675,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 234,
        "x": -224.8605925933219,
        "y": -144.37164713541674,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 235,
        "x": -222.76638888888894,
        "y": -154.51671694155092,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 292,
        "x": -292.89351851851853,
        "y": -245,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 293,
        "x": -300,
        "y": -235,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 294,
        "x": -297.9681877572017,
        "y": -241.5676728708526,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 295,
        "x": -292.40000000000003,
        "y": -260,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 296,
        "x": -300,
        "y": -250,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 297,
        "x": -297.99632654321,
        "y": -256.8355980195474,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 299,
        "x": -317.1740488932292,
        "y": -235,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 591,
        "x": -300,
        "y": -225,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 592,
        "x": -306.13257223958334,
        "y": -232.33086478645828,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 593,
        "x": -315.13640000000015,
        "y": -250,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 594,
        "x": -300,
        "y": -240,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 595,
        "x": -305.43999999999994,
        "y": -247.29891927083332,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 598,
        "x": -407.59999999999997,
        "y": -240,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 599,
        "x": -390,
        "y": -228.08814974247693,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 620,
        "x": -394.286322962963,
        "y": -237.04160116753474,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 310,
        "x": -405.86388888888877,
        "y": -255,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 311,
        "x": -390,
        "y": -246.11885514756946,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 312,
        "x": -395.5352437037037,
        "y": -253.53764609230322,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 314,
        "x": -481.4,
        "y": -164.82399999999998,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 315,
        "x": -460,
        "y": -141.40627041015622,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 316,
        "x": -473.42160000000007,
        "y": -151.27192910156256,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 319,
        "x": -469.5359999999999,
        "y": -240,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 320,
        "x": -460,
        "y": -230.70719414062492,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 321,
        "x": -463.5871999999998,
        "y": -235.54639707031245,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 322,
        "x": -471.55199999999985,
        "y": -255,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 323,
        "x": -460,
        "y": -243.84000976562504,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 324,
        "x": -463.73120000000023,
        "y": -251.94560488281243,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 326,
        "x": -407.70000000000005,
        "y": -160.3472,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 327,
        "x": -390,
        "y": -139.24079414062498,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 328,
        "x": -401.1815999999999,
        "y": -146.37119707031252,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 347,
        "x": -484.7,
        "y": -30.739722222222223,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 348,
        "x": -477.05138888888894,
        "y": -21.5,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 349,
        "x": -482.244177517361,
        "y": -24.59999999999999,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 356,
        "x": -495.3,
        "y": -30.560833333333335,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 357,
        "x": -487.70000000000005,
        "y": -21.5,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 358,
        "x": -492.9322113715275,
        "y": -24.460833333333333,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 359,
        "x": -506.8,
        "y": -30.37888888888888,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 360,
        "x": -498.72916666666663,
        "y": -21.5,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 361,
        "x": -504.0069769965276,
        "y": -24.27694444444445,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 362,
        "x": -518.2,
        "y": -32.10888888888889,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 363,
        "x": -509.72755555555574,
        "y": -21.5,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 364,
        "x": -515.8049390190974,
        "y": -25.264777777777784,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 366,
        "x": -575,
        "y": -154.16513995659724,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 367,
        "x": -540,
        "y": -115.74412298828123,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 368,
        "x": -562.0920933333332,
        "y": -130.64322981336812,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 371,
        "x": -553.06144,
        "y": -260,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 372,
        "x": -530.0824968500705,
        "y": -248.98823187500005,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 373,
        "x": -540.9568718750003,
        "y": -257.2645159374999,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 374,
        "x": -547.6700800000002,
        "y": -240,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 375,
        "x": -529.937516438187,
        "y": -226.16004078125002,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 376,
        "x": -536.42954765625,
        "y": -235.85042039062503,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 377,
        "x": -529.3733814956556,
        "y": -137.37727509765625,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 378,
        "x": -540,
        "y": -122.14624740234376,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 379,
        "x": -534.699853515625,
        "y": -131.21328125,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 424,
        "x": -540,
        "y": -34.7,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 425,
        "x": -530,
        "y": -21.5,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 426,
        "x": -537.1689671666666,
        "y": -26.10150933333335,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 427,
        "x": -234.00000000000003,
        "y": -171.4347119140625,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 428,
        "x": -225.00942546212792,
        "y": -142.31048046875,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 429,
        "x": -228.58333333333334,
        "y": -159.63192952473958,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 430,
        "x": -226.81666666666666,
        "y": -250,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 431,
        "x": -234.00000000000003,
        "y": -240,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 432,
        "x": -231.938425925926,
        "y": -246.52412109375,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 433,
        "x": -225.9,
        "y": -236.94220238435457,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 434,
        "x": -234.00000000000003,
        "y": -227,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 435,
        "x": -232.174537037037,
        "y": -233.9590386284723,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 438,
        "x": -286.5,
        "y": -168.50800488281251,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 439,
        "x": -300,
        "y": -147.19199999999998,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 440,
        "x": -291.13833333333326,
        "y": -156.08266910807293,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 443,
        "x": -280,
        "y": -126.25999999999999,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 444,
        "x": -300,
        "y": -109.99280151367185,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 445,
        "x": -289.84,
        "y": -116.72640380859376,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 446,
        "x": -336.9,
        "y": -163.10992244140618,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 447,
        "x": -300,
        "y": -124.48896151367191,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 448,
        "x": -322.56352000000015,
        "y": -140.36447892578127,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 461,
        "x": -423.00000000000006,
        "y": -31.149166666666673,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 462,
        "x": -414.66388888888906,
        "y": -21.5,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 463,
        "x": -420.276028726209,
        "y": -24.75480195473252,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 464,
        "x": -433.8,
        "y": -31.082777777777785,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 465,
        "x": -426.0902777777779,
        "y": -21.5,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 466,
        "x": -431.27100919495916,
        "y": -25.047047325102874,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 467,
        "x": -445,
        "y": -30.865000000000006,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 468,
        "x": -437.01250000000005,
        "y": -21.5,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 469,
        "x": -443.0102930893131,
        "y": -24.6899074074074,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 470,
        "x": -423.00000000000006,
        "y": -21.200000000000003,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 471,
        "x": -433.29999999999995,
        "y": -7.5,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 472,
        "x": -425.60347342785485,
        "y": -13.41418467078189,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 473,
        "x": -433.8,
        "y": -21.200000000000003,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 474,
        "x": -445.90000000000003,
        "y": -7.5,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 475,
        "x": -437.49819114904835,
        "y": -13.405452674897118,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 476,
        "x": -445,
        "y": -21.200000000000003,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 477,
        "x": -457.30000000000007,
        "y": -7.5,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 478,
        "x": -449.2085169913838,
        "y": -13.213387345679006,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 483,
        "x": -484.7,
        "y": -21.200000000000003,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 484,
        "x": -494.80000000000007,
        "y": -7.5,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 485,
        "x": -487.3353411458333,
        "y": -13.45,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 486,
        "x": -495.3,
        "y": -19.8,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 487,
        "x": -506.3,
        "y": -7.5,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 488,
        "x": -498.41202734375025,
        "y": -12.469499999999993,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 489,
        "x": -506.8,
        "y": -20.2,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 490,
        "x": -519.4,
        "y": -7.5,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 491,
        "x": -509.8346686197916,
        "y": -12.706000000000007,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 492,
        "x": -518.2,
        "y": -21.200000000000003,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 493,
        "x": -532.6,
        "y": -7.5,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 494,
        "x": -522.7640234375001,
        "y": -13.331333333333333,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 577,
        "x": -411.6,
        "y": -31.51666666666667,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 578,
        "x": -403.4527777777777,
        "y": -21.5,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 579,
        "x": -409.1519352146562,
        "y": -24.899012345679008,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 637,
        "x": -540,
        "y": -21.200000000000003,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 638,
        "x": -550.5,
        "y": -7.5,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 639,
        "x": -542.7822678385419,
        "y": -13.2048,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 654,
        "x": -177.36,
        "y": -31.660000000000004,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 655,
        "x": -168.9,
        "y": -21.5,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 656,
        "x": -174.70800048828121,
        "y": -25.12,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 657,
        "x": -188.76000000000002,
        "y": -31.4,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 658,
        "x": -179,
        "y": -21.5,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 659,
        "x": -185.43600585937494,
        "y": -24.99199951171875,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 660,
        "x": -199.56,
        "y": -31.188935185185183,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 661,
        "x": -191.08680555555554,
        "y": -21.5,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 662,
        "x": -197.67886257595484,
        "y": -24.392361721462663,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 663,
        "x": -210.76,
        "y": -31.141712962962977,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 664,
        "x": -203.64722222222224,
        "y": -21.5,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 665,
        "x": -208.96685818142367,
        "y": -24.793556043836805,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 676,
        "x": -199.56,
        "y": -20.5,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 677,
        "x": -211.83599999999998,
        "y": -7.5,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 678,
        "x": -202.1088,
        "y": -12.672799999999997,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 679,
        "x": -210.76,
        "y": -20.5,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 680,
        "x": -223.30879999999996,
        "y": -7.5,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 681,
        "x": -213.85000000000002,
        "y": -12.327200000000001,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 690,
        "x": -188.76000000000002,
        "y": -20.5,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 691,
        "x": -200.3544,
        "y": -7.5,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 692,
        "x": -191.56096000000002,
        "y": -12.532506666666665,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 693,
        "x": -177.36,
        "y": -20.4552,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 694,
        "x": -187.864,
        "y": -7.5,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 695,
        "x": -180.06016,
        "y": -12.5,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 739,
        "x": -347.59999999999997,
        "y": -167.5,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 740,
        "x": -300,
        "y": -129.8528015136719,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 741,
        "x": -325.45000000000005,
        "y": -144.07640380859374,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 744,
        "x": -419,
        "y": -165.2,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 745,
        "x": -390,
        "y": -145.9407958984375,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 746,
        "x": -406.95,
        "y": -152.82039794921874,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 749,
        "x": -491.70000000000005,
        "y": -167.80799472656247,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 750,
        "x": -460,
        "y": -146.59027441406255,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 751,
        "x": -477.70877265625006,
        "y": -153.7431345703125,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 754,
        "x": -209.3,
        "y": -173.3194140082466,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 755,
        "x": -224.7935945705652,
        "y": -149.27997639973964,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 756,
        "x": -216.1219645182291,
        "y": -164.97169520399297,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 759,
        "x": -275.79999999999995,
        "y": -172.65233333333327,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 760,
        "x": -300,
        "y": -141.4399975585938,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 761,
        "x": -290.4890000000001,
        "y": -157.43667154947912,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 778,
        "x": -565.9000000000001,
        "y": -31.173177777777774,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 779,
        "x": -558.929999609375,
        "y": -21.5,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 780,
        "x": -563.6404464409727,
        "y": -24.459422222222223,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 781,
        "x": -577.9,
        "y": -32.40367555555554,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 782,
        "x": -570.7731555121527,
        "y": -21.5,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 783,
        "x": -576.1626688368058,
        "y": -24.008122222222205,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 784,
        "x": -589.9,
        "y": -30.96097777777777,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 785,
        "x": -582.6485455729165,
        "y": -21.5,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 786,
        "x": -587.765811675347,
        "y": -24.229077777777796,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 787,
        "x": -601.9,
        "y": -31.024999999999984,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 788,
        "x": -594.301996875,
        "y": -21.5,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 789,
        "x": -599.9571428819444,
        "y": -24.35873333333334,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 790,
        "x": -619.5,
        "y": -36.34131145019532,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 791,
        "x": -609.9455984374997,
        "y": -21.5,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 792,
        "x": -617.23799921875,
        "y": -25.932254199218768,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 793,
        "x": -619.5,
        "y": -19.838911450195315,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 794,
        "x": -631.2575843750001,
        "y": -7.5,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 795,
        "x": -623.1323921875002,
        "y": -13.027854199218753,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 798,
        "x": -565.9000000000001,
        "y": -21.183360000000008,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 799,
        "x": -580.9536062500001,
        "y": -7.5,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 800,
        "x": -570.5229631249998,
        "y": -10.955792000000011,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1220,
        "x": -577.9,
        "y": -18.530400000000007,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1221,
        "x": -591.7952078124999,
        "y": -7.5,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1164,
        "x": -583.95560390625,
        "y": -12.4644,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 807,
        "x": -601.9,
        "y": -21.857600000000005,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 808,
        "x": -617.0880132812498,
        "y": -7.5,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 809,
        "x": -607.3220066406249,
        "y": -13.000400000000006,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 810,
        "x": -627.9,
        "y": -159.29919999999998,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 811,
        "x": -619.5,
        "y": -137.4,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 812,
        "x": -625.3412000000002,
        "y": -147.74479999999997,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 817,
        "x": -638.4000000000001,
        "y": -167.635205078125,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 818,
        "x": -619.5,
        "y": -144.23039550781252,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 819,
        "x": -631.7,
        "y": -153.53280029296877,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 861,
        "x": -225,
        "y": -22.055199999999996,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 862,
        "x": -238.6005193359375,
        "y": -7.5,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 863,
        "x": -228.646671875,
        "y": -12.5,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 893,
        "x": -210.10000000000002,
        "y": -128.97400797526043,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 894,
        "x": -225.03775478659412,
        "y": -115.54383951822913,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 895,
        "x": -217.77585937500007,
        "y": -122.66058430989582,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 927,
        "x": -635.55,
        "y": -31.597500000000007,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 928,
        "x": -626.7733333333331,
        "y": -21.5,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 929,
        "x": -632.7658333333333,
        "y": -25.23583333333334,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 930,
        "x": -646.75,
        "y": -31.198611111111106,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 931,
        "x": -638.2388888888889,
        "y": -21.5,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 932,
        "x": -643.9733333333334,
        "y": -24.986388888888882,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 934,
        "x": -646.75,
        "y": -19.4,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 935,
        "x": -659.0999999999999,
        "y": -7.5,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 936,
        "x": -650.4000000000001,
        "y": -12.750000000000002,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 937,
        "x": -635.55,
        "y": -18.8,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 938,
        "x": -647.014111328125,
        "y": -7.5,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 939,
        "x": -638.75703125,
        "y": -12.450000000000001,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 950,
        "x": -378.6,
        "y": -137.4,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 951,
        "x": -390,
        "y": -125.14079589843749,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 952,
        "x": -384.20000000000005,
        "y": -131.7703979492188,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 955,
        "x": -446.1,
        "y": -136.6,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 956,
        "x": -460,
        "y": -116.89919433593751,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 957,
        "x": -452.25,
        "y": -126.84959716796875,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 963,
        "x": -112.30000000000001,
        "y": -35.5,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 964,
        "x": -100.7,
        "y": -21.5,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 965,
        "x": -109.67000000000002,
        "y": -26.06,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 966,
        "x": -129.8,
        "y": -33.04000000000001,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 967,
        "x": -119.05520019531251,
        "y": -21.5,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 968,
        "x": -126.77760009765626,
        "y": -25.160000000000004,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 969,
        "x": -94.5,
        "y": -20.5,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 970,
        "x": -104.73279628906252,
        "y": -7.5,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 971,
        "x": -96.77559814453126,
        "y": -12.450000000000001,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 972,
        "x": -112.30000000000001,
        "y": -20.5,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 973,
        "x": -122.1751953125,
        "y": -7.5,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 974,
        "x": -114.58759765625003,
        "y": -12.5,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 975,
        "x": -129.8,
        "y": -20.5,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 976,
        "x": -140.281597265625,
        "y": -7.5,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 977,
        "x": -132.29959863281252,
        "y": -12.5,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1067,
        "x": -657.9000000000001,
        "y": -30.79250000000001,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1068,
        "x": -649.3055555555555,
        "y": -21.5,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1069,
        "x": -655.3830555555555,
        "y": -24.68527777777778,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1070,
        "x": -669.7,
        "y": -31.164444444444452,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1071,
        "x": -660.8297222222222,
        "y": -21.5,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1072,
        "x": -667.3116666666664,
        "y": -24.536944444444448,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1073,
        "x": -657.9000000000001,
        "y": -19,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1074,
        "x": -669.4,
        "y": -7.5,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1075,
        "x": -661.7,
        "y": -12.55,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1076,
        "x": -669.7,
        "y": -20.7,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1077,
        "x": -679.3000000000001,
        "y": -7.5,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1078,
        "x": -672.5,
        "y": -12.95,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1151,
        "x": -691.2440000000001,
        "y": -10.13041399416908,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1152,
        "x": -662,
        "y": -21.5,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1153,
        "x": -675.74,
        "y": -21.389999999999997,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1154,
        "x": -707.3000000000001,
        "y": -16.115999999999996,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1155,
        "x": -701.1800000000001,
        "y": -9.5,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1156,
        "x": -705.7999999999998,
        "y": -11.411999999999999,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1157,
        "x": -733.1,
        "y": -16.5,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1158,
        "x": -725.0060000000001,
        "y": -9.5,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1159,
        "x": -731.6560000000002,
        "y": -11.488,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1163,
        "x": -589.9,
        "y": -19.89520000000001,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 806,
        "x": -594.3276070312503,
        "y": -12.400400000000005,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 805,
        "x": -602.7456140624998,
        "y": -7.5,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1175,
        "x": -643.6727636718751,
        "y": -7.5,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1176,
        "x": -675.384015625,
        "y": 2,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1177,
        "x": -659.0244140625,
        "y": -3.75,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1180,
        "x": -760.5240000000001,
        "y": 2,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1181,
        "x": -778,
        "y": -15.072000122070312,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1182,
        "x": -775.8140000000001,
        "y": -1.3520000610351588,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1186,
        "x": -766,
        "y": -20.616000671386715,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1187,
        "x": -755.1839843749999,
        "y": -9.5,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1188,
        "x": -763.0759921875,
        "y": -12.466001098632818,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1193,
        "x": -768.9800000000002,
        "y": -81.80000000000001,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1194,
        "x": -778,
        "y": -92.0400048828125,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1195,
        "x": -775.5940156250001,
        "y": -85.13866910807293,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1197,
        "x": -768.2279941406249,
        "y": -70.90000000000002,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1198,
        "x": -778,
        "y": -81.23999926757811,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1199,
        "x": -775.62797265625,
        "y": -74.31399658203127,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1200,
        "x": -758.2716666666669,
        "y": -81.80000000000001,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1201,
        "x": -766.0000000000001,
        "y": -76.24799584960934,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1202,
        "x": -763.2926022916671,
        "y": -80.30891830989586,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1204,
        "x": -757.7274181406246,
        "y": -70.90000000000002,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1205,
        "x": -766,
        "y": -64.89350351171878,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1206,
        "x": -764.0353059895835,
        "y": -68.86391975585938,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1209,
        "x": -707.3000000000001,
        "y": -7.044000366210943,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1210,
        "x": -718.9439785156251,
        "y": 2,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1211,
        "x": -710.56596484375,
        "y": -0.5060001831054727,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1212,
        "x": -733.1,
        "y": -5.124000000000006,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1213,
        "x": -742.632017578125,
        "y": 2,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1214,
        "x": -736.3179843750002,
        "y": 0.16599999999999537,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1330,
        "x": -8.872560305175778,
        "y": -143.84,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1331,
        "x": -27,
        "y": -125.95287999999998,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1332,
        "x": -21.87851938964844,
        "y": -137.9040000000001,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1348,
        "x": -9.106800305175783,
        "y": -131.64,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1349,
        "x": -27,
        "y": -116.86341423177085,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1350,
        "x": -19.58813272298177,
        "y": -127.10033378255216,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1351,
        "x": -9.288240305175782,
        "y": -119.42999999999999,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1352,
        "x": -27,
        "y": -104.66976140625005,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1353,
        "x": -19.18571938964843,
        "y": -115.64368070312503,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1354,
        "x": -8.95416030517578,
        "y": -107.21999999999997,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1355,
        "x": -27,
        "y": -93.45504216796876,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1356,
        "x": -20.661319389648447,
        "y": -103.83272718749993,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1357,
        "x": -7.825200305175786,
        "y": -95.25999999999999,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1358,
        "x": -27,
        "y": -79.15295808593753,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1359,
        "x": -20.706359389648426,
        "y": -90.9911990429687,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1360,
        "x": -8.88504030517578,
        "y": -83.35000000000001,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1361,
        "x": -27,
        "y": -67.50048033203123,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1362,
        "x": -20.138999389648433,
        "y": -79.51920016601561,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1363,
        "x": -11.281200305175787,
        "y": -71.53999999999999,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1364,
        "x": -27,
        "y": -59.90591918945312,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1365,
        "x": -19.28939938964842,
        "y": -69.02639654296877,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1494,
        "x": -94.5,
        "y": -34.20000000000001,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1495,
        "x": -82.80000122070312,
        "y": -21.5,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1496,
        "x": -91.10999755859376,
        "y": -24.609999999999992,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1541,
        "x": -26.933052631578917,
        "y": -34.951294990234366,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1542,
        "x": -52.37808082031251,
        "y": -7.5,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1543,
        "x": -30.062400410156258,
        "y": -13.803888828125,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1629,
        "x": -151.80000000000004,
        "y": -147.6091796875,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1630,
        "x": -133.9300048828125,
        "y": -163.79999999999998,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1631,
        "x": -144.7080024414063,
        "y": -158.51858984375,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1687,
        "x": -133.9300048828125,
        "y": -163.79999999999998,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1688,
        "x": -163.79999999999998,
        "y": -140.64959716796875,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1689,
        "x": -159.45629204140627,
        "y": -163.19219028749998,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1690,
        "x": -137.49511755859373,
        "y": -176.8,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1691,
        "x": -163.79999999999998,
        "y": -165.35193633203133,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1692,
        "x": -157.83895688281257,
        "y": -176.6084420625,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1693,
        "x": -163.79999999999998,
        "y": -33.90627884277344,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1694,
        "x": -152.10324187499995,
        "y": -21.5,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1695,
        "x": -160.5425009375,
        "y": -24.94698094726562,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1718,
        "x": -151.80000000000004,
        "y": -22.720799389648437,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1719,
        "x": -166.90956064453127,
        "y": -7.5,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1720,
        "x": -155.81237421875,
        "y": -12.475199694824221,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1721,
        "x": -151.80000000000004,
        "y": -33.520799902343754,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1722,
        "x": -141.38929404296877,
        "y": -21.5,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1723,
        "x": -148.84105312499997,
        "y": -24.65919995117187,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1726,
        "x": -129.47719755859373,
        "y": -176.8,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1727,
        "x": -151.80000000000004,
        "y": -159.6264,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1728,
        "x": -145.17276168281242,
        "y": -170.79978560000006,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1739,
        "x": -65.2987578125,
        "y": -21.5,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1740,
        "x": -50,
        "y": -36.51633278808595,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1741,
        "x": -53.12547490624999,
        "y": -25.485467119921875,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1742,
        "x": -64.43327999999998,
        "y": -151.80000000000004,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1743,
        "x": -50,
        "y": -163.53872724609374,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1744,
        "x": -53.982399999999984,
        "y": -154.61080972656256,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1750,
        "x": -63.527998779296894,
        "y": -139.8,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1751,
        "x": -50,
        "y": -151.69056298828127,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1752,
        "x": -53.48800244140624,
        "y": -142.54127539062497,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1753,
        "x": -64.64400122070315,
        "y": -127.80000000000001,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1754,
        "x": -50,
        "y": -139.9804829101563,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1755,
        "x": -53.36199755859373,
        "y": -130.90223535156244,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1756,
        "x": -61.87200000000001,
        "y": -115.8,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1757,
        "x": -50,
        "y": -127.54752050781252,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1758,
        "x": -53.23599999999999,
        "y": -119.08176025390625,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1759,
        "x": -63.31199999999999,
        "y": -103.80000000000001,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1760,
        "x": -50,
        "y": -115.97280566406248,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1761,
        "x": -53.30799999999999,
        "y": -106.75440283203125,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1762,
        "x": -63.38399755859377,
        "y": -91.8,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1763,
        "x": -50,
        "y": -103.89984155273437,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1764,
        "x": -52.98399877929687,
        "y": -94.35792382812501,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1788,
        "x": -55.36999999999999,
        "y": -91.8,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1789,
        "x": -27,
        "y": -67.42847802734376,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1790,
        "x": -37.2967989013672,
        "y": -85.76303901367189,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1791,
        "x": -55.36999999999999,
        "y": -103.80000000000001,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1792,
        "x": -27,
        "y": -79.173119140625,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1793,
        "x": -36.64879853515626,
        "y": -97.99535957031252,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1794,
        "x": -55.36999999999999,
        "y": -115.8,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1795,
        "x": -27,
        "y": -93.58176313476562,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1796,
        "x": -37.761920610351574,
        "y": -111.17663851562499,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1797,
        "x": -55.36999999999999,
        "y": -127.80000000000001,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1798,
        "x": -27,
        "y": -104.591997421875,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1799,
        "x": -36.70856012207031,
        "y": -122.94095871093745,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1800,
        "x": -55.36999999999999,
        "y": -139.8,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1801,
        "x": -27,
        "y": -116.97024175781253,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1802,
        "x": -38.73248061035158,
        "y": -133.48560087890638,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1803,
        "x": -55.37280024414063,
        "y": -151.80000000000004,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1804,
        "x": -27,
        "y": -125.8232744140625,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1805,
        "x": -38.079200122070326,
        "y": -145.93243720703126,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1807,
        "x": -23.079839923706054,
        "y": -143.84,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1808,
        "x": -50,
        "y": -164.47185291015631,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1809,
        "x": -40.42791938964844,
        "y": -150.0605676757813,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1810,
        "x": -28.76357752370605,
        "y": -132.17542588947668,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1811,
        "x": -50,
        "y": -151.04832445312502,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1812,
        "x": -44.60967938964845,
        "y": -139.28783734375003,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1813,
        "x": -28.471199923706045,
        "y": -119.42999999999999,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1814,
        "x": -50,
        "y": -136.14720388671873,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1815,
        "x": -42.397839389648446,
        "y": -124.52267828125002,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1816,
        "x": -22.768799923706048,
        "y": -107.21999999999997,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1817,
        "x": -50,
        "y": -126.28895806640627,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1818,
        "x": -40.11687938964844,
        "y": -113.43672269531253,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1819,
        "x": -23.598239923706053,
        "y": -95.25999999999999,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1820,
        "x": -50,
        "y": -116.64097054687501,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1821,
        "x": -42.190479389648424,
        "y": -103.66953015624999,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1822,
        "x": -23.07983992370605,
        "y": -83.35000000000001,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1823,
        "x": -50,
        "y": -101.04576326171873,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1824,
        "x": -40.116879389648446,
        "y": -88.46540163085935,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1825,
        "x": -22.105247923706056,
        "y": -71.53999999999999,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1826,
        "x": -50,
        "y": -91.936896625,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      },
      {
        "id": 1827,
        "x": -41.65134338964842,
        "y": -76.82401753320313,
        "chargepoint": false,
        "stationpoint": false,
        "spinpoint": false,
        "stoppoint": false,
        "Non_keypoint": false,
        "QRcodepoint": false
      }
    ],
    "cluster": [
      {
        "id": 9,
        "vertexpoints": [
          {
            "id": 940,
            "x": -64.00133965567133,
            "y": -2.6642815945095486
          },
          {
            "id": 941,
            "x": -63.167016804108826,
            "y": -2.6488686428493917
          },
          {
            "id": 942,
            "x": -63.18661856192129,
            "y": -5.226666531032984
          },
          {
            "id": 943,
            "x": -64.0141673900463,
            "y": -5.319259304470484
          },
          {
            "id": 940,
            "x": -64.00133965567133,
            "y": -2.6642815945095486
          }
        ],
        "area_type": "2"
      },
      {
        "id": 10,
        "vertexpoints": [
          {
            "id": 944,
            "x": -65.1760418258102,
            "y": -2.649964939145688
          },
          {
            "id": 945,
            "x": -64.22367190393517,
            "y": -2.662068552426938
          },
          {
            "id": 946,
            "x": -64.20402795862269,
            "y": -5.529259214048027
          },
          {
            "id": 947,
            "x": -65.13324182581019,
            "y": -5.390833310727714
          },
          {
            "id": 944,
            "x": -65.1760418258102,
            "y": -2.649964939145688
          }
        ],
        "area_type": "2"
      },
      {
        "id": 11,
        "vertexpoints": [
          {
            "id": 978,
            "x": -13.39,
            "y": -5.44
          },
          {
            "id": 979,
            "x": -12.56,
            "y": -5.44
          },
          {
            "id": 980,
            "x": -12.44,
            "y": -6.49
          },
          {
            "id": 981,
            "x": -13.41,
            "y": -6.49
          },
          {
            "id": 978,
            "x": -13.39,
            "y": -5.44
          }
        ],
        "area_type": "2"
      },
      {
        "id": 12,
        "vertexpoints": [
          {
            "id": 982,
            "x": -11.66,
            "y": -5.42
          },
          {
            "id": 983,
            "x": -10.89,
            "y": -5.39
          },
          {
            "id": 984,
            "x": -10.84,
            "y": -6.590000000000002
          },
          {
            "id": 985,
            "x": -11.68,
            "y": -6.57
          },
          {
            "id": 982,
            "x": -11.66,
            "y": -5.42
          }
        ],
        "area_type": "2"
      },
      {
        "id": 13,
        "vertexpoints": [
          {
            "id": 986,
            "x": -9.95280000000001,
            "y": -5.422000000000001
          },
          {
            "id": 987,
            "x": -9.0128,
            "y": -5.402
          },
          {
            "id": 988,
            "x": -8.982800000000001,
            "y": -6.542
          },
          {
            "id": 989,
            "x": -9.932800000000006,
            "y": -6.612000000000002
          },
          {
            "id": 986,
            "x": -9.95280000000001,
            "y": -5.422000000000001
          }
        ],
        "area_type": "2"
      },
      {
        "id": 14,
        "vertexpoints": [
          {
            "id": 990,
            "x": -60.69960039062503,
            "y": -2.8270000122070296
          },
          {
            "id": 991,
            "x": -59.713799609375016,
            "y": -2.7778000122070314
          },
          {
            "id": 992,
            "x": -59.753999023437494,
            "y": -5.611599731445312
          },
          {
            "id": 993,
            "x": -60.68600097656249,
            "y": -5.615399780273438
          },
          {
            "id": 990,
            "x": -60.69960039062503,
            "y": -2.8270000122070296
          }
        ],
        "area_type": "2"
      },
      {
        "id": 15,
        "vertexpoints": [
          {
            "id": 994,
            "x": -59.450498694174996,
            "y": -2.77439442335078
          },
          {
            "id": 995,
            "x": -58.51470006136249,
            "y": -2.757194398936719
          },
          {
            "id": 996,
            "x": -58.52149830354998,
            "y": -5.606594337901559
          },
          {
            "id": 997,
            "x": -59.488700451987484,
            "y": -5.611394752940621
          },
          {
            "id": 994,
            "x": -59.450498694174996,
            "y": -2.77439442335078
          }
        ],
        "area_type": "2"
      },
      {
        "id": 16,
        "vertexpoints": [
          {
            "id": 998,
            "x": -58.25300097656252,
            "y": -2.733199890136719
          },
          {
            "id": 999,
            "x": -57.309798046875,
            "y": -2.726399938964844
          },
          {
            "id": 1000,
            "x": -57.284198046875,
            "y": -5.641400024414062
          },
          {
            "id": 1001,
            "x": -58.24460156250002,
            "y": -5.619599853515625
          },
          {
            "id": 998,
            "x": -58.25300097656252,
            "y": -2.733199890136719
          }
        ],
        "area_type": "2"
      },
      {
        "id": 17,
        "vertexpoints": [
          {
            "id": 1002,
            "x": -57.033146202256944,
            "y": -2.7619388034396724
          },
          {
            "id": 1003,
            "x": -56.14177144097223,
            "y": -2.7259387790256087
          },
          {
            "id": 1004,
            "x": -56.09352065972221,
            "y": -5.626094468858509
          },
          {
            "id": 1005,
            "x": -56.96180536024307,
            "y": -5.646011037868925
          },
          {
            "id": 1002,
            "x": -57.033146202256944,
            "y": -2.7619388034396724
          }
        ],
        "area_type": "2"
      },
      {
        "id": 22,
        "vertexpoints": [
          {
            "id": 1025,
            "x": -49.017199999999995,
            "y": -2.7452
          },
          {
            "id": 1026,
            "x": -48.96,
            "y": -5.23
          },
          {
            "id": 1027,
            "x": -47.81,
            "y": -5.29
          },
          {
            "id": 1028,
            "x": -47.87000000000001,
            "y": -2.7152000000000003
          },
          {
            "id": 1025,
            "x": -49.017199999999995,
            "y": -2.7452
          }
        ],
        "area_type": "2"
      },
      {
        "id": 23,
        "vertexpoints": [
          {
            "id": 1029,
            "x": -49.952799999999996,
            "y": -2.8051999999999992
          },
          {
            "id": 1030,
            "x": -50.01,
            "y": -5.22
          },
          {
            "id": 1031,
            "x": -49.13,
            "y": -5.22
          },
          {
            "id": 1032,
            "x": -49.168800000000005,
            "y": -2.7687999999999993
          },
          {
            "id": 1029,
            "x": -49.952799999999996,
            "y": -2.8051999999999992
          }
        ],
        "area_type": "2"
      },
      {
        "id": 24,
        "vertexpoints": [
          {
            "id": 1033,
            "x": -51.1028,
            "y": -2.8495999999999997
          },
          {
            "id": 1034,
            "x": -51.02,
            "y": -5.18
          },
          {
            "id": 1035,
            "x": -50.21,
            "y": -5.18
          },
          {
            "id": 1036,
            "x": -50.17,
            "y": -2.8307999999999995
          },
          {
            "id": 1033,
            "x": -51.1028,
            "y": -2.8495999999999997
          }
        ],
        "area_type": "2"
      },
      {
        "id": 25,
        "vertexpoints": [
          {
            "id": 1037,
            "x": -52.3344,
            "y": -2.8552
          },
          {
            "id": 1038,
            "x": -52.21,
            "y": -5.13
          },
          {
            "id": 1039,
            "x": -51.239999999999995,
            "y": -5.16
          },
          {
            "id": 1040,
            "x": -51.278800000000004,
            "y": -2.8156
          },
          {
            "id": 1037,
            "x": -52.3344,
            "y": -2.8552
          }
        ],
        "area_type": "2"
      },
      {
        "id": 26,
        "vertexpoints": [
          {
            "id": 1041,
            "x": -41.69954880000003,
            "y": -2.6855776000000016
          },
          {
            "id": 1042,
            "x": -41.544800000000016,
            "y": -5.236000000000001
          },
          {
            "id": 1043,
            "x": -40.71000000000001,
            "y": -5.26
          },
          {
            "id": 1044,
            "x": -40.66340799999999,
            "y": -2.7036608000000006
          },
          {
            "id": 1041,
            "x": -41.69954880000003,
            "y": -2.6855776000000016
          }
        ],
        "area_type": "2"
      },
      {
        "id": 27,
        "vertexpoints": [
          {
            "id": 1045,
            "x": -42.729641600000015,
            "y": -2.738144000000001
          },
          {
            "id": 1046,
            "x": -42.76,
            "y": -5.17
          },
          {
            "id": 1047,
            "x": -41.78,
            "y": -5.16
          },
          {
            "id": 1048,
            "x": -41.81536640000001,
            "y": -2.7173023999999995
          },
          {
            "id": 1045,
            "x": -42.729641600000015,
            "y": -2.738144000000001
          }
        ],
        "area_type": "2"
      },
      {
        "id": 28,
        "vertexpoints": [
          {
            "id": 1049,
            "x": -43.7984,
            "y": -2.793200000000001
          },
          {
            "id": 1050,
            "x": -43.800000000000004,
            "y": -5.2700000000000005
          },
          {
            "id": 1051,
            "x": -42.980000000000004,
            "y": -5.24
          },
          {
            "id": 1052,
            "x": -42.96959999999999,
            "y": -2.744400000000001
          },
          {
            "id": 1049,
            "x": -43.7984,
            "y": -2.793200000000001
          }
        ],
        "area_type": "2"
      },
      {
        "id": 29,
        "vertexpoints": [
          {
            "id": 1053,
            "x": -44.936800000000005,
            "y": -2.834800000000001
          },
          {
            "id": 1054,
            "x": -44.94,
            "y": -5.26
          },
          {
            "id": 1055,
            "x": -44.067600000000006,
            "y": -5.229999999999999
          },
          {
            "id": 1056,
            "x": -44.0444,
            "y": -2.7992000000000004
          },
          {
            "id": 1053,
            "x": -44.936800000000005,
            "y": -2.834800000000001
          }
        ],
        "area_type": "2"
      },
      {
        "id": 31,
        "vertexpoints": [
          {
            "id": 1083,
            "x": -67.35800000000002,
            "y": -2.718799999999999
          },
          {
            "id": 1084,
            "x": -66.574,
            "y": -2.677200000000001
          },
          {
            "id": 1085,
            "x": -66.59,
            "y": -5.21
          },
          {
            "id": 1086,
            "x": -67.4,
            "y": -5.2
          },
          {
            "id": 1083,
            "x": -67.35800000000002,
            "y": -2.718799999999999
          }
        ],
        "area_type": "2"
      },
      {
        "id": 33,
        "vertexpoints": [
          {
            "id": 1113,
            "x": -66.2772,
            "y": -2.6860000000000004
          },
          {
            "id": 1114,
            "x": -66.25,
            "y": -5.29
          },
          {
            "id": 1115,
            "x": -65.3,
            "y": -5.34
          },
          {
            "id": 1116,
            "x": -65.37559999999999,
            "y": -2.6284000000000005
          },
          {
            "id": 1113,
            "x": -66.2772,
            "y": -2.6860000000000004
          }
        ],
        "area_type": "2"
      },
      {
        "id": 36,
        "vertexpoints": [
          {
            "id": 1222,
            "x": -76.22258403020832,
            "y": -6.734933849023437
          },
          {
            "id": 1223,
            "x": -76.18525492864582,
            "y": -7.4689305042968765
          },
          {
            "id": 1224,
            "x": -74.42171595989582,
            "y": -7.481257098046873
          },
          {
            "id": 1225,
            "x": -74.45697165520833,
            "y": -6.917237849023438
          },
          {
            "id": 1222,
            "x": -76.22258403020832,
            "y": -6.734933849023437
          }
        ],
        "area_type": "2"
      },
      {
        "id": 37,
        "vertexpoints": [
          {
            "id": 1226,
            "x": -76.2254813765625,
            "y": -7.9081711157552075
          },
          {
            "id": 1227,
            "x": -76.21695715781253,
            "y": -8.672983664583331
          },
          {
            "id": 1228,
            "x": -74.26425959531252,
            "y": -8.696542258333332
          },
          {
            "id": 1229,
            "x": -74.37415981406251,
            "y": -7.981150286653643
          },
          {
            "id": 1226,
            "x": -76.2254813765625,
            "y": -7.9081711157552075
          }
        ],
        "area_type": "2"
      },
      {
        "id": 38,
        "vertexpoints": [
          {
            "id": 1230,
            "x": -73.65110446093749,
            "y": -1.6220160439453122
          },
          {
            "id": 1231,
            "x": -73.6957439765625,
            "y": -2.7889920688476555
          },
          {
            "id": 1232,
            "x": -73.05292659374999,
            "y": -2.840831973632812
          },
          {
            "id": 1233,
            "x": -72.91094632031249,
            "y": -1.5698879978027342
          },
          {
            "id": 1230,
            "x": -73.65110446093749,
            "y": -1.6220160439453122
          }
        ],
        "area_type": "2"
      },
      {
        "id": 39,
        "vertexpoints": [
          {
            "id": 1234,
            "x": -71.0622705078125,
            "y": -1.4423039978027345
          },
          {
            "id": 1235,
            "x": -71.01042968750001,
            "y": -2.892672119140625
          },
          {
            "id": 1236,
            "x": -70.4505615234375,
            "y": -2.913407897949219
          },
          {
            "id": 1237,
            "x": -70.4433615234375,
            "y": -1.39276806640625
          },
          {
            "id": 1234,
            "x": -71.0622705078125,
            "y": -1.4423039978027345
          }
        ],
        "area_type": "2"
      },
      {
        "id": 42,
        "vertexpoints": [
          {
            "id": 1564,
            "x": -9.428256225585937,
            "y": -8.791328125
          },
          {
            "id": 1565,
            "x": -9.386784057617188,
            "y": -9.610399780273438
          },
          {
            "id": 1566,
            "x": -8.381087982421874,
            "y": -9.651872138671875
          },
          {
            "id": 1567,
            "x": -8.4432958984375,
            "y": -8.791328125
          },
          {
            "id": 1564,
            "x": -9.428256225585937,
            "y": -8.791328125
          }
        ],
        "area_type": "2"
      },
      {
        "id": 43,
        "vertexpoints": [
          {
            "id": 1568,
            "x": -9.368472290039064,
            "y": -9.963648071289063
          },
          {
            "id": 1569,
            "x": -9.378839721679686,
            "y": -10.772352294921875
          },
          {
            "id": 1570,
            "x": -8.393880234375,
            "y": -10.710143558593748
          },
          {
            "id": 1571,
            "x": -8.362775982421873,
            "y": -10.025856155273438
          },
          {
            "id": 1568,
            "x": -9.368472290039064,
            "y": -9.963648071289063
          }
        ],
        "area_type": "2"
      },
      {
        "id": 44,
        "vertexpoints": [
          {
            "id": 1572,
            "x": -9.393104248046875,
            "y": -11.088023681640625
          },
          {
            "id": 1573,
            "x": -9.372368164062499,
            "y": -11.979671630859375
          },
          {
            "id": 1574,
            "x": -8.356303772460938,
            "y": -12.0107763671875
          },
          {
            "id": 1575,
            "x": -8.397775898437501,
            "y": -11.150232017578126
          },
          {
            "id": 1572,
            "x": -9.393104248046875,
            "y": -11.088023681640625
          }
        ],
        "area_type": "2"
      },
      {
        "id": 45,
        "vertexpoints": [
          {
            "id": 1576,
            "x": -9.359207870117187,
            "y": -12.34865657421875
          },
          {
            "id": 1577,
            "x": -9.379943954101563,
            "y": -13.229935871093753
          },
          {
            "id": 1578,
            "x": -8.218727962890625,
            "y": -13.261040123046879
          },
          {
            "id": 1579,
            "x": -8.291304046875,
            "y": -12.40049617382813
          },
          {
            "id": 1576,
            "x": -9.359207870117187,
            "y": -12.34865657421875
          }
        ],
        "area_type": "2"
      },
      {
        "id": 46,
        "vertexpoints": [
          {
            "id": 1580,
            "x": -9.37604799609375,
            "y": -13.494503742187499
          },
          {
            "id": 1581,
            "x": -9.365679954101564,
            "y": -14.386151691406251
          },
          {
            "id": 1582,
            "x": -8.25630383691406,
            "y": -14.396519859375001
          },
          {
            "id": 1583,
            "x": -8.266671962890626,
            "y": -13.567079425781248
          },
          {
            "id": 1580,
            "x": -9.37604799609375,
            "y": -13.494503742187499
          }
        ],
        "area_type": "2"
      },
      {
        "id": 47,
        "vertexpoints": [
          {
            "id": 1584,
            "x": -9.375527954101564,
            "y": -14.602039794921874
          },
          {
            "id": 1585,
            "x": -9.417000122070313,
            "y": -15.669943847656251
          },
          {
            "id": 1586,
            "x": -8.349096069335939,
            "y": -15.690679931640625
          },
          {
            "id": 1587,
            "x": -8.3905677109375,
            "y": -14.684983646484378
          },
          {
            "id": 1584,
            "x": -9.375527954101564,
            "y": -14.602039794921874
          }
        ],
        "area_type": "2"
      },
      {
        "id": 48,
        "vertexpoints": [
          {
            "id": 1588,
            "x": -28.175039746093752,
            "y": -3.196800092773439
          },
          {
            "id": 1589,
            "x": -27.164160292968752,
            "y": -3.1190401245117187
          },
          {
            "id": 1590,
            "x": -27.28511962890625,
            "y": -5.529600083007813
          },
          {
            "id": 1591,
            "x": -28.235520019531247,
            "y": -5.486400009765625
          },
          {
            "id": 1588,
            "x": -28.175039746093752,
            "y": -3.196800092773439
          }
        ],
        "area_type": "2"
      },
      {
        "id": 49,
        "vertexpoints": [
          {
            "id": 1592,
            "x": -26.9308809375,
            "y": -3.257280124511719
          },
          {
            "id": 1593,
            "x": -26.956798906250008,
            "y": -5.400000156250002
          },
          {
            "id": 1594,
            "x": -25.98912068359375,
            "y": -5.400000156250002
          },
          {
            "id": 1595,
            "x": -26.06687947265625,
            "y": -3.2140800512695313
          },
          {
            "id": 1592,
            "x": -26.9308809375,
            "y": -3.257280124511719
          }
        ],
        "area_type": "2"
      },
      {
        "id": 50,
        "vertexpoints": [
          {
            "id": 1596,
            "x": -25.790400253906245,
            "y": -3.179520051269531
          },
          {
            "id": 1597,
            "x": -25.807680527343745,
            "y": -5.356800019531245
          },
          {
            "id": 1598,
            "x": -24.995519882812495,
            "y": -5.408640229492183
          },
          {
            "id": 1599,
            "x": -24.935038925781246,
            "y": -3.127680146484375
          },
          {
            "id": 1596,
            "x": -25.790400253906245,
            "y": -3.179520051269531
          }
        ],
        "area_type": "2"
      },
      {
        "id": 51,
        "vertexpoints": [
          {
            "id": 1600,
            "x": -24.770880937500003,
            "y": -3.06720013671875
          },
          {
            "id": 1601,
            "x": -24.71039998046875,
            "y": -5.460480166015625
          },
          {
            "id": 1602,
            "x": -23.88959919921875,
            "y": -5.434559755859375
          },
          {
            "id": 1603,
            "x": -23.915519609375,
            "y": -3.0067200952148436
          },
          {
            "id": 1600,
            "x": -24.770880937500003,
            "y": -3.06720013671875
          }
        ],
        "area_type": "2"
      },
      {
        "id": 52,
        "vertexpoints": [
          {
            "id": 1604,
            "x": -21.4358401171875,
            "y": -3.1276800415039068
          },
          {
            "id": 1605,
            "x": -20.5977590625,
            "y": -3.1276800415039068
          },
          {
            "id": 1606,
            "x": -20.71872056640625,
            "y": -5.495040126953123
          },
          {
            "id": 1607,
            "x": -21.453121074218757,
            "y": -5.538239809570312
          },
          {
            "id": 1604,
            "x": -21.4358401171875,
            "y": -3.1276800415039068
          }
        ],
        "area_type": "2"
      },
      {
        "id": 53,
        "vertexpoints": [
          {
            "id": 1608,
            "x": -20.36448025390625,
            "y": -3.1449599999999998
          },
          {
            "id": 1609,
            "x": -19.56095974609375,
            "y": -3.1881600732421873
          },
          {
            "id": 1610,
            "x": -19.621439482421874,
            "y": -5.477760292968749
          },
          {
            "id": 1611,
            "x": -20.33855984375,
            "y": -5.434560219726562
          },
          {
            "id": 1608,
            "x": -20.36448025390625,
            "y": -3.1449599999999998
          }
        ],
        "area_type": "2"
      },
      {
        "id": 54,
        "vertexpoints": [
          {
            "id": 1612,
            "x": -19.319039580078126,
            "y": -3.0585599267578125
          },
          {
            "id": 1613,
            "x": -19.241279570312503,
            "y": -5.417280019531246
          },
          {
            "id": 1614,
            "x": -18.377280546875003,
            "y": -5.451839956054683
          },
          {
            "id": 1615,
            "x": -18.351360136718753,
            "y": -2.9807999169921873
          },
          {
            "id": 1612,
            "x": -19.319039580078126,
            "y": -3.0585599267578125
          }
        ],
        "area_type": "2"
      },
      {
        "id": 55,
        "vertexpoints": [
          {
            "id": 1616,
            "x": -18.144000517578124,
            "y": -2.903040085449219
          },
          {
            "id": 1617,
            "x": -18.0662405078125,
            "y": -5.434560166015624
          },
          {
            "id": 1618,
            "x": -17.23679958984375,
            "y": -5.408639755859374
          },
          {
            "id": 1619,
            "x": -17.167679716796876,
            "y": -2.894399948730469
          },
          {
            "id": 1616,
            "x": -18.144000517578124,
            "y": -2.903040085449219
          }
        ],
        "area_type": "2"
      },
      {
        "id": 56,
        "vertexpoints": [
          {
            "id": 1636,
            "x": -12.47270457421875,
            "y": -15.883776455078126
          },
          {
            "id": 1637,
            "x": -6.5940479580078115,
            "y": -15.977088222656253
          },
          {
            "id": 1638,
            "x": -6.56294383203125,
            "y": -16.889472255859374
          },
          {
            "id": 1639,
            "x": -12.483072005859373,
            "y": -16.785791835937502
          },
          {
            "id": 1636,
            "x": -12.47270457421875,
            "y": -15.883776455078126
          }
        ],
        "area_type": "2"
      },
      {
        "id": 57,
        "vertexpoints": [
          {
            "id": 1640,
            "x": -12.483072509765625,
            "y": -17.283455810546876
          },
          {
            "id": 1641,
            "x": -6.500736083984377,
            "y": -17.33529541015625
          },
          {
            "id": 1642,
            "x": -6.417791748046875,
            "y": -18.226944580078126
          },
          {
            "id": 1643,
            "x": -12.493439941406251,
            "y": -18.12326416015625
          },
          {
            "id": 1640,
            "x": -12.483072509765625,
            "y": -17.283455810546876
          }
        ],
        "area_type": "2"
      }
    ]
  }
}